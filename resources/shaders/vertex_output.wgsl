struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
};
