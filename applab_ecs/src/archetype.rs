//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::{any::TypeId, fmt::Debug, iter::FromIterator};

use applab_codegen::all_tuples;
use applab_util::{hashset, BlobVec, CastU8, HashMap, HashSet};
use log::debug;
use paste::paste;

use crate::{
    component::{Component, ComponentIndex, ComponentInfoContainer},
    entity::{Entity, EntityLocation},
    storage::{ComponentSlice, ComponentSliceMut},
    World,
};

//--------------------------------------------------------------------------------------------------
//-- TRAITS ----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

// TODO: ArchetypeAppender and ArchetypeBuilder can probably become a single trait.
pub trait ArchetypeAppender: Sized {
    fn is_match(archetype: &Archetype) -> bool;
    fn types() -> HashSet<TypeId>;
    fn register_types(world: &mut World);
    fn append_components<I>(items: I, archetype: &mut Archetype)
    where
        I: IntoIterator<Item = Self>;
    fn append_components_indexed<I>(items: I, index: ComponentIndex, archetype: &mut Archetype)
    where
        I: IntoIterator<Item = Self>;
}

pub trait ArchetypeBuilder {
    fn build(component_info: &mut ComponentInfoContainer) -> Archetype;
}

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// The index of an [`Archetype`] within a [`World`](World).
#[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
pub struct ArchetypeIndex(pub usize);

//--------------------------------------------------------------------------------------------------

#[derive(Debug)]
pub struct Archetype {
    /// Component types contained by this Archetype
    pub(crate) types: HashSet<TypeId>,

    /// Component storage
    // TODO: Make these variables private
    pub(crate) components: HashMap<TypeId, BlobVec>,
    pub(crate) entities:   Vec<Entity>,

    len: usize,
}

impl Archetype {
    pub(crate) fn new(types: HashSet<TypeId>, components: HashMap<TypeId, BlobVec>) -> Self {
        Self {
            types,
            components,
            entities: Vec::default(),
            len: 0,
        }
    }

    #[inline]
    pub const fn len(&self) -> usize {
        self.len
    }

    #[inline]
    pub(crate) unsafe fn set_len(&mut self, len: usize) {
        self.len = len;
    }

    #[inline]
    pub const fn is_empty(&self) -> bool {
        self.len == 0
    }

    #[inline]
    pub fn contains_component<C: Component>(&self) -> bool {
        self.types.contains(&TypeId::of::<C>())
    }

    #[inline]
    pub fn contains_entity(&self, entity: Entity) -> bool {
        self.entities.contains(&entity)
    }

    pub fn reserve(&mut self, amount: usize) {
        for vec in self.components.values_mut() {
            vec.reserve(amount);
        }
    }

    /// Allocate space for a new [Entity], and return its index.
    ///
    /// # Safety
    /// Components must be allocated immediately following the allocation.
    /// Not doing so will result in undefined behaviour.
    // TODO: Investigate providing a default value for all components on allocation.
    //       This will make this function completely safe to use, but I'll need test what
    //       what the performance impact is (if any).
    pub fn allocate(&mut self, entity: Entity) -> ComponentIndex {
        self.reserve(1);
        self.entities.push(entity);

        let index = self.len;
        self.len += 1;

        ComponentIndex(index)
    }

    #[inline]
    pub fn get<T: Component>(&self) -> Option<ComponentSlice<T>> {
        let ptr = self.components.get(&TypeId::of::<T>())?.as_ptr().cast::<T>();

        // SAFETY: `ptr` is a pointer to the underlying archetype storage, which has numerous checks to
        //         ensure the allocations are correct.
        //         The length of the archetype will always match that of its storage.
        let slice = unsafe { std::slice::from_raw_parts::<T>(ptr, self.len) };
        Some(ComponentSlice::new(slice))
    }

    #[inline]
    pub fn get_mut<T: Component>(&mut self) -> Option<ComponentSliceMut<T>> {
        let ptr = self.components.get_mut(&TypeId::of::<T>())?.as_ptr_mut().cast::<T>();

        // SAFETY: `ptr` is a pointer to the underlying archetype storage, which has numerous checks to
        //         ensure the allocations are correct.
        //         The length of the archetype will always match that of its storage.
        let slice = unsafe { std::slice::from_raw_parts_mut::<T>(ptr, self.len) };
        Some(ComponentSliceMut::new(slice))
    }

    #[inline]
    pub fn ptr<T: Component>(&self) -> Option<*const T> {
        Some(self.components.get(&TypeId::of::<T>())?.as_ptr().cast::<T>())
    }

    /// Runs a validation step to ensure self.len is the same as `BlobVec::len`.
    ///
    /// It will only run in debug and during tests.
    ///
    /// # Panics
    /// Will panic if there is a discrepancy in the length variables.
    pub fn validate(&self) {
        #[cfg(any(debug_assertions, test))]
        for vec in self.components.values() {
            assert_eq!(self.len, vec.len(), "Discrepancy found in length values");
        }
    }
}

//--------------------------------------------------------------------------------------------------

#[derive(Debug)]
pub struct ArchetypeContainer {
    archetypes: Vec<Archetype>,
}

impl Default for ArchetypeContainer {
    fn default() -> Self {
        // There is always an archetype for entities with no components.
        let archetypes = vec![Archetype::new(HashSet::default(), HashMap::default())];
        Self { archetypes }
    }
}

impl ArchetypeContainer {
    pub fn spawn(&mut self, entity: Entity) -> EntityLocation {
        let (id, archetype) = (
            0_usize,
            self.archetypes
                .first_mut()
                .expect("[BUG] `archetypes` should always contain at least one entry"),
        );

        debug_assert!(
            archetype.types == HashSet::default(),
            "[BUG] Archetype with index 0 should never contain any types"
        );

        let len = archetype.len();
        archetype.entities.push(entity);

        // SAFETY: A single entity has already been added, and this archetype doesn't
        //         contain any components to be accessed.
        //         There is a debug_assert in place above to ensure this is the case.
        unsafe {
            archetype.set_len(len + 1);
        }

        EntityLocation::new(ArchetypeIndex(id), ComponentIndex(len))
    }

    pub fn spawn_batched<C, I, E>(
        &mut self,
        component_info: &mut ComponentInfoContainer,
        components: I,
        entities: E,
    ) -> Vec<EntityLocation>
    where
        C: ArchetypeAppender + ArchetypeBuilder,
        I: IntoIterator<Item = C>,
        E: IntoIterator<Item = Entity>,
    {
        // Iterate though all archetypes to find a match for the components.
        // If one exists, return it and its index.
        // Otherwise, create a new archetype
        let (id, archetype) = {
            if let Some(archetype) = self.archetypes.iter_mut().enumerate().find(|a| C::is_match(a.1)) {
                debug!("Returning existing archetype");
                archetype
            } else {
                debug!("Creating new archetype");
                self.archetypes.push(C::build(component_info));
                (
                    self.archetypes.len() - 1,
                    self.archetypes.last_mut().expect("Failed to create new archetype"),
                )
            }
        };

        C::append_components(components, archetype);

        // Register the entities with the archetype and return the EntityLocation
        let old_len = archetype.entities.len();
        archetype.entities.extend(entities);
        let new_len = archetype.entities.len();

        (old_len..new_len)
            .map(|index| EntityLocation::new(ArchetypeIndex(id), ComponentIndex(index)))
            .collect()
    }

    /// Returns a reference to the archetype with the given id.
    ///
    /// # Panics
    /// Panics if id is invalid.
    #[inline]
    pub fn get(&self, id: ArchetypeIndex) -> &Archetype {
        self.archetypes
            .get(id.0)
            .unwrap_or_else(|| panic!("Failed to find archetype with id: {}", id.0))
    }

    /// Returns a mutable reference to the archetype with the given id.
    ///
    /// # Panics
    /// Panics if id is invalid.
    #[inline]
    pub fn get_mut(&mut self, id: ArchetypeIndex) -> &mut Archetype {
        self.archetypes
            .get_mut(id.0)
            .unwrap_or_else(|| panic!("Failed to find archetype with id: {}", id.0))
    }

    /// Returns a reference to the archetype at the given entity location.
    ///
    /// # Panics
    /// Panics if location is invalid.
    #[inline]
    pub fn get_for_entity(&self, entity_location: &EntityLocation) -> &Archetype {
        self.archetypes
            .get(entity_location.archetype().0)
            .unwrap_or_else(|| panic!("Failed to find archetype with id: {}", entity_location.archetype().0))
    }

    /// Returns a mutable reference to the archetype at the given entity location.
    ///
    /// # Panics
    /// Panics if location is invalid.
    #[inline]
    pub fn get_for_entity_mut(&mut self, entity_location: &EntityLocation) -> &mut Archetype {
        self.archetypes
            .get_mut(entity_location.archetype().0)
            .unwrap_or_else(|| panic!("Failed to find archetype with id: {}", entity_location.archetype().0))
    }

    #[inline]
    pub fn get_inner(&self) -> &[Archetype] {
        &self.archetypes
    }

    #[inline]
    pub fn get_inner_mut(&mut self) -> &mut [Archetype] {
        &mut self.archetypes
    }

    #[inline]
    pub fn len(&self) -> usize {
        self.archetypes.len()
    }

    #[inline]
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// Runs a validation step to ensure the length of all archetypes is correct.
    ///
    /// It will only run in debug and during tests.
    ///
    /// # Panics
    /// Will panic if there is a discrepancy in the length variables.
    pub fn validate(&self) {
        #[cfg(any(debug_assertions, test))]
        for archetype in &self.archetypes {
            archetype.validate();
        }
    }

    /// Returns the [`ArchetypeIndex`] of an existing [`Archetype`] or creates a new one if it doesn't exist.
    // TODO: This should be made private in the future, once the ArchetypeContainer is able to handle moving entities from
    //       one archetype to another.
    pub(crate) fn get_or_insert(
        &mut self,
        types: HashSet<TypeId>,
        component_info: &ComponentInfoContainer,
    ) -> ArchetypeIndex {
        self.archetypes
            .iter()
            .enumerate()
            .find(|(_, arch)| arch.types == types)
            .map(|(index, _)| ArchetypeIndex(index))
            .unwrap_or_else(|| {
                let components = types
                    .iter()
                    .map(|t| {
                        let component_info = component_info
                            .0
                            .get(t)
                            .expect("No component with matching TypeId found");

                        (*t, BlobVec::new(component_info.layout(), component_info.drop_fn()))
                    })
                    .collect();

                self.archetypes.push(Archetype::new(types, components));
                ArchetypeIndex(self.archetypes.len() - 1)
            })
    }
}

//--------------------------------------------------------------------------------------------------
//-- MACROS ----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

macro_rules! impl_archetype_appender {

    ($($component: ident),*) => {

        #[allow(non_snake_case, unused_parens)]
        impl<$($component),*> ArchetypeAppender for ($($component),*)
        where
            $(
                $component: Component,
            )*
        {
            fn is_match(archetype: &Archetype) -> bool {
                hashset!($($component),*) == archetype.types
            }

            fn types() -> HashSet<TypeId> {
                hashset!($($component),*)
            }

            fn register_types(world: &mut World) {
                $(
                    world.register_component::<$component>();
                )*
            }

            fn append_components<Iter>(items: Iter, archetype: &mut Archetype)
            where
                Iter: IntoIterator<Item = Self>
            {
                archetype.extend(items);
            }

            fn append_components_indexed<I>(items: I, index: ComponentIndex, archetype: &mut Archetype)
            where
                I: IntoIterator<Item = Self>
            {
                let iter = items.into_iter();

                paste! {
                    $(
                        let [<$component _ref>] = archetype.components.get_mut(&TypeId::of::<$component>()).unwrap() as *mut BlobVec;
                    )*
                }

                for ($($component),*) in iter {

                    unsafe {
                        $(
                            let blob_vec = paste! { &mut (*[<$component _ref>]) };

                            if index.0 == blob_vec.len() {
                                blob_vec.push($component.cast_u8());
                            } else if index.0 > blob_vec.len() {
                                panic!("Index out of range!");
                            } else {
                                blob_vec.replace(index.0, $component.cast_u8());
                            }

                            std::mem::forget($component);
                        )*
                    }
                }
            }
        }
    }
}

macro_rules! impl_archetype_builder {

    ($($component: ident),*) => {

        #[allow(unused_parens)]
        impl<$($component: Component),*> ArchetypeBuilder for ($($component),*) {

            fn build(component_info: &mut ComponentInfoContainer) -> Archetype {

                let types = hashset!($($component),*);
                let components = HashMap::from_iter([
                    $(
                        {
                            let type_id = TypeId::of::<$component>();
                            let info = component_info.get_or_insert::<$component>();

                            (type_id, BlobVec::new(info.layout(), info.drop_fn()))
                        }
                    ),*
                ]);

                Archetype::new(types, components)
            }
        }
    }
}

macro_rules! impl_archetype_extend {
    ($($ty: ident),*) => {

    #[allow(non_snake_case, unused_parens)]
    impl<$($ty),*> std::iter::Extend<($($ty),*)> for Archetype
    where
        $($ty: Component),* {

            fn extend<T>(&mut self, iter: T)
            where
                T: IntoIterator<Item = ($($ty),*)> {

                let iter = iter.into_iter();
                let (lower, upper) = iter.size_hint();
                let len = upper.unwrap_or(lower);

                self.reserve(len);

                paste! {

                    $(
                        let [<$ty _ref>] = self.components.get_mut(&TypeId::of::<$ty>()).unwrap() as *mut BlobVec;
                    )*
                }

                for ($($ty),*) in iter {

                    unsafe {
                        $(
                            let blob_vec = paste! { &mut (*[<$ty _ref>]) };
                            blob_vec.push($ty.cast_u8());
                            std::mem::forget($ty);
                        )*
                    }
                }

                self.len += len;
            }
        }
    }
}

all_tuples!(impl_archetype_appender, 1, 16, A);
all_tuples!(impl_archetype_builder, 1, 16, A);
all_tuples!(impl_archetype_extend, 1, 16, A);

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[cfg(test)]
#[allow(clippy::indexing_slicing, clippy::undocumented_unsafe_blocks)]
mod tests {
    use applab_ecs_macros::Component;

    use crate as applab_ecs;

    #[derive(Debug, Component)]
    pub struct Name(pub String);

    #[derive(Debug, Component)]
    pub struct Age(pub u32);

    #[allow(dead_code)]
    #[derive(Debug, Component)]
    pub enum Colour {
        Red,
        Green,
        Blue,
    }

    mod archetype_builder {
        use crate::{
            archetype::{
                tests::{Age, Name},
                ArchetypeBuilder,
            },
            component::ComponentInfoContainer,
        };

        #[test]
        fn build() {
            let mut component_info = ComponentInfoContainer::default();
            let archetype = <(Name, Age)>::build(&mut component_info);

            assert!(archetype.contains_component::<Name>());
            assert!(archetype.contains_component::<Age>());
        }
    }

    mod archetype {
        use std::any::TypeId;

        use applab_util::CastU8;

        use crate::{
            archetype::{
                tests::{Age, Name},
                Archetype, ArchetypeBuilder,
            },
            component::ComponentInfoContainer,
            entity::Entity,
        };

        #[test]
        fn reserve() {
            let mut component_info = ComponentInfoContainer::default();
            let mut archetype: Archetype = <(Name, Age)>::build(&mut component_info);

            archetype.reserve(10);

            for i in archetype.components.values() {
                assert_eq!(i.capacity(), 10);
            }
        }

        #[test]
        fn allocate() {
            let mut component_info = ComponentInfoContainer::default();
            let mut archetype: Archetype = <(Name, Age)>::build(&mut component_info);

            let entities = [Entity(0), Entity(1), Entity(2), Entity(3)];

            for (iter, entity) in entities.iter().enumerate() {
                let res = archetype.allocate(*entity);

                let name = Name("John".to_owned());
                let age = Age(40);

                // Populate with data to prevent undefined behaviour on drop
                unsafe {
                    archetype
                        .components
                        .get_mut(&TypeId::of::<Name>())
                        .unwrap()
                        .push(name.cast_u8());

                    archetype
                        .components
                        .get_mut(&TypeId::of::<Age>())
                        .unwrap()
                        .push(age.cast_u8());
                }

                std::mem::forget(name);
                std::mem::forget(age);

                assert!(archetype.contains_entity(*entity));
                assert_eq!(res.0, iter);
                assert_eq!(archetype.len(), iter + 1);
            }

            archetype.validate();
        }
    }

    mod archetype_container {
        use crate::{
            archetype::{
                tests::{Age, Colour, Name},
                ArchetypeContainer,
            },
            component::ComponentInfoContainer,
            entity::Entity,
        };

        #[test]
        fn spawn() {
            let mut archetype_container = ArchetypeContainer::default();
            assert_eq!(archetype_container.archetypes.len(), 1);

            archetype_container.spawn(Entity(0));
            archetype_container.spawn(Entity(1));
            archetype_container.spawn(Entity(2));

            assert_eq!(archetype_container.archetypes.len(), 1);
            assert_eq!(archetype_container.archetypes[0].len(), 3);

            archetype_container.validate();
        }

        #[test]
        fn add_entity() {
            let mut component_info = ComponentInfoContainer::default();
            let mut archetype_container = ArchetypeContainer::default();

            // Create a new archetype with two components and two entities
            let components = vec![(Name("Foo".to_owned()), Age(20)), (Name("Bar".to_owned()), Age(22))];
            let entities = vec![Entity(0), Entity(1)];
            let locations = archetype_container.spawn_batched(&mut component_info, components, entities);

            assert_eq!(locations[0].archetype().0, 1);
            assert_eq!(locations[1].archetype().0, 1);
            assert_eq!(locations[0].index().0, 0);
            assert_eq!(locations[1].index().0, 1);
            assert_eq!(archetype_container.archetypes.len(), 2);
            assert_eq!(archetype_container.archetypes[1].len(), 2);

            // Add a new entity to the archetype created above
            let components = Some((Name("Duck".to_owned()), Age(40)));
            let entities = Some(Entity(2));
            let locations = archetype_container.spawn_batched(&mut component_info, components, entities);

            assert_eq!(locations[0].archetype().0, 1);
            assert_eq!(locations[0].index().0, 2);
            assert_eq!(archetype_container.archetypes.len(), 2);
            assert_eq!(archetype_container.archetypes[1].len(), 3);

            // Create a new archetype with a single entity
            let components = Some((Name("Foo".to_owned()), Colour::Green));
            let entities = Some(Entity(3));
            let locations = archetype_container.spawn_batched(&mut component_info, components, entities);

            assert_eq!(locations[0].archetype().0, 2);
            assert_eq!(locations[0].index().0, 0);
            assert_eq!(archetype_container.archetypes.len(), 3);
            assert_eq!(archetype_container.archetypes[1].len(), 3);
            assert_eq!(archetype_container.archetypes[2].len(), 1);

            archetype_container.validate();
        }
    }
}
