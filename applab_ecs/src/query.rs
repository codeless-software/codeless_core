//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

mod access;
mod fetch;
mod iter;

use std::{marker::PhantomData, sync::Arc};

pub(crate) use access::{Access, AccessLock, Lockable};
use applab_codegen::all_tuples;
pub(crate) use fetch::{FetchEntity, FetchRead, FetchWrite, ReadOnlyFetch};
pub(crate) use iter::QueryIter;

use crate::{query::fetch::Fetch, World};

//--------------------------------------------------------------------------------------------------
//-- TRAITS ----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub trait WorldQuery: Lockable {
    type Fetch: for<'a> Fetch<'a>;
}

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub struct Query<'world, Q: WorldQuery> {
    world:   &'world World,
    phantom: PhantomData<Q>,
}

impl<'world, Q: WorldQuery> Query<'world, Q> {
    #[inline]
    pub(crate) fn new(world: &'world World) -> Self {
        Self {
            world,
            phantom: PhantomData::default(),
        }
    }

    #[inline]
    pub fn iter(&self) -> QueryIter<'world, Q>
    where
        Q::Fetch: ReadOnlyFetch,
    {
        QueryIter::new(
            self.world.archetype_container.get_inner(),
            Arc::clone(&self.world.access),
        )
    }

    #[inline]
    pub fn iter_mut(&mut self) -> QueryIter<'world, Q> {
        QueryIter::new(
            self.world.archetype_container.get_inner(),
            Arc::clone(&self.world.access),
        )
    }
}

//--------------------------------------------------------------------------------------------------
//-- MACROS ----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

macro_rules! impl_world_query {

    ($($ty: ident),*) => {

        #[allow(non_snake_case)]
        impl<$($ty: WorldQuery),*> WorldQuery for ($($ty,)*) {
            type Fetch = ($($ty::Fetch,)*);
        }

        impl<$($ty: ReadOnlyFetch),*> ReadOnlyFetch for ($($ty,)*) {}
    }
}

all_tuples!(impl_world_query, 1, 16, A);
