//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use applab_codegen::all_tuples;

use crate::{archetype::Archetype, component::Component, entity::Entity};

//--------------------------------------------------------------------------------------------------
//-- TRAITS ----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub trait Fetch<'a>: Sized {
    type Item;

    unsafe fn get(archetype: &'a Archetype) -> Option<Self>;
    unsafe fn fetch(&self, index: usize) -> Self::Item;
}

pub trait ReadOnlyFetch {}

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub struct FetchRead<T>(*const T);

impl<T> ReadOnlyFetch for FetchRead<T> {}

impl<'a, T: Component> Fetch<'a> for FetchRead<T> {
    type Item = &'a T;

    unsafe fn get(archetype: &'a Archetype) -> Option<Self> {
        Some(Self(archetype.ptr::<T>()?))
    }

    unsafe fn fetch(&self, index: usize) -> Self::Item {
        &*self.0.add(index)
    }
}

//--------------------------------------------------------------------------------------------------

pub struct FetchWrite<T>(*mut T);

impl<'a, T: Component> Fetch<'a> for FetchWrite<T> {
    type Item = &'a mut T;

    unsafe fn get(archetype: &'a Archetype) -> Option<Self> {
        Some(Self(archetype.ptr::<T>()? as *mut _))
    }

    unsafe fn fetch(&self, index: usize) -> Self::Item {
        &mut *self.0.add(index)
    }
}

//--------------------------------------------------------------------------------------------------

pub struct FetchEntity(*const Entity);

impl ReadOnlyFetch for FetchEntity {}

impl<'a> Fetch<'a> for FetchEntity {
    type Item = Entity;

    unsafe fn get(archetype: &'a Archetype) -> Option<Self> {
        Some(Self(archetype.entities.as_ptr()))
    }

    unsafe fn fetch(&self, index: usize) -> Self::Item {
        *self.0.add(index)
    }
}

//--------------------------------------------------------------------------------------------------
//-- MACROS ----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

macro_rules! impl_fetch {

    ($($ty: ident),*) => {

        impl<'a, $($ty: Fetch<'a>),*> Fetch<'a> for ($($ty,)*) {
            type Item = ($($ty::Item,)*);

            #[allow(non_snake_case)]
            unsafe fn get(archetype: &'a Archetype) -> Option<Self> {
                Some(( $($ty::get(archetype)?,)* ))
            }

            #[allow(non_snake_case)]
            unsafe fn fetch(&self, index: usize) -> Self::Item {
                let ($($ty,)*) = self;
                ($($ty.fetch(index),)*)
            }
        }
    }
}

all_tuples!(impl_fetch, 1, 16, A);
