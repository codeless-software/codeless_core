//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::{any::TypeId, marker::PhantomData, sync::Arc};

use applab_codegen::all_tuples;
use applab_util::{short_type_name, HashMap, HashSet};
use parking_lot::Mutex;

use crate::query::WorldQuery;

//--------------------------------------------------------------------------------------------------
//-- TRAITS ----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub trait Lockable {
    fn acquire_lock(_access: &mut Access) {}
    fn release_lock(_access: &mut Access) {}
}

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[derive(Debug, Default)]
pub struct Access {
    read:  HashMap<TypeId, usize>,
    write: HashSet<TypeId>,
}

impl Access {
    /// Adds a read lock for [T]
    pub fn add_read<T: 'static>(&mut self) {
        let id = TypeId::of::<T>();
        let name = short_type_name::<T>();

        // Check for existing writes
        assert!(
            !self.write.contains(&id),
            "Cannot add read lock for {}, as it is already locked for writing",
            name
        );

        if let Some(val) = self.read.get_mut(&id) {
            *val += 1;
        } else {
            self.read.insert(id, 1);
        }
    }

    /// Adds a write lock for [T]
    pub fn add_write<T: 'static>(&mut self) {
        let id = TypeId::of::<T>();
        let name = short_type_name::<T>();

        // Check for existing reads or writes
        assert!(
            !(self.read.contains_key(&id) || self.write.contains(&id)),
            "Cannot add write lock for {} as it is already locked for reading or writing",
            name
        );

        self.write.insert(id);
    }

    /// Removes a read lock for [T]
    pub fn remove_read<T: 'static>(&mut self) {
        let id = TypeId::of::<T>();
        let name = short_type_name::<T>();

        let remove_entry = self.read.get_mut(&id).map_or_else(
            || {
                panic!(
                    "Cannot remove read lock for {} as there are no readers (this is a bug!)",
                    name
                );
            },
            |val| {
                *val -= 1;
                *val == 0
            },
        );

        if remove_entry {
            self.read.remove(&id);
        }
    }

    /// Removes a write lock for [T]
    pub fn remove_write<T: 'static>(&mut self) {
        let id = TypeId::of::<T>();
        let name = short_type_name::<T>();

        assert!(
            self.write.remove(&id),
            "Cannot remove write lock for {} as there are no writers (this is a bug!)",
            name
        );
    }
}

//--------------------------------------------------------------------------------------------------

pub struct AccessLock<T: WorldQuery> {
    access: Arc<Mutex<Access>>,
    marker: PhantomData<T>,
}

impl<T: WorldQuery> AccessLock<T> {
    // TODO: Should we pass World in here instead?
    pub fn new(access: Arc<Mutex<Access>>) -> Self {
        T::acquire_lock(&mut access.lock());

        Self {
            access,
            marker: PhantomData::default(),
        }
    }
}

impl<T: WorldQuery> Drop for AccessLock<T> {
    fn drop(&mut self) {
        T::release_lock(&mut self.access.lock());
    }
}

//--------------------------------------------------------------------------------------------------
//-- MACROS ----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

macro_rules! impl_lockable {

    ($($ty: ident),*) => {

        #[allow(non_snake_case)]
        impl<$($ty: Lockable),*> Lockable for ($($ty,)*) {
            fn acquire_lock(access: &mut Access) {
                $( $ty::acquire_lock(access); )*
            }

            fn release_lock(access: &mut Access) {
                $( $ty::release_lock(access); )*
            }
        }
    }
}

all_tuples!(impl_lockable, 1, 16, A);

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[cfg(test)]
mod access_tests {
    use std::any::TypeId;

    use applab_util::{HashMap, HashSet};

    use crate::query::access::Access;

    struct TestComponent;

    fn get_id() -> TypeId {
        TypeId::of::<TestComponent>()
    }

    #[test]
    fn add_read() {
        let mut access = Access::default();

        access.add_read::<TestComponent>();
        assert_eq!(access.read[&get_id()], 1);

        access.add_read::<TestComponent>();
        assert_eq!(access.read[&get_id()], 2);
    }

    #[test]
    #[should_panic(expected = "Cannot add read lock for TestComponent, as it is already locked for writing")]
    fn add_read_with_write() {
        let mut access = Access {
            read:  HashMap::default(),
            write: HashSet::from_iter([get_id()]),
        };
        access.add_read::<TestComponent>();
    }

    #[test]
    fn remove_read() {
        let mut access = Access {
            read:  HashMap::from_iter([(get_id(), 3)]),
            write: HashSet::default(),
        };

        access.remove_read::<TestComponent>();
        assert_eq!(access.read[&get_id()], 2);

        access.remove_read::<TestComponent>();
        assert_eq!(access.read[&get_id()], 1);

        access.remove_read::<TestComponent>();
        assert!(!access.read.contains_key(&get_id()));
    }

    #[test]
    #[should_panic(expected = "Cannot remove read lock for TestComponent as there are no readers (this is a bug!)")]
    fn remove_read_with_no_readers() {
        let mut access = Access::default();
        access.remove_read::<TestComponent>();
    }

    #[test]
    fn add_write() {
        let mut access = Access::default();

        access.add_write::<TestComponent>();
        assert!(access.write.contains(&get_id()));
    }

    #[test]
    #[should_panic(expected = "Cannot add write lock for TestComponent as it is already locked for reading or writing")]
    fn add_write_with_reader() {
        let mut access = Access {
            read:  HashMap::from_iter([(get_id(), 3)]),
            write: HashSet::default(),
        };
        access.add_write::<TestComponent>();
    }

    #[test]
    #[should_panic(expected = "Cannot add write lock for TestComponent as it is already locked for reading or writing")]
    fn add_write_with_writer() {
        let mut access = Access {
            read:  HashMap::default(),
            write: HashSet::from_iter([get_id()]),
        };
        access.add_write::<TestComponent>();
    }

    #[test]
    fn remove_write() {
        let mut access = Access {
            read:  HashMap::default(),
            write: HashSet::from_iter([get_id()]),
        };

        access.remove_write::<TestComponent>();
        assert!(!access.write.contains(&get_id()));
    }

    #[test]
    #[should_panic(expected = "Cannot remove write lock for TestComponent as there are no writers (this is a bug!)")]
    fn remove_write_with_no_writers() {
        let mut access = Access::default();
        access.remove_write::<TestComponent>();
    }
}

//--------------------------------------------------------------------------------------------------

#[cfg(test)]
mod access_lock_tests {
    use std::{any::TypeId, sync::Arc};

    use applab_ecs_macros::Component;
    use parking_lot::Mutex;

    use crate as applab_ecs;
    use crate::query::access::{Access, AccessLock};

    #[derive(Component)]
    struct Name(String);

    #[derive(Component)]
    struct Age(u8);

    fn name_id() -> TypeId {
        TypeId::of::<Name>()
    }

    fn age_id() -> TypeId {
        TypeId::of::<Age>()
    }

    #[test]
    fn multi_read() {
        let access = Arc::new(Mutex::new(Access::default()));

        {
            let _access_lock1 = AccessLock::<(&Name, &Age)>::new(Arc::clone(&access));
            assert_eq!(access.lock().read[&name_id()], 1);
            assert_eq!(access.lock().read[&age_id()], 1);

            let _access_lock2 = AccessLock::<(&Name, &Age)>::new(Arc::clone(&access));
            assert_eq!(access.lock().read[&name_id()], 2);
            assert_eq!(access.lock().read[&age_id()], 2);

            let _access_lock3 = AccessLock::<&Name>::new(Arc::clone(&access));
            assert_eq!(access.lock().read[&name_id()], 3);
            assert_eq!(access.lock().read[&age_id()], 2);
        }

        assert!(!access.lock().read.contains_key(&name_id()));
        assert!(!access.lock().read.contains_key(&age_id()));
    }

    #[test]
    fn multi_write() {
        let access = Arc::new(Mutex::new(Access::default()));

        let _access_lock1 = AccessLock::<&mut Name>::new(Arc::clone(&access));
        assert!(access.lock().write.contains(&name_id()));

        let _access_lock2 = AccessLock::<&mut Age>::new(Arc::clone(&access));
        assert!(access.lock().write.contains(&name_id()));
        assert!(access.lock().write.contains(&age_id()));

        drop(_access_lock1);
        assert!(!access.lock().write.contains(&name_id()));
        assert!(access.lock().write.contains(&age_id()));

        drop(_access_lock2);
        assert!(!access.lock().write.contains(&name_id()));
        assert!(!access.lock().write.contains(&age_id()));
    }

    // Lock component for reading, then try to lock for writing.
    #[test]
    #[should_panic(expected = "Cannot add write lock for Age as it is already locked for reading or writing")]
    fn read_write_conflict() {
        let access = Arc::new(Mutex::new(Access::default()));

        let _access_lock1 = AccessLock::<(&Name, &Age)>::new(Arc::clone(&access));
        let _access_lock2 = AccessLock::<&mut Age>::new(Arc::clone(&access));
    }

    // Lock component for writing, then try to lock for reading.
    #[test]
    #[should_panic(expected = "Cannot add read lock for Name, as it is already locked for writing")]
    fn write_read_conflict() {
        let access = Arc::new(Mutex::new(Access::default()));

        let _access_lock1 = AccessLock::<&mut Name>::new(Arc::clone(&access));
        let _access_lock2 = AccessLock::<(&Name, &Age)>::new(Arc::clone(&access));
    }

    // Lock component for writing, then try to lock it again for writing.
    #[test]
    #[should_panic(expected = "Cannot add write lock for Name as it is already locked for reading or writing")]
    fn write_write_conflict() {
        let access = Arc::new(Mutex::new(Access::default()));

        let _access_lock1 = AccessLock::<&mut Name>::new(Arc::clone(&access));
        let _access_lock2 = AccessLock::<(&mut Name, &mut Age)>::new(Arc::clone(&access));
    }
}
