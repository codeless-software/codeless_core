//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::sync::Arc;

use parking_lot::Mutex;

use crate::{
    archetype::Archetype,
    query::{fetch::Fetch, Access, AccessLock, WorldQuery},
};

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub struct QueryIter<'world, Q: WorldQuery> {
    archetypes:      &'world [Archetype],
    archetype_index: usize,
    position:        usize,
    len:             usize,
    fetch:           Option<Q::Fetch>,

    _access_lock: AccessLock<Q>,
}

impl<'world, Q: WorldQuery> QueryIter<'world, Q> {
    #[inline]
    pub(crate) fn new(archetypes: &'world [Archetype], access_lock: Arc<Mutex<Access>>) -> Self {
        Self {
            archetypes,
            archetype_index: 0,
            position: 0,
            len: 0,
            fetch: None,

            _access_lock: AccessLock::new(access_lock),
        }
    }
}

impl<'world, Q: WorldQuery> Iterator for QueryIter<'world, Q> {
    type Item = <Q::Fetch as Fetch<'world>>::Item;

    // TODO: Can this function be sped up by filtering out invalid archetypes?
    //       That way we don't need to keep looping.
    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if self.position >= self.len {
                let archetype = self.archetypes.get(self.archetype_index)?;
                self.archetype_index += 1;

                // Continue if there are no entities assigned to this archetype.
                if archetype.is_empty() {
                    continue;
                }

                // SAFETY: The access lock ensures that there cannot be multiple mutable references to the
                //         components in an archetype.
                self.fetch = if let Some(v) = unsafe { Q::Fetch::get(archetype) } {
                    self.position = 0;
                    self.len = archetype.len();
                    Some(v)
                } else {
                    continue;
                };
            }

            // SAFETY: The position has already been checked to ensure it does not exceed the
            //         size of the underlying array.
            let item = unsafe { self.fetch.as_ref()?.fetch(self.position) };
            self.position += 1;

            return Some(item);
        }
    }
}

impl<'world, Q: WorldQuery> ExactSizeIterator for QueryIter<'world, Q> {
    fn len(&self) -> usize {
        self.archetypes
            .iter()
            // SAFETY: The access lock ensures that there cannot be multiple mutable references to the
            //         components in an archetype.
            .filter(|&archetype| unsafe { Q::Fetch::get(archetype).is_some() })
            .map(Archetype::len)
            .sum()
    }
}
