//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::ops::{Deref, DerefMut, Index, IndexMut};

use crate::component::{Component, ComponentIndex};

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub struct ComponentSlice<'a, T: Component> {
    components: &'a [T],
}

impl<'a, T: Component> ComponentSlice<'a, T> {
    pub(crate) const fn new(components: &'a [T]) -> Self {
        Self { components }
    }

    pub const fn into_slice(self) -> &'a [T] {
        self.components
    }
}

impl<'a, T: Component> Deref for ComponentSlice<'a, T> {
    type Target = [T];

    fn deref(&self) -> &Self::Target {
        self.components
    }
}

impl<'a, T: Component> Index<ComponentIndex> for ComponentSlice<'a, T> {
    type Output = T;

    fn index(&self, index: ComponentIndex) -> &Self::Output {
        &self.components[index.0]
    }
}

//--------------------------------------------------------------------------------------------------

pub struct ComponentSliceMut<'a, T: Component> {
    components: &'a mut [T],
}

impl<'a, T: Component> ComponentSliceMut<'a, T> {
    pub(crate) fn new(components: &'a mut [T]) -> Self {
        Self { components }
    }

    pub fn into_slice(self) -> &'a mut [T] {
        self.components
    }
}

impl<'a, T: Component> Deref for ComponentSliceMut<'a, T> {
    type Target = [T];

    fn deref(&self) -> &Self::Target {
        self.components
    }
}

impl<'a, T: Component> DerefMut for ComponentSliceMut<'a, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.components
    }
}

impl<'a, T: Component> Index<ComponentIndex> for ComponentSliceMut<'a, T> {
    type Output = T;

    fn index(&self, index: ComponentIndex) -> &Self::Output {
        &self.components[index.0]
    }
}

impl<'a, T: Component> IndexMut<ComponentIndex> for ComponentSliceMut<'a, T> {
    fn index_mut(&mut self, index: ComponentIndex) -> &mut Self::Output {
        &mut self.components[index.0]
    }
}
