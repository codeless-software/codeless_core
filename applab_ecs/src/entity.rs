//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::any::{type_name, Any, TypeId};

use applab_util::CastU8;
use thiserror::Error;

use crate::{
    archetype::{ArchetypeAppender, ArchetypeBuilder, ArchetypeIndex},
    component::{Component, ComponentIndex},
    query::{FetchEntity, Lockable, WorldQuery},
    World,
};

//--------------------------------------------------------------------------------------------------
//-- ENUMS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[derive(Error, Copy, Clone, Debug)]
pub enum ComponentError {
    #[error("Entity does not contain the component {component_name}")]
    NotFound {
        component_type: TypeId,
        component_name: &'static str,
    },
}

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
pub struct Entity(pub usize);

impl Lockable for Entity {}

impl WorldQuery for Entity {
    type Fetch = FetchEntity;
}

//--------------------------------------------------------------------------------------------------

/// Location of an Entity within the [World].
#[derive(Debug, Copy, Clone, PartialOrd, PartialEq, Eq)]
pub struct EntityLocation {
    /// The archetype index.
    archetype: ArchetypeIndex,

    /// Index of the entity in the archetype.
    index: ComponentIndex,
}

impl EntityLocation {
    /// Create a new `EntityLocation`
    ///
    /// # Arguments
    ///
    /// * `archetype`: Archetype index
    /// * `index`: Index within the archetype
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_ecs::archetype::ArchetypeIndex;
    /// # use applab_ecs::component::ComponentIndex;
    /// # use applab_ecs::entity::EntityLocation;
    /// EntityLocation::new(ArchetypeIndex(1), ComponentIndex(0));
    /// ```
    pub const fn new(archetype: ArchetypeIndex, index: ComponentIndex) -> Self {
        Self { archetype, index }
    }

    /// Return the archetype index of the `EntityLocation`
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_ecs::archetype::ArchetypeIndex;
    /// # use applab_ecs::component::ComponentIndex;
    /// # use applab_ecs::entity::EntityLocation;
    /// let location = EntityLocation::new(ArchetypeIndex(1), ComponentIndex(0));
    /// assert_eq!(location.archetype(), ArchetypeIndex(1));
    /// ```
    #[inline]
    pub const fn archetype(&self) -> ArchetypeIndex {
        self.archetype
    }

    /// Return the component index of the `EntityLocation`
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_ecs::archetype::ArchetypeIndex;
    /// # use applab_ecs::component::ComponentIndex;
    /// # use applab_ecs::entity::EntityLocation;
    /// let location = EntityLocation::new(ArchetypeIndex(1), ComponentIndex(0));
    /// assert_eq!(location.index(), ComponentIndex(0));
    /// ```
    #[inline]
    pub const fn index(&self) -> ComponentIndex {
        self.index
    }
}

//--------------------------------------------------------------------------------------------------

/// Provides read-only access to an [Entity].
pub struct EntityRef<'w> {
    world:    &'w World,
    entity:   Entity,
    location: EntityLocation,
}

impl<'w> EntityRef<'w> {
    #[inline]
    pub(crate) const fn new(world: &'w World, entity: Entity, location: EntityLocation) -> Self {
        Self {
            world,
            entity,
            location,
        }
    }

    #[inline]
    pub const fn id(&self) -> Entity {
        self.entity
    }

    /// Get an immutable reference to a component.
    ///
    /// # Examples
    /// ```
    /// # use applab_ecs::entity::Entity;
    /// # use applab_ecs::World;
    /// # use applab_ecs_macros::Component;
    /// # #[derive(Component)]
    /// # struct Name(String);
    /// # let mut world = World::default();
    /// world.spawn_batched(vec![Name("John".to_owned())]);
    /// let entity = world.get_entity(Entity(0)).unwrap();
    /// assert_eq!(entity.get_component::<Name>().unwrap().0, "John".to_owned());
    /// ```
    pub fn get_component<T: Component>(&self) -> Result<&T, ComponentError> {
        get_component(self.world, self.location)
    }
}

//--------------------------------------------------------------------------------------------------

/// Provides mutable access to an [Entity].
///
/// It provides easy access to the underlying components, as well as various other utility functions.
pub struct EntityMut<'w> {
    world:    &'w mut World,
    entity:   Entity,
    location: EntityLocation,
}

impl<'w> EntityMut<'w> {
    /// Create a new `EntityMut`.
    pub(crate) fn new(world: &'w mut World, entity: Entity, location: EntityLocation) -> Self {
        Self {
            world,
            entity,
            location,
        }
    }

    #[inline]
    pub const fn id(&self) -> Entity {
        self.entity
    }

    /// Get an immutable reference to a component.
    ///
    /// # Examples
    /// ```
    /// # use applab_ecs::entity::Entity;
    /// # use applab_ecs::World;
    /// # use applab_ecs_macros::Component;
    /// # #[derive(Component)]
    /// # struct Name(String);
    /// # let mut world = World::default();
    /// world.spawn_batched(vec![Name("John".to_owned())]);
    /// let entity = world.get_entity_mut(Entity(0)).unwrap();
    ///
    /// assert_eq!(entity.get_component::<Name>().unwrap().0, "John".to_owned());
    /// ```
    pub fn get_component<T: Component>(&self) -> Result<&T, ComponentError> {
        get_component(self.world, self.location)
    }

    /// Get a mutable reference to a component.
    ///
    /// # Examples
    /// ```
    /// # use applab_ecs::entity::Entity;
    /// # use applab_ecs::World;
    /// # use applab_ecs_macros::Component;
    /// # #[derive(Component)]
    /// # struct Name(String);
    /// # let mut world = World::default();
    /// world.spawn_batched(vec![Name("John".to_owned())]);
    ///
    /// let mut entity = world.get_entity_mut(Entity(0)).unwrap();
    /// *entity.get_component_mut::<Name>().unwrap() = Name("FooBar".to_owned());
    /// # assert_eq!(entity.get_component::<Name>().unwrap().0, "FooBar".to_owned());
    /// ```
    pub fn get_component_mut<T: Component>(&mut self) -> Result<&mut T, ComponentError> {
        let archetype = self.location.archetype();
        let component = self.location.index();

        self.world
            .archetypes_mut()
            .get_mut(archetype)
            .get_mut::<T>()
            .and_then(move |slice| slice.into_slice().get_mut(component.0))
            .ok_or_else(|| ComponentError::NotFound {
                component_type: TypeId::of::<T>(),
                component_name: type_name::<T>(),
            })
    }

    /// Add a new component to the entity.
    ///
    /// If the component already exists, its value will be replaced with the new one.
    ///
    /// # Examples
    /// ```
    /// # use applab_ecs::entity::Entity;
    /// # use applab_ecs::World;
    /// # use applab_ecs_macros::Component;
    /// # #[derive(Component)]
    /// # struct MyComponent(u32);
    /// # let mut world = World::default();
    /// world.spawn_batched(vec![()]);
    ///
    /// let mut entity = world.get_entity_mut(Entity(0)).unwrap();
    /// entity.add_component(MyComponent(42_u32));
    /// # assert_eq!(entity.get_component::<MyComponent>().unwrap().0, 42);
    /// ```
    pub fn add_component<T: Component>(&mut self, component: T) -> &mut Self {
        // If the component already exists, replace it and return
        if let Ok(comp) = self.get_component_mut::<T>() {
            *comp = component;
            return self;
        }

        // Ensure the type is registered with the world
        self.world.register_component::<T>();

        // Get the target archetype index, creating a new archetype if required
        let target_archetype = {
            let components = &self.world.component_info;
            let archetypes = &mut self.world.archetype_container;

            let mut layout = archetypes.get(self.location.archetype()).types.clone();
            layout.insert(component.type_id());

            archetypes.get_or_insert(layout, components)
        };

        // Transfer component from the current archetype to the target one
        // SAFETY: The from and to archetypes are guaranteed to exist, and the component index is in bounds.
        let target_index = unsafe {
            self.world
                .transfer_archetype(self.location.archetype(), target_archetype, self.location.index())
        };

        // Copy the new component to the target archetype
        // SAFETY: The component has correct memory layout for the BlobVec.
        unsafe {
            self.world
                .archetypes_mut()
                .get_mut(target_archetype)
                .components
                .get_mut(&component.type_id())
                .unwrap()
                .push(component.cast_u8());
            std::mem::forget(component);
        }

        self.world.archetypes().validate();
        self.location = EntityLocation::new(target_archetype, target_index);
        self
    }

    /// Add a batch of components to the Entity.
    pub fn add_bundle<C>(&mut self, components: C) -> &mut Self
    where
        C: ArchetypeAppender + ArchetypeBuilder,
    {
        C::register_types(self.world);
        let archetype = self.location.archetype();

        // Find the new target archetype.
        let target_archetype_index = {
            let archetypes = &mut self.world.archetype_container;
            let component_info = &self.world.component_info;

            let mut layout = C::types();
            layout.extend(&archetypes.get(archetype).types);

            archetypes.get_or_insert(layout, component_info)
        };

        // Transfer all existing component from the old archetype to the new archetype.
        // SAFETY: The archetype from and to indexes as well as the component index are guaranteed
        //         to be in scope.
        //         All remaining component types to be added to the new archetype are done so directly
        //         after this call.
        let target_index = unsafe {
            self.world
                .transfer_archetype(archetype, target_archetype_index, self.location.index)
        };

        // Add all new components to the new archetype.
        let target_archetype = self.world.archetypes_mut().get_mut(target_archetype_index);
        C::append_components_indexed(std::iter::once(components), target_index, target_archetype);

        self.world.archetypes().validate();
        self.location = EntityLocation::new(target_archetype_index, target_index);

        self
    }

    /// Remove a component from the entity.
    ///
    /// # Examples
    /// ```
    /// # use applab_ecs::entity::Entity;
    /// # use applab_ecs::World;
    /// # use applab_ecs_macros::Component;
    /// # #[derive(Component)]
    /// # struct Name(String);
    /// # let mut world = World::default();
    /// world.spawn_batched(vec![Name("John".to_owned())]);
    /// let mut entity = world.get_entity_mut(Entity(0)).unwrap();
    ///
    /// # assert!(entity.get_component::<Name>().is_ok());
    /// entity.remove_component::<Name>();
    /// # assert!(entity.get_component::<Name>().is_err());
    /// ```
    pub fn remove_component<T: Component>(&mut self) -> &mut Self {
        // Get the target archetype index, creating a new archetype if required
        let target_archetype = {
            let components = &self.world.component_info;
            let archetypes = &mut self.world.archetype_container;

            let mut layout = archetypes.get(self.location.archetype()).types.clone();
            layout.remove(&TypeId::of::<T>());

            archetypes.get_or_insert(layout, components)
        };

        // Transfer component from the current archetype to the target one
        // SAFETY: The from and to archetypes are guaranteed to exist, and the component index is in bounds.
        let target_index = unsafe {
            self.world
                .transfer_archetype(self.location.archetype(), target_archetype, self.location.index())
        };

        self.world.archetypes().validate();
        self.location = EntityLocation::new(target_archetype, target_index);
        self
    }

    /// Despawn the entity, removing all of its components from the world.
    ///
    /// # Examples
    /// ```
    /// # use applab_ecs::entity::Entity;
    /// # use applab_ecs::World;
    /// # let mut world = World::default();
    /// world.spawn_batched(vec![()]);
    /// # assert!(world.get_entity(Entity(0)).is_some());
    /// world.get_entity_mut(Entity(0)).unwrap().despawn();
    /// # assert!(world.get_entity(Entity(0)).is_none());
    /// ```
    pub fn despawn(self) {
        let world = self.world;
        let index = self.location.index.0;

        // Remove all components from the archetype
        let archetype = world.archetype_container.get_for_entity_mut(&self.location);
        for t in &archetype.types {
            archetype.components.get_mut(t).unwrap().swap_drop(index);
        }
        archetype.entities.swap_remove(index);

        // SAFETY: And entity has already been removed from this archetype, so its length needs to be decreased.
        //         Not doing so could cause a buffer overflow.
        unsafe { archetype.set_len(archetype.len() - 1) };

        // Remove the entity from the world and update locations
        world.entities.swap_remove(self.entity.0);

        if archetype.len() > index {
            world.entities[self.entity.0] = self.location;
        }

        world.archetypes().validate();
    }
}

//--------------------------------------------------------------------------------------------------
//-- PRIVATE FUNCTIONS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

fn get_component<T: Component>(world: &World, location: EntityLocation) -> Result<&T, ComponentError> {
    let archetype = location.archetype();
    let component = location.index();

    world
        .archetypes()
        .get(archetype)
        .get::<T>()
        .and_then(move |slice| slice.into_slice().get(component.0))
        .ok_or_else(|| ComponentError::NotFound {
            component_type: TypeId::of::<T>(),
            component_name: type_name::<T>(),
        })
}

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[allow(clippy::unwrap_used)]
#[cfg(test)]
mod tests {
    use applab_ecs_macros::Component;

    use crate as applab_ecs;
    use crate::{
        archetype::ArchetypeIndex,
        component::ComponentIndex,
        entity::{Entity, EntityLocation},
        world::WorldError,
        World,
    };

    #[derive(Debug, Component)]
    struct Name(String);

    #[derive(Debug, Component)]
    struct Age(u8);

    #[derive(Debug, PartialEq, Component)]
    enum Colour {
        Red,
        Blue,
        Green,
    }

    #[test]
    fn add_remove_component() -> Result<(), WorldError> {
        let mut world = World::default();

        let components1 = vec![(Name("Foo".to_owned()), Age(8)), (Name("Bar".to_owned()), Age(16))];
        let components2 = vec![Name("John".to_owned()), Name("Jane".to_owned()), Name("Doe".to_owned())];

        /*
        |---- Archetype 0 ----|---- Archetype 1 ----|
        | Foo   (0)           | John  (2)           |
        | Bar   (1)           | Jane  (3)           |
        |                     | Doe   (4)           |
        |---------------------|---------------------|
        */

        world.spawn_batched(components1);
        world.spawn_batched(components2);

        /*
        |---- Archetype 0 ----|---- Archetype 1 ----|
        | Foo   (0)           | Doe   (4)           |
        | Bar   (1)           | Jane  (3)           |
        | John  (2)           |                     |
        |---------------------|---------------------|
        */

        let mut e = world.get_entity_mut(Entity(2)).unwrap();
        e.add_component(Age(42));

        // Check John
        assert_eq!(e.location, EntityLocation::new(ArchetypeIndex(1), ComponentIndex(2)));
        assert_eq!(e.get_component::<Name>().unwrap().0, "John".to_owned());

        // Check container length
        assert_eq!(world.archetype_container.len(), 3);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(1)).len(), 3);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(2)).len(), 2);

        /*
        |---- Archetype 0 ----|---- Archetype 1 ----|---- Archetype 2 ----|
        | Foo   (0)           | Doe   (4)           | Bar   (1)           |
        | John  (2)           | Jane  (3)           |                     |
        |                     |                     |                     |
        |---------------------|---------------------|---------------------|
        */

        let mut e = world.get_entity_mut(Entity(1)).unwrap();
        e.add_component(Colour::Green);

        // Check Bar
        let e = world.get_entity(Entity(1)).unwrap();
        assert_eq!(e.location, EntityLocation::new(ArchetypeIndex(3), ComponentIndex(0)));
        assert_eq!(e.get_component::<Name>().unwrap().0, "Bar".to_owned());

        // Check John
        let e = world.get_entity(Entity(2)).unwrap();
        assert_eq!(e.location, EntityLocation::new(ArchetypeIndex(1), ComponentIndex(1)));
        assert_eq!(e.get_component::<Name>().unwrap().0, "John".to_owned());

        // Check container length
        assert_eq!(world.archetype_container.len(), 4);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(1)).len(), 2);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(2)).len(), 2);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(3)).len(), 1);

        /*
        |---- Archetype 0 ----|---- Archetype 1 ----|---- Archetype 2 ----|
        |                     | Doe   (4)           | Bar   (1)           |
        |                     | Jane  (3)           | Foo   (0)           |
        |                     |                     | John  (2)           |
        |---------------------|---------------------|---------------------|
        */

        let mut e = world.get_entity_mut(Entity(0)).unwrap();
        e.add_component(Colour::Blue);

        let mut e = world.get_entity_mut(Entity(2)).unwrap();
        e.add_component(Colour::Red);

        // Check Foo
        let e = world.get_entity(Entity(0)).unwrap();
        assert_eq!(e.location, EntityLocation::new(ArchetypeIndex(3), ComponentIndex(1)));
        assert_eq!(e.get_component::<Name>().unwrap().0, "Foo".to_owned());

        // Check John
        let e = world.get_entity(Entity(2)).unwrap();
        assert_eq!(e.location, EntityLocation::new(ArchetypeIndex(3), ComponentIndex(2)));
        assert_eq!(e.get_component::<Name>().unwrap().0, "John".to_owned());

        // Check container length
        assert_eq!(world.archetype_container.len(), 4);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(1)).len(), 0);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(2)).len(), 2);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(3)).len(), 3);

        /*
        |---- Archetype 0 ----|---- Archetype 1 ----|---- Archetype 2 ----|
        |                     | Doe   (4)           | John  (2)           |
        |                     | Jane  (3)           |                     |
        |                     | Bar   (1)           |                     |
        |                     | Foo   (0)           |                     |
        |---------------------|---------------------|---------------------|
        */

        let mut e = world.get_entity_mut(Entity(0)).unwrap();
        e.remove_component::<Age>();
        e.remove_component::<Colour>();

        let mut e = world.get_entity_mut(Entity(1)).unwrap();
        e.remove_component::<Age>();
        e.remove_component::<Colour>();

        // Check Foo
        let e = world.get_entity(Entity(0)).unwrap();
        assert_eq!(e.location, EntityLocation::new(ArchetypeIndex(2), ComponentIndex(2)));
        assert_eq!(e.get_component::<Name>().unwrap().0, "Foo".to_owned());

        // Check Bar
        let e = world.get_entity(Entity(1)).unwrap();
        assert_eq!(e.location, EntityLocation::new(ArchetypeIndex(2), ComponentIndex(3)));
        assert_eq!(e.get_component::<Name>().unwrap().0, "Bar".to_owned());

        // Check John
        let e = world.get_entity(Entity(2)).unwrap();
        assert_eq!(e.location, EntityLocation::new(ArchetypeIndex(3), ComponentIndex(0)));
        assert_eq!(e.get_component::<Name>().unwrap().0, "John".to_owned());

        // Check container length
        // NOTE: The archetype len is 4 as a new archetype was created when `Age` was removed from
        //       Foo and Bar.
        assert_eq!(world.archetype_container.len(), 5);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(1)).len(), 0);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(2)).len(), 4);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(3)).len(), 1);

        Ok(())
    }

    #[test]
    fn add_bundle() {
        #[derive(Component)]
        struct Foo;

        #[derive(Component)]
        struct Bar;

        let mut world = World::default();

        /*
        |---- Archetype 0 ----|
        | John  (0)           |
        |                     |
        |---------------------|
        */

        // Create new entity
        let entity1 = world.spawn().entity;

        assert_eq!(world.archetype_container.len(), 1);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(0)).len(), 1);

        // Check Entity1 location
        assert_eq!(world.entities[0].archetype, ArchetypeIndex(0));
        assert_eq!(world.entities[0].index, ComponentIndex(0));

        /*
        |---- Archetype 0 ----|---- Archetype 1 ----|
        |                     | John  (0)           |
        |                     |                     |
        |---------------------|---------------------|
        */

        // Add Name and Age component to Entity1.
        let mut entity = world.get_entity_mut(entity1).unwrap();
        entity.add_bundle((Name("John".to_owned()), Age(30)));

        assert_eq!(entity.get_component::<Name>().unwrap().0, "John".to_owned());
        assert_eq!(entity.get_component::<Age>().unwrap().0, 30);

        assert_eq!(world.archetype_container.len(), 2);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(0)).len(), 0);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(1)).len(), 1);

        // Check Entity1 location
        assert_eq!(world.entities[0].archetype, ArchetypeIndex(1));
        assert_eq!(world.entities[0].index, ComponentIndex(0));

        /*
        |---- Archetype 0 ----|---- Archetype 1 ----|---- Archetype 2 ----|
        |                     |                     | John  (0)           |
        |                     |                     |                     |
        |---------------------|---------------------|---------------------|
        */

        // Add Colour, Foo, and Bar components to Entity1.
        let mut entity = world.get_entity_mut(entity1).unwrap();
        entity.add_bundle((Colour::Blue, Foo, Bar));

        assert_eq!(entity.get_component::<Name>().unwrap().0, "John".to_owned());
        assert_eq!(entity.get_component::<Age>().unwrap().0, 30);
        assert_eq!(*entity.get_component::<Colour>().unwrap(), Colour::Blue);
        assert!(entity.get_component::<Foo>().is_ok());
        assert!(entity.get_component::<Bar>().is_ok());

        assert_eq!(world.archetype_container.len(), 3);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(0)).len(), 0);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(1)).len(), 0);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(2)).len(), 1);

        // Check Entity1 location
        assert_eq!(world.entities[0].archetype, ArchetypeIndex(2));
        assert_eq!(world.entities[0].index, ComponentIndex(0));

        /*
        |---- Archetype 0 ----|---- Archetype 1 ----|---- Archetype 2 ----|---- Archetype 3 ----|
        |                     |                     | John  (0)           | Mary  (1)           |
        |                     |                     |                     |                     |
        |---------------------|---------------------|---------------------|---------------------|
        */

        // Spawn Entity2 with a Colour, Name, and Age component.
        let entity2 = world
            .spawn()
            .add_bundle((Colour::Red, Name("Mary".to_owned()), Age(30)))
            .entity;

        assert_eq!(world.archetype_container.len(), 4);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(0)).len(), 0);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(1)).len(), 0);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(2)).len(), 1);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(3)).len(), 1);

        // Check Entity1 location
        assert_eq!(world.entities[0].archetype, ArchetypeIndex(2));
        assert_eq!(world.entities[0].index, ComponentIndex(0));

        // Check Entity2 location
        assert_eq!(world.entities[1].archetype, ArchetypeIndex(3));
        assert_eq!(world.entities[1].index, ComponentIndex(0));

        /*
        |---- Archetype 0 ----|---- Archetype 1 ----|---- Archetype 2 ----|---- Archetype 3 ----|
        |                     |                     | John  (0)           |                     |
        |                     |                     | Mary  (1)           |                     |
        |---------------------|---------------------|---------------------|---------------------|
        */

        // Add a Foo and Bar component to Entity2, and replace its Colour component
        let mut entity = world.get_entity_mut(entity2).unwrap();
        entity.add_bundle((Colour::Green, Foo, Bar));

        assert_eq!(entity.get_component::<Name>().unwrap().0, "Mary".to_owned());
        assert_eq!(entity.get_component::<Age>().unwrap().0, 30);
        assert_eq!(*entity.get_component::<Colour>().unwrap(), Colour::Green);
        assert!(entity.get_component::<Foo>().is_ok());
        assert!(entity.get_component::<Bar>().is_ok());

        assert_eq!(world.archetype_container.len(), 4);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(0)).len(), 0);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(1)).len(), 0);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(2)).len(), 2);
        assert_eq!(world.archetype_container.get(ArchetypeIndex(3)).len(), 0);

        // Check Entity1 location
        assert_eq!(world.entities[0].archetype, ArchetypeIndex(2));
        assert_eq!(world.entities[0].index, ComponentIndex(0));

        // Check Entity2 location
        assert_eq!(world.entities[1].archetype, ArchetypeIndex(2));
        assert_eq!(world.entities[1].index, ComponentIndex(1));
    }

    #[test]
    fn despawn() {
        let mut world = World::default();

        let components = vec![Name("John".to_owned()), Name("Jane".to_owned()), Name("Doe".to_owned())];

        world.spawn_batched(components);

        assert_eq!(world.entities.len(), 3);
        assert_eq!(world.archetypes().get(ArchetypeIndex(1)).len(), 3);

        // Despawn the "Jane" entity
        world.get_entity_mut(Entity(1)).unwrap().despawn();

        assert_eq!(world.entities.len(), 2);
        assert_eq!(world.archetypes().get(ArchetypeIndex(1)).len(), 2);

        assert_eq!(
            world.get_entity(Entity(0)).unwrap().get_component::<Name>().unwrap().0,
            "John".to_owned()
        );

        assert_eq!(
            world.get_entity(Entity(1)).unwrap().get_component::<Name>().unwrap().0,
            "Doe".to_owned()
        );

        assert!(world.get_entity(Entity(2)).is_none());
    }
}
