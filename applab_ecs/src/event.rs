//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::{fmt::Debug, marker::PhantomData, mem};

use crate as applab_ecs;
use crate::{
    macros::Resource,
    resource::{LocalResMut, Res, ResMut},
};

//--------------------------------------------------------------------------------------------------
//-- TRAITS ----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub trait Event: 'static + Debug {}

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// Stores and manages events of a single type.
///
/// It contains two buffers, which are swapped and cleared on every [`Events::update`], ensuring
/// each [`Event`] lasts two updates.
#[derive(Debug, Resource)]
pub struct Events<T: Event> {
    /// Total event count.
    /// This value increments on every new event of type `T`, and never decreases.
    pub(crate) event_count: usize,

    /// Stores all events created after the last call to [`Events::update`].
    pub(crate) buffer_a: Vec<T>,

    /// Stores all events created before the last call to [`Events::update`].
    pub(crate) buffer_b: Vec<T>,
}

impl<T: Event> Default for Events<T> {
    fn default() -> Self {
        Self {
            event_count: usize::default(),
            buffer_a:    Vec::default(),
            buffer_b:    Vec::default(),
        }
    }
}

impl<T: Event> Events<T> {
    /// Returns the total number of events currently stored.
    #[inline]
    pub fn len(&self) -> usize {
        self.buffer_a.len() + self.buffer_b.len()
    }

    /// Returns whether there are any stored events.
    #[inline]
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// Add a new [`Event`].
    pub fn send(&mut self, event: T) {
        self.buffer_a.push(event);
        self.event_count += 1;
    }

    /// Returns a new reader.
    ///
    /// See [`ManualEventReader`] for more information.
    pub fn get_reader(&self) -> ManualEventReader<T> {
        ManualEventReader::default()
    }

    /// Clears out old events, and swaps the buffers.
    pub fn update(&mut self) {
        self.buffer_b.clear();
        mem::swap(&mut self.buffer_a, &mut self.buffer_b);
    }

    /// System added [`CoreStage::First`] which calls [`Events::update`] at the beginning
    /// of each frame.
    pub fn update_system(mut event: ResMut<Self>) {
        event.update();
    }
}

//--------------------------------------------------------------------------------------------------

/// Allows reading events from within a [`System`].
///
/// # Examples
///
/// ```
/// # use applab_ecs::event::EventReader;
/// # use applab_ecs::macros::Event;
///
/// # #[derive(Debug, Default, Event)]
/// # struct MyEvent;
/// fn my_system(mut reader: EventReader<MyEvent>) {
///     for event in reader.iter() {
///         // Do stuff...
///     }
/// }
/// ```
pub struct EventReader<'w, 's, T: Event> {
    reader: LocalResMut<'s, ManualEventReader<T>>,
    events: Res<'w, Events<T>>,
}

impl<'w, 's, T: Event> EventReader<'w, 's, T> {
    /// Returns `true` if there are any events in the reader.
    #[inline]
    pub fn is_empty(&self) -> bool {
        self.events.is_empty()
    }

    /// Create a new reader.
    pub fn new(reader: LocalResMut<'s, ManualEventReader<T>>, events: Res<'w, Events<T>>) -> Self {
        Self { reader, events }
    }

    /// Return an iterator over the events.
    pub fn iter(&mut self) -> impl Iterator<Item = &'w T> {
        self.reader.iter(self.events.inner)
    }
}

//--------------------------------------------------------------------------------------------------

/// Allows writing events from within a [`System`].
///
/// # Examples
///
/// ```
/// # use applab_ecs::event::EventWriter;
/// # use applab_ecs::macros::Event;
///
/// # #[derive(Debug, Default, Event)]
/// # struct MyEvent;
/// fn my_system(mut writer: EventWriter<MyEvent>) {
///     writer.send(MyEvent);
/// }
/// ```
pub struct EventWriter<'a, T: Event> {
    events: ResMut<'a, Events<T>>,
}

impl<'a, T: Event> EventWriter<'a, T> {
    /// Create a new writer.
    pub fn new(events: ResMut<'a, Events<T>>) -> Self {
        Self { events }
    }

    /// Send a new event.
    pub fn send(&mut self, event: T) {
        self.events.send(event);
    }
}

//--------------------------------------------------------------------------------------------------

#[derive(Debug, Resource)]
pub struct ManualEventReader<T: Event> {
    last_event_count: usize,
    phantom:          PhantomData<T>,
}

impl<T: Event> Default for ManualEventReader<T> {
    fn default() -> Self {
        Self {
            last_event_count: 0,
            phantom:          PhantomData::default(),
        }
    }
}

impl<T: Event> ManualEventReader<T> {
    pub fn iter<'w>(&mut self, events: &'w Events<T>) -> impl Iterator<Item = &'w T> {
        let unread = events.event_count - self.last_event_count;
        let skip = (events.buffer_a.len() + events.buffer_b.len()).saturating_sub(unread);

        let a = &events.buffer_a;
        let b = &events.buffer_b;

        debug_assert!(self.last_event_count + unread == events.event_count);
        self.last_event_count = events.event_count;

        a.iter().chain(b.iter()).skip(skip)
    }
}
