//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::{alloc::dealloc, ptr, sync::Arc};

use applab_util::short_type_name;
use parking_lot::Mutex;
use thiserror::Error;

use crate::{
    archetype::{ArchetypeAppender, ArchetypeBuilder, ArchetypeContainer, ArchetypeIndex},
    component::{Component, ComponentIndex, ComponentInfoContainer},
    entity::{Entity, EntityLocation, EntityMut, EntityRef},
    query::{Access, Query, ReadOnlyFetch, WorldQuery},
    resource::{Res, ResMut, Resource, ResourceContainer},
};

//--------------------------------------------------------------------------------------------------
//-- ENUMS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[derive(Error, Debug)]
pub enum WorldError {
    #[error("entity at index {0} could not be found")]
    InvalidEntityIndex(usize),

    #[error("entity {0:?} could not be found due to an invalid index")]
    InvalidEntityArchetype(Entity),

    #[error("component {0} was not found in this Archetype")]
    TypeNotFoundInArchetype(String),

    #[error("failed to move components from archetype {0:?} to {1:?}")]
    FailedToMoveComponents(EntityLocation, EntityLocation),
}

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// A collection of Entities and Components.
///
/// The `World` allows for adding, removing, and updating both [Entity's](Entity) and their [Component's](Component).
/// Entities can also be efficiently iterated upon via queries.
///
/// See [`World::query`] and [`World::query_mut`] on how to create queries.
#[derive(Debug, Default)]
pub struct World {
    pub(crate) archetype_container: ArchetypeContainer,
    pub(crate) entities:            Vec<EntityLocation>,
    pub(crate) component_info:      ComponentInfoContainer,
    pub(crate) access:              Arc<Mutex<Access>>,
    resources:                      ResourceContainer,
}

// Public functions
impl World {
    /// Returns an [`EntityRef`] for the given `entity`.
    ///
    /// If the entity is invalid, `None` will be returned instead.
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_ecs::World;
    /// # use applab_ecs::entity::Entity;
    /// # use applab_ecs::macros::Component;
    /// # let mut world = World::default();
    /// # #[derive(Component)]
    /// # struct MyComponent;
    /// # world.spawn_batched(vec![MyComponent]);
    /// # let entity = Entity(0);
    /// let entity_ref = world.get_entity(entity).unwrap();
    /// ```
    pub fn get_entity(&self, entity: Entity) -> Option<EntityRef> {
        let location = *self.entities.get(entity.0)?;
        Some(EntityRef::new(self, entity, location))
    }

    /// Returns an [`EntityMut`] for the given `entity`.
    ///
    /// If the entity is invalid, `None` will be returned instead.
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_ecs::World;
    /// # use applab_ecs::entity::Entity;
    /// # use applab_ecs::macros::Component;
    /// # let mut world = World::default();
    /// # #[derive(Component)]
    /// # struct MyComponent;
    /// # world.spawn_batched(vec![MyComponent]);
    /// # let entity = Entity(0);
    /// let entity_mut = world.get_entity_mut(entity).unwrap();
    /// ```
    pub fn get_entity_mut(&mut self, entity: Entity) -> Option<EntityMut> {
        let location = *self.entities.get(entity.0)?;
        Some(EntityMut::new(self, entity, location))
    }

    /// Registers a [`Component`] with the world.
    ///
    /// This is normally handled by the world, and does not need to be invoked manually.
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_ecs::World;
    /// # use applab_ecs_macros::Component;
    /// # #[derive(Component)]
    /// # struct MyComponent;
    /// # let mut world = World::default();
    /// world.register_component::<MyComponent>();
    /// ```
    pub fn register_component<T: Component>(&mut self) {
        self.component_info.insert::<T>();
    }

    /// Spawns a new Entity, returning an `EntityMut` which can be used to add further components.
    ///
    /// # Panics
    /// Panics if the call to `World::get_entity_mut` returns None.
    /// This means that the newly created entity could not be found.
    ///
    /// # Examples
    /// ```
    /// # use applab_ecs::World;
    /// # use applab_ecs_macros::Component;
    /// # #[derive(Component)]
    /// # struct MyComponent;
    /// # let mut world = World::default();
    /// let mut entity = world.spawn();
    /// entity.add_component(MyComponent);
    /// ```
    pub fn spawn(&mut self) -> EntityMut {
        let entity = Entity(self.entities.len());
        let location = self.archetype_container.spawn(entity);

        self.entities.push(location);
        self.archetypes().validate();

        self.get_entity_mut(entity).unwrap_or_else(|| {
            panic!(
                "[BUG] Failed to spawn entity with ID {}. World::get_entity_mut returned None",
                entity.0
            );
        })
    }

    pub fn spawn_batched<C, I>(&mut self, components: I) -> Vec<Entity>
    where
        C: ArchetypeAppender + ArchetypeBuilder,
        I: IntoIterator<Item = C>,
    {
        let iter = components.into_iter();
        let (lower, upper) = iter.size_hint();

        let len = upper.unwrap_or(lower);
        let elen = self.entities.len();

        let entities = (0..len).map(|n| Entity(n + elen));
        let mut locations = self
            .archetype_container
            .spawn_batched(&mut self.component_info, iter, entities.clone());
        self.entities.append(&mut locations);

        self.archetypes().validate();
        entities.collect()
    }

    /// Constructs a query.
    ///
    /// The query will have shared, immutable access to the specified components.
    ///
    /// # Examples
    /// ```
    /// # use applab_ecs::World;
    /// # use applab_ecs::macros::Component;
    /// # #[derive(Component)]
    /// # struct MyComponent;
    /// # let world = World::default();
    /// // Query single component
    /// let query = world.query::<&MyComponent>();
    /// ```
    /// ```
    /// # use applab_ecs::World;
    /// # use applab_ecs_macros::Component;
    /// # #[derive(Component)]
    /// # struct MyComponent1;
    /// # #[derive(Component)]
    /// # struct MyComponent2;
    /// # let world = World::default();
    /// // Query multiple components
    /// let query = world.query::<(&MyComponent1, &MyComponent2)>();
    /// ```
    #[inline]
    pub fn query<Q: WorldQuery>(&self) -> Query<Q>
    where
        Q::Fetch: ReadOnlyFetch,
    {
        Query::new(self)
    }

    /// Constructs a mutable query.
    ///
    /// The query will have exclusive, mutable access to the specified components.
    ///
    /// # Examples
    /// ```
    /// # use applab_ecs::World;
    /// # use applab_ecs::macros::Component;
    /// # #[derive(Component)]
    /// # struct MyComponent;
    /// # let world = World::default();
    /// // Query single component.
    /// let query = world.query_mut::<&mut MyComponent>();
    /// ```
    /// ```
    /// # use applab_ecs::World;
    /// # use applab_ecs_macros::Component;
    /// # #[derive(Component)]
    /// # struct MyComponent1;
    /// # #[derive(Component)]
    /// # struct MyComponent2;
    /// # let world = World::default();
    /// // Query multiple components.
    /// // In this instance, the query will provide mutable access to MyComponent2, while
    /// // MyComponent1 will be immutable.
    /// let query = world.query_mut::<(&MyComponent1, &mut MyComponent2)>();
    /// ```
    #[inline]
    pub fn query_mut<Q: WorldQuery>(&self) -> Query<Q> {
        Query::new(self)
    }

    /// Initialise a resource using the default values.
    ///
    /// # Examples
    /// ```
    /// # use applab_ecs::{World, macros::Resource};
    /// #[derive(Default, Debug, Resource)]
    /// struct MyResource {
    ///     image_path: String,
    /// };
    ///
    /// # impl MyResource {
    /// #     pub fn new(path: &str) -> Self {
    /// #         MyResource { image_path: path.to_owned() }
    /// #     }
    /// # }
    /// let mut world = World::default();
    /// world.init_resource::<MyResource>();
    /// ```
    pub fn init_resource<T>(&mut self)
    where
        T: Resource + Default,
    {
        self.add_resource(T::default());
    }

    /// Adds a new resource.
    ///
    /// # Arguments
    ///
    /// * `resource`: Resource to add to the `World`.
    ///
    /// # Panics
    /// Panics if the resource already exists.
    /// To replace the resource, please refer to [`World::replace_resource`].
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_ecs::World;
    /// # use applab_ecs::macros::Resource;
    /// #[derive(Debug, Resource)]
    /// struct MyResource {
    ///     image_path: String,
    /// };
    ///
    /// # impl MyResource {
    /// #     pub fn new(path: &str) -> Self {
    /// #         MyResource { image_path: path.to_owned() }
    /// #     }
    /// # }
    /// let mut world = World::default();
    /// world.add_resource(MyResource::new("path/to/image.png"));
    /// ```
    pub fn add_resource<T: Resource>(&mut self, resource: T) {
        if !self.resources.add(resource) {
            let type_name = short_type_name::<T>();
            panic!("Resource '{type_name}' already exists.\nIf you want to replace it, call replace_resource instead");
        }
    }

    /// Adds a resource to the `World`, replacing existing resources of the same type if present.
    ///
    /// # Arguments
    ///
    /// * `resource`: Resource to add to the `World`.
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_ecs::World;
    /// # use applab_ecs::macros::Resource;
    /// #[derive(Debug, Resource)]
    /// struct MyResource {
    ///     image_path: String,
    /// };
    ///
    /// # impl MyResource {
    /// #     pub fn new(path: &str) -> Self {
    /// #         MyResource { image_path: path.to_owned() }
    /// #     }
    /// # }
    /// let mut world = World::default();
    /// world.replace_resource(MyResource::new("path/to/image.png"));
    /// ```
    pub fn replace_resource<T: Resource>(&mut self, resource: T) {
        self.resources.replace(resource);
    }

    pub fn remove_resource<T: Resource>(&mut self) {
        self.resources.remove::<T>();
    }

    /// Return a [`Res<T>`](Res) where `T` is the resource to fetch.
    ///
    /// # Panics
    /// Panics if the resource `T` does not exist.
    /// For the non-panicking function, please see [`World::get_resource`].
    #[must_use]
    pub fn resource<T: Resource>(&self) -> Res<T> {
        let type_name = short_type_name::<T>();
        self.resources
            .get()
            .map_or_else(|| panic!("Requested resource `{}` not found", type_name), |res| res)
    }

    /// Return a [`ResMut<T>`](ResMut) where `T` is the resource to fetch.
    ///
    /// # Panics
    /// Panics if the resource `T` does not exist.
    /// For the non-panicking function, please see [`World::get_resource_mut`].
    #[must_use]
    pub fn resource_mut<T: Resource>(&self) -> ResMut<T> {
        let type_name = short_type_name::<T>();
        self.resources
            .get_mut()
            .map_or_else(|| panic!("Requested resource `{}` not found", type_name), |res| res)
    }

    #[must_use]
    pub fn get_resource<T: Resource>(&self) -> Option<Res<T>> {
        self.resources.get()
    }

    #[must_use]
    pub fn get_resource_mut<T: Resource>(&self) -> Option<ResMut<T>> {
        self.resources.get_mut()
    }

    #[must_use]
    pub fn has_resource<T: Resource>(&self) -> bool {
        self.resources.contains::<T>()
    }

    /// Moves an [`Entity`] and its components from one [`Archetype`] to another
    pub(crate) unsafe fn transfer_archetype(
        &mut self,
        ArchetypeIndex(from): ArchetypeIndex,
        ArchetypeIndex(to): ArchetypeIndex,
        ComponentIndex(idx): ComponentIndex,
    ) -> ComponentIndex {
        if from == to {
            return ComponentIndex(idx);
        }

        // Find the archetypes
        let (from_arch, to_arch) = if from < to {
            let (a, b) = self.archetype_container.get_inner_mut().split_at_mut(to);
            (&mut a[from], &mut b[0])
        } else {
            let (a, b) = self.archetype_container.get_inner_mut().split_at_mut(from);
            (&mut b[0], &mut a[to])
        };

        // Move the entity ID
        let entity = from_arch.entities.swap_remove(idx);
        let new_idx = to_arch.allocate(entity);

        self.entities[entity.0] = EntityLocation::new(ArchetypeIndex(to), new_idx);

        if from_arch.len() - 1 > idx {
            let moved = from_arch.entities[idx];
            self.entities[moved.0] = EntityLocation::new(ArchetypeIndex(from), ComponentIndex(idx));
        }

        // Move components
        for key in &from_arch.types {
            let from_component = from_arch.components.get_mut(key).unwrap();

            // If the destination archetype contains the component, copy it.
            if let Some(to_component) = to_arch.components.get_mut(key) {
                let to_component = to_component;
                if let Some(data) = from_component.swap_remove(idx) {
                    to_component.push(data);
                    dealloc(data, to_component.layout());
                } else if to_component.is_zero() {
                    to_component.push(ptr::null());
                }
            }
            // If the destination archetype does not contain the component, drop it.
            else {
                from_component.swap_drop(idx);
            }
        }

        from_arch.set_len(from_arch.len() - 1);
        new_idx
    }

    #[inline]
    pub const fn archetypes(&self) -> &ArchetypeContainer {
        &self.archetype_container
    }

    #[inline]
    pub(crate) fn archetypes_mut(&mut self) -> &mut ArchetypeContainer {
        &mut self.archetype_container
    }

    #[inline]
    pub(crate) const fn component_info(&self) -> &ComponentInfoContainer {
        &self.component_info
    }
}

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[cfg(test)]
#[allow(clippy::unwrap_used, clippy::indexing_slicing)]
mod tests {
    use std::fmt::Debug;

    use applab_ecs_macros::Component;

    use crate as applab_ecs;
    use crate::{entity::Entity, macros::Resource, world::WorldError, World};

    #[derive(Debug, Component)]
    struct Name(String);

    #[derive(Debug, Component)]
    struct Age(u8);

    #[derive(Debug, PartialEq, Eq, Component)]
    enum Colour {
        Red,
        Blue,
        Green,
    }

    #[derive(Debug, Resource)]
    struct Counter(u32);

    #[test]
    fn spawn() {
        let mut world = World::default();
        assert_eq!(world.archetypes().len(), 1);

        let entity = world.spawn().id();
        assert_eq!(entity.0, 0);
        assert_eq!(world.archetypes().len(), 1);
        assert_eq!(world.archetypes().get_inner()[0].len(), 1);

        let entity = world
            .spawn()
            .add_component(Name("Foo".to_owned()))
            .add_component(Age(30))
            .id();

        assert_eq!(entity.0, 1);
        assert_eq!(world.archetypes().len(), 3); // There are 3 archetypes as we added two components without batching.
        assert_eq!(world.archetypes().get_inner()[0].len(), 1);
        assert_eq!(world.archetypes().get_inner()[1].len(), 0);
        assert_eq!(world.archetypes().get_inner()[2].len(), 1);
    }

    #[test]
    fn spawn_batched() {
        let mut world = World::default();

        let components1 = vec![(Name("Foo".to_owned()), Age(8)), (Name("Bar".to_owned()), Age(16))];
        let components2 = vec![Name("John".to_owned()), Name("Doe".to_owned())];

        let res1 = world.spawn_batched(components1);
        let res2 = world.spawn_batched(components2);

        assert_eq!(world.entities.len(), 4);
        assert_eq!(res1.len(), 2);
        assert_eq!(res2.len(), 2);

        assert_eq!(world.entities[res1[0].0].archetype().0, 1);
        assert_eq!(world.entities[res1[1].0].archetype().0, 1);
        assert_eq!(world.entities[res1[0].0].index().0, 0);
        assert_eq!(world.entities[res1[1].0].index().0, 1);

        assert_eq!(world.entities[res2[0].0].archetype().0, 2);
        assert_eq!(world.entities[res2[1].0].archetype().0, 2);
        assert_eq!(world.entities[res2[0].0].index().0, 0);
        assert_eq!(world.entities[res2[1].0].index().0, 1);
    }

    #[test]
    fn query() -> Result<(), WorldError> {
        let mut world = World::default();

        let components1 = vec![Name("John".to_owned()), Name("Jane".to_owned()), Name("Doe".to_owned())];

        let components2 = vec![
            (Name("Mike".to_owned()), Age(40)),
            (Name("Mary".to_owned()), Age(24)),
            (Name("Max".to_owned()), Age(86)),
        ];

        let components3 = vec![Colour::Red, Colour::Green, Colour::Blue];

        world.spawn_batched(components1);
        world.spawn_batched(components2);
        world.spawn_batched(components3);

        let mut query = world.query::<&Name>().iter();
        assert_eq!(query.next().unwrap().0, "John".to_owned());
        assert_eq!(query.next().unwrap().0, "Jane".to_owned());
        assert_eq!(query.next().unwrap().0, "Doe".to_owned());
        assert_eq!(query.next().unwrap().0, "Mike".to_owned());
        assert_eq!(query.next().unwrap().0, "Mary".to_owned());
        assert_eq!(query.next().unwrap().0, "Max".to_owned());
        assert!(query.next().is_none());

        let mut query = world.query::<(&Name, &Age)>().iter();
        assert_eq!(query.next().unwrap().0.0, "Mike".to_owned());
        assert_eq!(query.next().unwrap().0.0, "Mary".to_owned());
        assert_eq!(query.next().unwrap().0.0, "Max".to_owned());
        assert!(query.next().is_none());

        let mut query = world.query::<&Colour>().iter();
        assert_eq!(*query.next().unwrap(), Colour::Red);
        assert_eq!(*query.next().unwrap(), Colour::Green);
        assert_eq!(*query.next().unwrap(), Colour::Blue);
        assert!(query.next().is_none());

        let mut query = world.query::<(&Age, &Colour)>().iter();
        assert!(query.next().is_none());

        Ok(())
    }

    #[test]
    fn query_mut() {
        let mut world = World::default();

        let components = vec![
            (Name("Mike".to_owned()), Age(40)),
            (Name("Mary".to_owned()), Age(24)),
            (Name("Max".to_owned()), Age(86)),
        ];

        world.spawn_batched(components);

        for (name, age) in world.query_mut::<(&mut Name, &mut Age)>().iter_mut() {
            name.0 = format!("{}_{}", name.0, age.0);
            age.0 /= 2;
        }

        let mut query = world.query::<(&Name, &Age)>().iter_mut();

        let (name, age) = query.next().unwrap();
        assert_eq!(name.0, "Mike_40".to_owned());
        assert_eq!(age.0, 20);

        let (name, age) = query.next().unwrap();
        assert_eq!(name.0, "Mary_24".to_owned());
        assert_eq!(age.0, 12);

        let (name, age) = query.next().unwrap();
        assert_eq!(name.0, "Max_86".to_owned());
        assert_eq!(age.0, 43);
    }

    #[test]
    fn query_entity() {
        let mut world = World::default();

        let components1 = vec![Name("Mike".to_owned()), Name("Mary".to_owned())];
        let components2 = vec![Age(1), Age(2)];
        let components3 = vec![Name("Max".to_owned())];

        dbg!(world.spawn_batched(components1));
        dbg!(world.spawn_batched(components2));
        dbg!(world.spawn_batched(components3));

        let mut name_iter = world.query::<(Entity, &Name)>().iter();
        assert_eq!(name_iter.next().unwrap().0, Entity(0));
        assert_eq!(name_iter.next().unwrap().0, Entity(1));
        assert_eq!(name_iter.next().unwrap().0, Entity(4));

        let mut age_iter = world.query::<(Entity, &Age)>().iter();
        assert_eq!(age_iter.next().unwrap().0, Entity(2));
        assert_eq!(age_iter.next().unwrap().0, Entity(3));

        let mut entity_iter = world.query::<Entity>().iter();
        assert_eq!(entity_iter.next().unwrap(), Entity(0));
        assert_eq!(entity_iter.next().unwrap(), Entity(1));
        assert_eq!(entity_iter.next().unwrap(), Entity(4));
        assert_eq!(entity_iter.next().unwrap(), Entity(2));
        assert_eq!(entity_iter.next().unwrap(), Entity(3));
    }

    #[test]
    fn add_remove_resource() {
        let mut world = World::default();

        world.add_resource(Counter(0));
        assert!(world.get_resource::<Counter>().is_some());

        world.remove_resource::<Counter>();
        assert!(world.get_resource::<Counter>().is_none());
    }

    #[test]
    #[should_panic(
        expected = "Resource 'Counter' already exists.\nIf you want to replace it, call replace_resource instead"
    )]
    fn add_resource_with_existing() {
        let mut world = World::default();
        world.add_resource(Counter(0));
        world.add_resource(Counter(0));
    }

    #[test]
    fn replace_resource() {
        let mut world = World::default();

        world.add_resource(Counter(0));
        assert_eq!(world.get_resource::<Counter>().unwrap().inner.0, 0);

        world.replace_resource(Counter(6));
        assert_eq!(world.get_resource::<Counter>().unwrap().inner.0, 6);
    }

    #[test]
    fn get_resource() {
        let mut world = World::default();

        assert!(world.get_resource::<Counter>().is_none());
        world.add_resource(Counter(0));
        assert!(world.get_resource::<Counter>().is_some());
    }

    #[test]
    fn get_resource_mut() {
        let mut world = World::default();

        world.add_resource(Counter(0));
        world.get_resource_mut::<Counter>().unwrap().inner.0 = 6;
        assert_eq!(world.get_resource::<Counter>().unwrap().inner.0, 6);
    }
}
