//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::{cell::RefCell, marker::PhantomData, rc::Rc};

use applab_codegen::all_tuples;

use crate::{
    resource::{Resource, ResourceContainer},
    system::command::CommandBuffer,
    World,
};

pub mod command;
mod fetch;
pub mod label;
mod schedule;
mod stage;

pub use self::{
    schedule::{CoreStage, Scheduler},
    stage::SystemStage,
};

//--------------------------------------------------------------------------------------------------
//-- TRAITS ----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub trait SysArg {
    type Fetch: for<'w, 's> SysArgFetch<'w, 's>;
}

//--------------------------------------------------------------------------------------------------

pub trait SysArgFetch<'w, 's>: Sized {
    type Item: SysArg;

    fn init(_system: &'s mut impl SysRunner) {}
    fn fetch(world: &'w World, system: &'s impl SysRunner) -> Self::Item;
}

//--------------------------------------------------------------------------------------------------

/// Implemented for any function which can be used as a system.
pub trait SysFn<In, Arg, Marker>: 'static {}

//--------------------------------------------------------------------------------------------------

pub trait IntoSystem<In, Arg, Marker> {
    type System;
    type Builder;

    fn system(&'static self) -> Self::System;
    fn builder(&'static self) -> Self::Builder;
}

impl<In, Arg, Marker, F> IntoSystem<In, Arg, Marker> for F
where
    F: SysFn<In, Arg, Marker>,
{
    type System = System<In, Arg, Marker, F>;
    type Builder = SystemBuilder<F, In, Arg, Marker>;

    fn system(&'static self) -> Self::System {
        System {
            call_function:  self,
            command_buffer: Rc::default(),
            resources:      ResourceContainer::default(),
            marker:         PhantomData::default(),
        }
    }

    fn builder(&'static self) -> Self::Builder {
        Self::Builder::new()
    }
}

//--------------------------------------------------------------------------------------------------

pub trait SysRunner {
    fn init(&mut self);
    fn run(&self, world: &World);
    fn apply_buffers(&self, world: &mut World);

    fn commands(&self) -> Rc<RefCell<CommandBuffer>>;
    fn resources(&self) -> &ResourceContainer;
    fn resources_mut(&mut self) -> &mut ResourceContainer;
}

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub struct System<In, Arg, Marker, F>
where
    F: SysFn<In, Arg, Marker>,
{
    call_function:  &'static F,
    command_buffer: Rc<RefCell<CommandBuffer>>,
    resources:      ResourceContainer,
    marker:         PhantomData<(In, Arg, Marker)>,
}

//--------------------------------------------------------------------------------------------------

pub struct SystemBuilder<F, In, Arg, Marker>
where
    F: SysFn<In, Arg, Marker>,
{
    resources: ResourceContainer,
    marker:    PhantomData<(F, In, Arg, Marker)>,
}

impl<F, In, Arg, Marker> SystemBuilder<F, In, Arg, Marker>
where
    F: SysFn<In, Arg, Marker>,
{
    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        Self {
            resources: ResourceContainer::default(),
            marker:    PhantomData::default(),
        }
    }

    pub fn with_resource(mut self, resource: impl Resource) -> Self {
        self.resources.add(resource);
        self
    }

    pub fn build(self, system: &'static F) -> System<In, Arg, Marker, F> {
        System {
            call_function:  system,
            command_buffer: Rc::default(),
            resources:      self.resources,
            marker:         PhantomData::default(),
        }
    }
}

//--------------------------------------------------------------------------------------------------
//-- MACROS ----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

macro_rules! impl_sys_fn {

    ($($param: ident),*) => {
        impl<F: 'static, $($param: SysArg),*> SysFn<(), ($($param,)*), ()> for F
        where
            for<'a> &'a mut F:
                FnMut($($param),*) +
                FnMut($(<<$param as SysArg>::Fetch as SysArgFetch>::Item),*)
        {}

        impl<F, $($param: SysArg),*> SysRunner for System<(), ($($param,)*), (), F>
        where
            F: SysFn<(), ($($param,)*), ()> +
               Fn($(<<$param as SysArg>::Fetch as SysArgFetch>::Item),*)
        {
            #[inline]
            fn init(&mut self) {
                $(
                    <<$param as SysArg>::Fetch as SysArgFetch>::init(self);
                )*
            }

            #[inline]
            fn run(&self, world: &World) {
                (self.call_function)($(<<$param as SysArg>::Fetch as SysArgFetch>::fetch(&world, self)),*);
            }

            #[inline]
            fn apply_buffers(&self, world: &mut World) {
                self.command_buffer.borrow_mut().apply(world);
            }

            #[inline]
            fn commands(&self) -> Rc<RefCell<CommandBuffer>> {
                self.command_buffer.clone()
            }

            #[inline]
            fn resources(&self) -> &ResourceContainer {
                &self.resources
            }

            #[inline]
            fn resources_mut(&mut self) -> &mut ResourceContainer {
                &mut self.resources
            }
        }
    }
}

all_tuples!(impl_sys_fn, 1, 16, A);

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use applab_ecs_macros::Component;

    use crate as applab_ecs;
    use crate::{
        macros::Resource,
        query::Query,
        resource::{Res, ResMut},
        system::{IntoSystem, SysRunner},
        World,
    };

    #[derive(Component)]
    struct Human;

    #[derive(Component)]
    struct Cat;

    #[derive(Component)]
    struct Name(String);

    #[derive(Debug, PartialEq, Component)]
    enum Mood {
        Happy,
        Hungry,
        Sad,
        Angry,
        Relaxed,
        Constipated,
        Playful,
    }

    #[derive(Debug, Resource)]
    struct Counter(u32);

    fn print_mood(counter: ResMut<Counter>, query: Query<(&Name, &Mood)>) {
        for (name, mood) in query.iter() {
            println!("{} is {:?}", name.0, mood);
        }

        counter.inner.0 += 2;
    }

    fn set_mood_human(counter: ResMut<Counter>, mut query: Query<(&Human, &mut Mood)>) {
        for (_, mood) in query.iter_mut() {
            *mood = Mood::Happy;
        }

        counter.inner.0 *= 3;
    }

    fn set_mood_cat(counter: ResMut<Counter>, mut query: Query<(&Cat, &mut Mood)>) {
        for (_, mood) in query.iter_mut() {
            *mood = Mood::Hungry;
        }

        counter.inner.0 /= 2;
    }

    fn validate_mood(counter: Res<Counter>, query_human: Query<(&Human, &Mood)>, query_cat: Query<(&Cat, &Mood)>) {
        for (_, mood) in query_human.iter() {
            assert_eq!(*mood, Mood::Happy);
        }

        for (_, mood) in query_cat.iter() {
            assert_eq!(*mood, Mood::Hungry);
        }

        assert_eq!(counter.inner.0, 5);
    }

    #[test]
    fn run_system() {
        let mut world = World::default();

        let components1 = vec![
            (Human, Name("John".to_owned()), Mood::Angry),
            (Human, Name("Jane".to_owned()), Mood::Sad),
            (Human, Name("Jack".to_owned()), Mood::Hungry),
        ];

        let components2 = vec![
            (Cat, Name("Neutron".to_owned()), Mood::Relaxed),
            (Cat, Name("Tom".to_owned()), Mood::Playful),
            (Cat, Name("Seppy".to_owned()), Mood::Constipated),
        ];

        world.spawn_batched(components1);
        world.spawn_batched(components2);
        world.add_resource(Counter(0));

        let system_list: Vec<Box<dyn SysRunner>> = vec![
            Box::new(print_mood.system()),
            Box::new(set_mood_human.system()),
            Box::new(set_mood_cat.system()),
            Box::new(print_mood.system()),
            Box::new(validate_mood.system()),
        ];

        for s in system_list {
            s.run(&world);
        }
    }
}
