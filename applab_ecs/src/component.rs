//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::{
    alloc::Layout,
    any::{type_name, TypeId},
    fmt::Debug,
};

use applab_util::{drop_generic, HashMap};

use crate::query::{Access, FetchRead, FetchWrite, Lockable, WorldQuery};

//--------------------------------------------------------------------------------------------------
//-- TRAITS ----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub trait Component: 'static + Sized {
    fn info(&self) -> ComponentInfo {
        ComponentInfo::new::<Self>()
    }
}

impl Component for () {}

impl<'a, T: Component> Lockable for &'a T {
    fn acquire_lock(access: &mut Access) {
        access.add_read::<T>();
    }

    fn release_lock(access: &mut Access) {
        access.remove_read::<T>();
    }
}

impl<'a, T: Component> Lockable for &'a mut T {
    fn acquire_lock(access: &mut Access) {
        access.add_write::<T>();
    }

    fn release_lock(access: &mut Access) {
        access.remove_write::<T>();
    }
}

impl<'a, T: Component> WorldQuery for &'a T {
    type Fetch = FetchRead<T>;
}

impl<'a, T: Component> WorldQuery for &'a mut T {
    type Fetch = FetchWrite<T>;
}

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// The index of a [`Component`] within an [`Archetype`](crate::archetype::Archetype).
#[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
pub struct ComponentIndex(pub usize);

//--------------------------------------------------------------------------------------------------

#[derive(Debug)]
pub struct ComponentInfo {
    name:    String,
    type_id: TypeId,
    layout:  Layout,
    drop_fn: unsafe fn(ptr: *mut u8),
}

impl ComponentInfo {
    pub fn new<T: Component>() -> Self {
        Self {
            name:    type_name::<T>().to_owned(),
            type_id: TypeId::of::<T>(),
            layout:  Layout::new::<T>(),
            drop_fn: drop_generic::<T>,
        }
    }

    #[inline]
    pub fn name(&self) -> &str {
        &self.name
    }

    #[inline]
    pub const fn type_id(&self) -> TypeId {
        self.type_id
    }

    #[inline]
    pub const fn layout(&self) -> Layout {
        self.layout
    }

    #[inline]
    pub fn drop_fn(&self) -> unsafe fn(*mut u8) {
        self.drop_fn
    }
}

//--------------------------------------------------------------------------------------------------

#[derive(Debug, Default)]
pub struct ComponentInfoContainer(pub HashMap<TypeId, ComponentInfo>);

impl ComponentInfoContainer {
    /// Insert a new component info into the container.
    #[inline]
    pub fn insert<T: Component>(&mut self) {
        let inner = &mut self.0;
        let id = TypeId::of::<T>();

        inner.entry(id).or_insert_with(|| ComponentInfo::new::<T>());
    }

    /// Returns an instance of [`ComponentInfo`], creating one if it doesn't exist.
    #[inline]
    pub fn get_or_insert<T: Component>(&mut self) -> &ComponentInfo {
        self.0.entry(TypeId::of::<T>()).or_insert_with(ComponentInfo::new::<T>)
    }
}
