//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use crate::{system::SysRunner, World};

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// Stores and executes systems.
///
/// Systems may be executed in parallel, and are not guaranteed to be run in a particular order.
#[derive(Default)]
pub struct SystemStage {
    /// Systems contained in the stage.
    systems: Vec<Box<dyn SysRunner>>,

    /// Determines whether the systems have been modified since the last update.
    // TODO: Determine whether this is actually required.
    systems_modified: bool,
}

impl SystemStage {
    /// Returns the number of systems in the stage.
    #[inline]
    pub fn len(&self) -> usize {
        self.systems.len()
    }

    /// Returns whether this stage has any systems.
    #[inline]
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// Adds a new [`System`](crate::system::System) to the stage.
    ///
    /// # Arguments
    ///
    /// * `system`: The system to add to this stage.
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_ecs::query::Query;
    /// # use applab_ecs::system::{SystemStage, IntoSystem};
    /// fn my_system(query: Query<&()>) { /* ... */
    /// }
    ///
    /// let mut stage = SystemStage::default();
    /// stage.add_system(my_system.system());
    /// ```
    pub fn add_system(&mut self, mut system: impl SysRunner + 'static) {
        system.init();
        self.systems.push(Box::new(system));
        self.systems_modified = true;
    }

    /// Executes all systems stored in this stage.
    ///
    /// Systems may be run in parallel.
    ///
    /// # Arguments
    ///
    /// * `world`: Mutable reference to the [`World`]
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_ecs::World;
    /// # use applab_ecs::query::Query;
    /// # use applab_ecs::system::{SystemStage, IntoSystem};
    /// let mut world = World::default();
    /// let mut stage = SystemStage::default();
    ///
    /// stage.run(&mut world);
    /// ```
    pub fn run(&mut self, world: &mut World) {
        for system in &mut self.systems {
            system.run(world);
            system.apply_buffers(world);
        }

        self.systems_modified = false;
    }
}
