//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

// TODO: Can some of the code here be simplified with GATs?
//       I don't think it would be possible to improve WorldQuery or Fetch, but I wonder
//       if SysArgFetch could be merged with SysArg.

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use applab_util::short_type_name;

use crate::{
    event::{Event, EventReader, EventWriter, Events, ManualEventReader},
    query::{Query, WorldQuery},
    resource::{LocalRes, LocalResMut, Res, ResMut, Resource},
    system::{
        command::{CommandBuffer, Commands},
        SysArg, SysArgFetch, SysRunner,
    },
    World,
};

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// Allows for fetching a query for a system.
pub struct QueryFetch<T: WorldQuery>(T);

impl<'w, 's, T: WorldQuery> SysArgFetch<'w, 's> for QueryFetch<T> {
    type Item = Query<'w, T>;

    fn fetch(world: &'w World, _system: &'s impl SysRunner) -> Self::Item {
        world.query_mut()
    }
}

impl<'a, T: WorldQuery> SysArg for Query<'a, T> {
    type Fetch = QueryFetch<T>;
}

//--------------------------------------------------------------------------------------------------

pub struct ResFetch<T: Resource>(T);

impl<'w, 's, T: Resource> SysArgFetch<'w, 's> for ResFetch<T> {
    type Item = Res<'w, T>;

    fn fetch(world: &'w World, _system: &'s impl SysRunner) -> Self::Item {
        world.get_resource().map_or_else(
            || {
                let type_name = short_type_name::<T>();
                panic!("Could not fetch Res<{type_name}>. Resource does not exist");
            },
            |res| res,
        )
    }
}

impl<'a, T: Resource> SysArg for Res<'a, T> {
    type Fetch = ResFetch<T>;
}

//--------------------------------------------------------------------------------------------------

pub struct OptResFetch<T: Resource>(T);

impl<'w, 's, T: Resource> SysArgFetch<'w, 's> for OptResFetch<T> {
    type Item = Option<Res<'w, T>>;

    fn fetch(world: &'w World, _system: &'s impl SysRunner) -> Self::Item {
        world.get_resource()
    }
}

impl<'a, T: Resource> SysArg for Option<Res<'a, T>> {
    type Fetch = OptResFetch<T>;
}

//--------------------------------------------------------------------------------------------------

pub struct ResMutFetch<T: Resource>(T);

impl<'w, 's, T: Resource> SysArgFetch<'w, 's> for ResMutFetch<T> {
    type Item = ResMut<'w, T>;

    fn fetch(world: &'w World, _system: &'s impl SysRunner) -> Self::Item {
        world.get_resource_mut().map_or_else(
            || {
                let type_name = short_type_name::<T>();
                panic!("Could not fetch ResMut<{type_name}>. Resource does not exist");
            },
            |res| res,
        )
    }
}

impl<'a, T: Resource> SysArg for ResMut<'a, T> {
    type Fetch = ResMutFetch<T>;
}

//--------------------------------------------------------------------------------------------------

pub struct OptResMutFetch<T: Resource>(T);

impl<'w, 's, T: Resource> SysArgFetch<'w, 's> for OptResMutFetch<T> {
    type Item = Option<ResMut<'w, T>>;

    fn fetch(world: &'w World, _system: &'s impl SysRunner) -> Self::Item {
        world.get_resource_mut()
    }
}

impl<'a, T: Resource> SysArg for Option<ResMut<'a, T>> {
    type Fetch = OptResMutFetch<T>;
}

//--------------------------------------------------------------------------------------------------

pub struct LocalResFetch<T>(T)
where
    T: Resource + Default;

impl<'w, 's, T> SysArgFetch<'w, 's> for LocalResFetch<T>
where
    T: Resource + Default,
{
    type Item = LocalRes<'s, T>;

    fn init(system: &'s mut impl SysRunner) {
        system.resources_mut().add(T::default());
    }

    fn fetch(_world: &'w World, system: &'s impl SysRunner) -> Self::Item {
        system.resources().get().map_or_else(
            || {
                let type_name = short_type_name::<T>();
                panic!("[BUG] Could not fetch local resource \"{type_name}\". Resource does not exist");
            },
            LocalRes::from,
        )
    }
}

impl<'a, T> SysArg for LocalRes<'a, T>
where
    T: Resource + Default,
{
    type Fetch = LocalResFetch<T>;
}

//--------------------------------------------------------------------------------------------------

pub struct LocalResMutFetch<T>(T)
where
    T: Resource + Default;

impl<'w, 's, T> SysArgFetch<'w, 's> for LocalResMutFetch<T>
where
    T: Resource + Default,
{
    type Item = LocalResMut<'s, T>;

    fn init(system: &'s mut impl SysRunner) {
        system.resources_mut().add(T::default());
    }

    fn fetch(_world: &'w World, system: &'s impl SysRunner) -> Self::Item {
        system.resources().get_mut().map_or_else(
            || {
                let type_name = short_type_name::<T>();
                panic!("[BUG] Could not fetch local resource \"{type_name}\". Resource does not exist");
            },
            LocalResMut::new,
        )
    }
}

impl<'a, T> SysArg for LocalResMut<'a, T>
where
    T: Resource + Default,
{
    type Fetch = LocalResMutFetch<T>;
}

//--------------------------------------------------------------------------------------------------

pub struct EventReaderFetch<T: Event>(T);

impl<'w, 's, T: Event> SysArgFetch<'w, 's> for EventReaderFetch<T> {
    type Item = EventReader<'w, 's, T>;

    fn init(system: &'s mut impl SysRunner) {
        system.resources_mut().add(ManualEventReader::<T>::default());
    }

    fn fetch(world: &'w World, system: &'s impl SysRunner) -> Self::Item {
        let reader = system.resources().get_mut::<ManualEventReader<T>>().unwrap_or_else(|| {
            let type_name = short_type_name::<T>();
            panic!("[BUG] Could not fetch local event resource \"{type_name}\". Resource does not exist");
        });

        let events = world.get_resource::<Events<T>>().unwrap_or_else(|| {
            let type_name = short_type_name::<T>();
            panic!("Could not fetch event \"{type_name}\". Has it been registered?");
        });

        Self::Item::new(LocalResMut::new(reader), events)
    }
}

impl<'w, 's, T: Event> SysArg for EventReader<'w, 's, T> {
    type Fetch = EventReaderFetch<T>;
}

//--------------------------------------------------------------------------------------------------

pub struct EventWriterFetch<T: Event>(T);

impl<'w, 's, T: Event> SysArgFetch<'w, 's> for EventWriterFetch<T> {
    type Item = EventWriter<'w, T>;

    fn init(system: &'s mut impl SysRunner) {
        system.resources_mut().add(ManualEventReader::<T>::default());
    }

    fn fetch(world: &'w World, _system: &'s impl SysRunner) -> Self::Item {
        let events = world.get_resource_mut::<Events<T>>().unwrap_or_else(|| {
            let type_name = short_type_name::<T>();
            panic!("Could not fetch event \"{type_name}\". Has it been registered?");
        });

        Self::Item::new(events)
    }
}

impl<'w, T: Event> SysArg for EventWriter<'w, T> {
    type Fetch = EventWriterFetch<T>;
}

//--------------------------------------------------------------------------------------------------

impl<'w, 's> SysArgFetch<'w, 's> for CommandBuffer {
    type Item = Commands;

    fn fetch(_world: &'w World, system: &'s impl SysRunner) -> Self::Item {
        Commands {
            inner: system.commands(),
        }
    }
}

impl SysArg for Commands {
    type Fetch = CommandBuffer;
}

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[cfg(test)]
#[allow(clippy::unwrap_used)]
mod resource_tests {

    use crate as applab_ecs;
    use crate::{
        macros::Resource,
        resource::{LocalRes, LocalResMut, Res, ResMut},
        system::{IntoSystem, SysArg, SysArgFetch, SysRunner},
        World,
    };

    #[derive(Debug, Resource)]
    struct Counter(u32);
    impl Default for Counter {
        fn default() -> Self {
            Self(42)
        }
    }

    #[derive(Debug, Resource)]
    struct Health(i32);
    impl Default for Health {
        fn default() -> Self {
            Self(3)
        }
    }

    fn placeholder_system(_: Res<Counter>) {
        unimplemented!("This is intentional. It is not meant to be called");
    }

    #[test]
    #[should_panic(expected = "Could not fetch Res<Counter>. Resource does not exist")]
    fn res_fetch_missing_type() {
        let world = World::default();
        let placeholder = placeholder_system.system();

        <<Res<Counter> as SysArg>::Fetch as SysArgFetch>::fetch(&world, &placeholder);
    }

    #[test]
    #[should_panic(expected = "Could not fetch ResMut<Counter>. Resource does not exist")]
    fn res_mut_fetch_missing_type() {
        let world = World::default();
        let placeholder = placeholder_system.system();

        <<ResMut<Counter> as SysArg>::Fetch as SysArgFetch>::fetch(&world, &placeholder);
    }

    #[test]
    fn opt_res_fetch() {
        let mut world = World::default();
        let placeholder = placeholder_system.system();

        assert!(<<Option<Res<Counter>> as SysArg>::Fetch as SysArgFetch>::fetch(&world, &placeholder).is_none());
        world.add_resource(Counter(0));
        assert!(<<Option<Res<Counter>> as SysArg>::Fetch as SysArgFetch>::fetch(&world, &placeholder).is_some());
    }

    #[test]
    fn opt_mut_res_fetch() {
        let mut world = World::default();
        let placeholder = placeholder_system.system();

        assert!(<<Option<Res<Counter>> as SysArg>::Fetch as SysArgFetch>::fetch(&world, &placeholder).is_none());
        world.add_resource(Counter(0));

        {
            let res = <<Option<ResMut<Counter>> as SysArg>::Fetch as SysArgFetch>::fetch(&world, &placeholder);
            assert!(res.is_some());
            res.unwrap().0 = 6;
        }

        let res = <<Option<Res<Counter>> as SysArg>::Fetch as SysArgFetch>::fetch(&world, &placeholder).unwrap();
        assert_eq!(res.0, 6);
    }

    #[test]
    fn local_res_fetch() {
        fn system(counter: LocalRes<Counter>, mut health: LocalResMut<Health>) {
            assert_eq!(counter.0, 42);
            health.0 -= 1;
        }

        let mut sys_fn = system.system();
        let world = World::default();

        sys_fn.init();

        for i in (0..4).rev() {
            {
                let res = <<LocalRes<Health> as SysArg>::Fetch as SysArgFetch>::fetch(&world, &sys_fn);
                assert_eq!(res.0, i);
            }

            sys_fn.run(&world);
        }
    }
}

//--------------------------------------------------------------------------------------------------

#[cfg(test)]
#[allow(clippy::unwrap_used)]
mod event_tests {
    use crate as applab_ecs;
    use crate::{
        event::{EventReader, EventWriter, Events},
        macros::{Event, Resource},
        resource::LocalResMut,
        system::{CoreStage, IntoSystem, Scheduler, SysRunner},
        World,
    };

    #[derive(Debug, Event)]
    struct MyEvent;

    #[derive(Default, Debug, Resource)]
    struct Counter(u8);

    #[derive(Default, Debug, Resource)]
    struct Iteration(u8);

    fn read_system(
        mut reader: EventReader<MyEvent>,
        mut counter: LocalResMut<Counter>,
        mut iteration: LocalResMut<Iteration>,
    ) {
        for _ in reader.iter() {
            counter.0 += 1;
        }

        if iteration.0 == 0 {
            assert_eq!(counter.0, 3);
        } else {
            assert_eq!(counter.0, 8);
        }

        iteration.0 += 1;
    }

    fn write_system(mut writer: EventWriter<MyEvent>, mut iteration: LocalResMut<Iteration>) {
        let count = if iteration.0 == 1 { 2 } else { 3 };
        for _ in 0..count {
            writer.send(MyEvent {});
        }

        iteration.0 += 1;
    }

    #[test]
    fn event_fetch() {
        let mut world = World::default();
        let mut scheduler = Scheduler::default();

        world.init_resource::<Events<MyEvent>>();
        scheduler.add_system_to_stage(CoreStage::First, Events::<MyEvent>::update_system.system());
        scheduler.add_system_to_stage(CoreStage::Update, write_system.system());
        scheduler.add_system_to_stage(CoreStage::Update, read_system.system());
        scheduler.add_system_to_stage(CoreStage::Update, write_system.system());

        // The event should start out empty
        {
            let event = world.get_resource::<Events<MyEvent>>().unwrap();
            assert_eq!(event.event_count, 0);
            assert_eq!(event.buffer_a.len(), 0);
            assert_eq!(event.buffer_b.len(), 0);
        }

        // Execute the world once.
        // This should add 6 events to the stack, and all events should be in buffer_a.
        scheduler.execute(&mut world);
        {
            let event = world.get_resource::<Events<MyEvent>>().unwrap();
            assert_eq!(event.event_count, 6);
            assert_eq!(event.buffer_a.len(), 6);
            assert_eq!(event.buffer_b.len(), 0);
        }

        // Execute the world again.
        // There should now be 10 events, with 4 in buffer_a, and 6 in buffer_b.
        scheduler.execute(&mut world);
        {
            let event = world.get_resource::<Events<MyEvent>>().unwrap();
            assert_eq!(event.event_count, 10);
            assert_eq!(event.buffer_a.len(), 4);
            assert_eq!(event.buffer_b.len(), 6);
        }

        let mut event = world.get_resource_mut::<Events<MyEvent>>().unwrap();

        // Swap the event buffers, setting buffer_a to 0 and buffer_b to 4.
        // The event count should remain at 10, as this value never decreases.
        event.update();
        assert_eq!(event.event_count, 10);
        assert_eq!(event.buffer_a.len(), 0);
        assert_eq!(event.buffer_b.len(), 4);

        // Swap the event buffers, setting buffer_a and buffer_b to 0.
        // The event count should remain at 10, as this value never decreases.
        event.update();
        assert_eq!(event.event_count, 10);
        assert_eq!(event.buffer_a.len(), 0);
        assert_eq!(event.buffer_b.len(), 0);
    }

    #[test]
    #[should_panic(expected = "Could not fetch event \"MyEvent\". Has it been registered?")]
    fn event_read_missing_res() {
        let mut world = World::default();
        let mut scheduler = Scheduler::default();

        scheduler.add_system_to_stage(CoreStage::Update, read_system.system());
        scheduler.execute(&mut world);
    }

    #[test]
    #[should_panic(expected = "Could not fetch event \"MyEvent\". Has it been registered?")]
    fn event_write_missing_res() {
        let mut world = World::default();
        let mut scheduler = Scheduler::default();

        scheduler.add_system_to_stage(CoreStage::Update, write_system.system());
        scheduler.execute(&mut world);
    }

    #[test]
    #[should_panic(expected = "[BUG] Could not fetch local event resource \"MyEvent\". Resource does not exist")]
    fn event_read_missing_local_res() {
        let mut world = World::default();
        world.init_resource::<Events<MyEvent>>();

        // Skip system init and go straight to running
        read_system.system().run(&world);
    }
}
