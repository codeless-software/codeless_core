//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::{
    cell::{Cell, RefCell},
    marker::PhantomData,
    mem, ptr,
    rc::Rc,
};

use applab_util::{short_type_name, CastU8};

use crate::{
    archetype::{ArchetypeAppender, ArchetypeBuilder},
    component::Component,
    entity::Entity,
    event::{Event, Events},
    resource::Resource,
    World,
};

//--------------------------------------------------------------------------------------------------
//-- TYPES -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

type EntityRc = Rc<Cell<Entity>>;

//--------------------------------------------------------------------------------------------------
//-- TRAITS ----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub trait Command {
    fn write(self, world: &mut World);
}

//--------------------------------------------------------------------------------------------------
//-- PUBLIC STRUCTS --------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// Stores metadata on a command.
pub struct CommandMetadata {
    offset:   usize,
    write_fn: unsafe fn(value: *mut u8, world: &mut World),
}

//--------------------------------------------------------------------------------------------------

#[derive(Default)]
pub struct CommandBuffer {
    bytes: Vec<u8>,
    meta:  Vec<CommandMetadata>,
}

//--------------------------------------------------------------------------------------------------

impl CommandBuffer {
    pub fn push<C>(&mut self, command: C)
    where
        C: Command,
    {
        unsafe fn write_fn<T>(command: *mut u8, world: &mut World)
        where
            T: Command,
        {
            let command = command.cast::<T>().read_unaligned();
            command.write(world);
        }

        let size = mem::size_of::<C>();
        let old_len = self.bytes.len();

        self.meta.push(CommandMetadata {
            offset:   old_len,
            write_fn: write_fn::<C>,
        });

        if size > 0 {
            self.bytes.reserve(size);

            // SAFETY: The `bytes` has enough storage for adding the command, as it has
            // reserved above, and the length is set correctly.
            // We also call forget on the command below to prevent a double free when calling write.
            unsafe {
                ptr::copy_nonoverlapping(command.cast_u8(), self.bytes.as_mut_ptr().add(old_len), size);
                self.bytes.set_len(old_len + size);
            }
        }

        mem::forget(command);
    }

    pub fn apply(&mut self, world: &mut World) {
        unsafe { self.bytes.set_len(0) }

        let byte_ptr = if self.bytes.as_mut_ptr().is_null() {
            // SAFETY: If the vector's ptr is null, this means that nothing has been pushed to it.
            // This means that there are either no commands, or only zero sized commands were pushed.
            unsafe { ptr::NonNull::dangling().as_mut() }
        } else {
            self.bytes.as_mut_ptr()
        };

        for meta in self.meta.drain(..) {
            unsafe {
                (meta.write_fn)(byte_ptr.add(meta.offset), world);
            }
        }
    }
}

//--------------------------------------------------------------------------------------------------

pub struct Commands {
    pub(crate) inner: Rc<RefCell<CommandBuffer>>,
}

impl Commands {
    pub fn add_batch<C: 'static, I: 'static>(&mut self, batch: I)
    where
        C: ArchetypeAppender + ArchetypeBuilder,
        I: IntoIterator<Item = C>,
    {
        self.inner.borrow_mut().push(AddBatch { batch_iter: batch });
    }

    pub fn spawn(&mut self) -> EntityCommands {
        let id = Rc::new(Cell::new(Entity(!0)));
        self.inner.borrow_mut().push(Spawn { id: id.clone() });

        EntityCommands::new(id, self.inner.clone())
    }

    pub fn despawn(&mut self, entity: Entity) {
        self.inner.borrow_mut().push(Despawn { id: entity });
    }

    pub fn get_entity(&mut self, entity: Entity) -> EntityCommands {
        let id = Rc::new(Cell::new(entity));
        EntityCommands::new(id, self.inner.clone())
    }

    pub fn add_resource<T: Resource>(&mut self, resource: T) {
        self.inner.borrow_mut().push(ResourceCommand::Add(resource));
    }

    pub fn replace_resource<T: Resource>(&mut self, resource: T) {
        self.inner.borrow_mut().push(ResourceCommand::Replace(resource));
    }

    pub fn remove_resource<T: Resource>(&mut self) {
        self.inner.borrow_mut().push(ResourceCommand::Remove::<T>);
    }

    pub fn send_event(&mut self, event: impl Event) {
        self.inner.borrow_mut().push(SendEvent { event });
    }
}

//--------------------------------------------------------------------------------------------------

pub struct EntityCommands {
    id:     EntityRc,
    buffer: Rc<RefCell<CommandBuffer>>,
}

impl EntityCommands {
    pub(crate) fn new(id: EntityRc, buffer: Rc<RefCell<CommandBuffer>>) -> Self {
        Self { id, buffer }
    }

    pub fn add_component(&mut self, component: impl Component) -> &mut Self {
        self.buffer.borrow_mut().push(AddComponent {
            id: self.id.clone(),
            component,
        });

        self
    }

    pub fn remove_component<T: Component>(&mut self) -> &mut Self {
        self.buffer.borrow_mut().push(RemoveComponent {
            id:      self.id.clone(),
            phantom: PhantomData::<T>::default(),
        });

        self
    }
}

//--------------------------------------------------------------------------------------------------
//-- PRIVATE STRUCTS -------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

struct AddBatch<C, I>
where
    C: ArchetypeAppender + ArchetypeBuilder,
    I: IntoIterator<Item = C>,
{
    pub batch_iter: I,
}

impl<C, I> Command for AddBatch<C, I>
where
    C: ArchetypeAppender + ArchetypeBuilder,
    I: IntoIterator<Item = C>,
{
    fn write(self, world: &mut World) {
        world.spawn_batched(self.batch_iter);
    }
}

//--------------------------------------------------------------------------------------------------

enum ResourceCommand<T: Resource> {
    Add(T),
    Replace(T),
    Remove,
}

impl<T: Resource> Command for ResourceCommand<T> {
    fn write(self, world: &mut World) {
        match self {
            Self::Add(res) => world.add_resource(res),
            Self::Replace(res) => world.replace_resource(res),
            Self::Remove => world.remove_resource::<T>(),
        }
    }
}

//--------------------------------------------------------------------------------------------------

struct SendEvent<T: Event> {
    event: T,
}

impl<T: Event> Command for SendEvent<T> {
    fn write(self, world: &mut World) {
        world
            .get_resource_mut::<Events<T>>()
            .unwrap_or_else(|| {
                let type_name = short_type_name::<T>();
                panic!("Could not send event {type_name}. Has is been registered?");
            })
            .send(self.event);
    }
}

//--------------------------------------------------------------------------------------------------

struct Spawn {
    id: EntityRc,
}

impl Command for Spawn {
    fn write(self, world: &mut World) {
        let entity = world.spawn();
        self.id.set(entity.id());
    }
}

//--------------------------------------------------------------------------------------------------

struct Despawn {
    id: Entity,
}

impl Command for Despawn {
    fn write(self, world: &mut World) {
        world.get_entity_mut(self.id).unwrap().despawn();
    }
}

//--------------------------------------------------------------------------------------------------

struct AddComponent<T> {
    id:        EntityRc,
    component: T,
}

impl<T: Component> Command for AddComponent<T> {
    fn write(self, world: &mut World) {
        let entity = self.id.get();
        world.get_entity_mut(entity).unwrap().add_component(self.component);
    }
}

//--------------------------------------------------------------------------------------------------

struct RemoveComponent<T> {
    id:      EntityRc,
    phantom: PhantomData<T>,
}

impl<T: Component> Command for RemoveComponent<T> {
    fn write(self, world: &mut World) {
        let entity = self.id.get();
        world.get_entity_mut(entity).unwrap().remove_component::<T>();
    }
}

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[cfg(test)]
#[allow(clippy::unwrap_used)]
mod tests {
    use applab_ecs_macros::Component;

    use crate as applab_ecs;
    use crate::{
        entity::Entity,
        event::{EventReader, Events},
        macros::{Event, Resource},
        query::Query,
        system::{command::Commands, CoreStage, IntoSystem, Scheduler},
        World,
    };

    #[derive(Component)]
    struct TestComponent;

    #[derive(Component)]
    struct TestComponentSized(String);

    #[derive(Debug, Resource)]
    struct TestResource(String);

    #[derive(Debug, Resource)]
    struct TestResourceZeroSized;

    #[test]
    fn zero_sized_and_empty() {
        fn system_no_command(mut _commands: Commands) {
            // Intentionally left empty
        }

        fn system_zero_sized(mut commands: Commands) {
            commands.add_resource(TestResourceZeroSized);
        }

        let mut world = World::default();
        let mut scheduler = Scheduler::default();

        assert!(!world.has_resource::<TestResourceZeroSized>());

        scheduler.add_system_to_stage(CoreStage::Update, system_no_command.system());
        scheduler.add_system_to_stage(CoreStage::Update, system_zero_sized.system());
        scheduler.execute(&mut world);

        assert!(world.has_resource::<TestResourceZeroSized>());
    }

    #[test]
    fn add_batch() {
        fn system(mut commands: Commands) {
            commands.add_batch(vec![TestComponent, TestComponent]);
        }

        let mut world = World::default();
        let mut scheduler = Scheduler::default();

        assert!(world.get_entity(Entity(0)).is_none());
        assert!(world.get_entity(Entity(1)).is_none());

        scheduler.add_system_to_stage(CoreStage::Update, system.system());
        scheduler.execute(&mut world);

        assert!(world.get_entity(Entity(0)).is_some());
        assert!(world.get_entity(Entity(1)).is_some());
    }

    #[test]
    fn spawn_add_remove_despawn() {
        fn system_spawn(mut commands: Commands) {
            commands
                .spawn()
                .add_component(TestComponent)
                .add_component(TestComponentSized("John".to_owned()));
        }

        fn system_remove_component(mut commands: Commands, query: Query<Entity>) {
            for entity in query.iter() {
                commands.get_entity(entity).remove_component::<TestComponent>();
            }
        }

        fn system_despawn(mut commands: Commands, query: Query<Entity>) {
            for entity in query.iter() {
                commands.despawn(entity);
            }
        }

        let mut world = World::default();
        let mut scheduler = Scheduler::default();

        scheduler.add_system_to_stage(CoreStage::PreUpdate, system_spawn.system());
        scheduler.add_system_to_stage(CoreStage::Update, system_remove_component.system());
        scheduler.add_system_to_stage(CoreStage::PostUpdate, system_despawn.system());

        scheduler.execute_stage(CoreStage::PreUpdate, &mut world).unwrap();
        let entity = world.get_entity(Entity(0)).unwrap();
        assert_eq!(world.archetypes().len(), 3);
        assert!(entity.get_component::<TestComponent>().is_ok());
        assert!(entity.get_component::<TestComponentSized>().is_ok());

        scheduler.execute_stage(CoreStage::Update, &mut world).unwrap();
        let entity = world.get_entity(Entity(0)).unwrap();
        assert_eq!(world.archetypes().len(), 4);
        assert!(entity.get_component::<TestComponent>().is_err());
        assert!(entity.get_component::<TestComponentSized>().is_ok());

        scheduler.execute_stage(CoreStage::PostUpdate, &mut world).unwrap();
        assert_eq!(world.archetypes().len(), 4);
        assert!(world.get_entity(Entity(0)).is_none());
    }

    #[test]
    fn add_resource() {
        fn system(mut commands: Commands) {
            commands.add_resource(TestResource("Foo".to_owned()));
        }

        let mut world = World::default();
        let mut scheduler = Scheduler::default();

        assert!(!world.has_resource::<TestResource>());

        scheduler.add_system_to_stage(CoreStage::Update, system.system());
        scheduler.execute(&mut world);

        assert!(world.has_resource::<TestResource>());
    }

    #[test]
    fn replace_resource() {
        fn system(mut commands: Commands) {
            commands.replace_resource(TestResource("Bar".to_owned()));
        }

        let mut world = World::default();
        let mut scheduler = Scheduler::default();

        world.add_resource(TestResource("Foo".to_owned()));
        assert_eq!(world.get_resource::<TestResource>().unwrap().inner.0, "Foo");

        scheduler.add_system_to_stage(CoreStage::Update, system.system());
        scheduler.execute(&mut world);

        assert_eq!(world.get_resource::<TestResource>().unwrap().inner.0, "Bar");
    }

    #[test]
    fn remove_resource() {
        fn system(mut commands: Commands) {
            commands.remove_resource::<TestResource>();
        }

        let mut world = World::default();
        let mut scheduler = Scheduler::default();

        world.add_resource(TestResource("Foo".to_owned()));
        assert!(world.has_resource::<TestResource>());

        scheduler.add_system_to_stage(CoreStage::Update, system.system());
        scheduler.execute(&mut world);

        assert!(!world.has_resource::<TestResource>());
    }

    #[test]
    fn send_event() {
        #[derive(Debug, Event)]
        struct MyEvent(u8);

        fn system_send(mut commands: Commands) {
            commands.send_event(MyEvent(10));
            commands.send_event(MyEvent(14));
            commands.send_event(MyEvent(5));
            commands.send_event(MyEvent(13));
        }

        fn system_recv(mut event: EventReader<MyEvent>) {
            let mut res = 0;

            for e in event.iter() {
                println!("{}", e.0);
                res += e.0;
            }

            assert_eq!(res, 42);
        }

        let mut world = World::default();
        let mut scheduler = Scheduler::default();

        world.add_resource(Events::<MyEvent>::default());
        scheduler.add_system_to_stage(CoreStage::Update, system_send.system());
        scheduler.add_system_to_stage(CoreStage::Update, system_recv.system());

        scheduler.execute(&mut world);
    }
}
