//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::{any::type_name, hash::Hash};

use applab_ecs_macros as ecm;
use applab_util::HashMap;
use thiserror::Error;

use crate as applab_ecs;
use crate::{
    system::{
        label::{BoxedStageLabel, StageLabel},
        stage::SystemStage,
        SysRunner,
    },
    World,
};

//--------------------------------------------------------------------------------------------------
//-- ENUMS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[derive(Error, Debug)]
pub enum ScheduleError {
    #[error("The stage {stage_name} could not be found.\nPlease ensure it has been registered with the Scheduler")]
    StageNotFound { stage_name: &'static str },
}

//--------------------------------------------------------------------------------------------------

#[derive(Copy, Clone, Debug, Hash, Eq, PartialEq, ecm::StageLabel)]
pub enum CoreStage {
    Startup,
    First,
    PreUpdate,
    Update,
    PostUpdate,
    Last,
}

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// Schedules and executes stages and their systems.
pub struct Scheduler {
    stages:      HashMap<BoxedStageLabel, SystemStage>,
    stage_order: Vec<BoxedStageLabel>,
}

impl Default for Scheduler {
    fn default() -> Self {
        let mut scheduler = Self {
            stages:      HashMap::default(),
            stage_order: vec![],
        };

        scheduler.add_stage(CoreStage::Startup);
        scheduler.add_stage(CoreStage::First);
        scheduler.add_stage(CoreStage::PreUpdate);
        scheduler.add_stage(CoreStage::Update);
        scheduler.add_stage(CoreStage::PostUpdate);
        scheduler.add_stage(CoreStage::Last);

        scheduler
    }
}

impl Scheduler {
    /// Returns whether the Scheduler contains `stage_label`.
    #[inline]
    pub fn has_stage(&self, stage_label: &dyn StageLabel) -> bool {
        self.stages.contains_key(stage_label)
    }

    /// Returns a reference to the [`SystemStage`] identified by `stage_label`.
    ///
    /// If the stage does not exist, `None` is returned instead.
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_ecs::system::Scheduler;
    /// # let mut scheduler = Scheduler::default();
    /// # scheduler.add_stage("my_stage");
    /// let stage = scheduler
    ///     .get_stage(&"my_stage")
    ///     .expect("Could not find stage");
    /// ```
    #[inline]
    pub fn get_stage(&self, stage_label: &dyn StageLabel) -> Option<&SystemStage> {
        self.stages.get(stage_label)
    }

    /// Returns a mutable reference to the [`SystemStage`] identified by `stage_label`.
    ///
    /// If the stage does not exist, `None` is returned instead.
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_ecs::system::Scheduler;
    /// # let mut scheduler = Scheduler::default();
    /// # scheduler.add_stage("my_stage");
    /// let mut stage = scheduler
    ///     .get_stage_mut(&"my_stage")
    ///     .expect("Could not find stage");
    /// ```
    #[inline]
    pub fn get_stage_mut(&mut self, stage_label: &dyn StageLabel) -> Option<&mut SystemStage> {
        self.stages.get_mut(stage_label)
    }

    /// Adds a new stage to the last position in the schedule.
    ///
    /// # Panics
    /// Panics if the stage already exits.
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_ecs::system::Scheduler;
    /// # let mut scheduler = Scheduler::default();
    /// scheduler.add_stage("my_stage");
    /// ```
    // NOTE: This will place the stage before CoreStage::Last. Is this really what we want?
    pub fn add_stage(&mut self, label: impl StageLabel) {
        let label: BoxedStageLabel = Box::new(label);

        self.stage_order.push(label.clone());
        assert!(
            self.stages.insert(label.clone(), SystemStage::default()).is_none(),
            "Stage already exists: {label:?}"
        );
    }

    /// Adds a new stage immediately before the `target` stage.
    ///
    /// # Panics
    /// Panics if the `target` stage doesn't exist, or the stage defined by `label` already exists.
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_ecs::system::Scheduler;
    /// # let mut scheduler = Scheduler::default();
    /// # scheduler.add_stage("target_stage");
    /// scheduler.add_stage_before("my_stage", "target_stage");
    /// ```
    pub fn add_stage_before(&mut self, label: impl StageLabel, target: impl StageLabel) {
        let label: BoxedStageLabel = Box::new(label);
        let target_index = self
            .get_stage_position(&target)
            .unwrap_or_else(|| panic!("Target stage does not exist: {:?}", target));

        assert_ne!(target_index, 0, "Cannot add a stage before CoreStage::Startup");

        self.stage_order.insert(target_index, label.clone());
        assert!(
            self.stages.insert(label.clone(), SystemStage::default()).is_none(),
            "Stage already exists: {label:?}"
        );
    }

    /// Adds a new stage immediately after the `target` stage.
    ///
    /// # Panics
    /// Panics if the `target` stage doesn't exist, or the stage defined by `label` already exists.
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_ecs::system::Scheduler;
    /// # let mut scheduler = Scheduler::default();
    /// # scheduler.add_stage("target_stage");
    /// scheduler.add_stage_after("my_stage", "target_stage");
    /// ```
    pub fn add_stage_after(&mut self, label: impl StageLabel, target: impl StageLabel) {
        let label: BoxedStageLabel = Box::new(label);
        let target_index = self
            .get_stage_position(&target)
            .unwrap_or_else(|| panic!("Target stage does not exist: {target:?}"));

        self.stage_order.insert(target_index + 1, label.clone());
        assert!(
            self.stages.insert(label.clone(), SystemStage::default()).is_none(),
            "Stage already exists: {label:?}",
        );
    }

    /// Adds a new system to [`CoreStage::Startup`].
    pub fn add_startup_system(&mut self, system: impl SysRunner + 'static) {
        let stage = self
            .get_stage_mut(&CoreStage::Startup)
            .expect("Startup stage does not exist! This is a bug");

        stage.add_system(system);
    }

    /// Adds a system to the `target` stage.
    ///
    /// # Panics
    /// Panics if the `target` stage doesn't exist.
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_ecs::query::Query;
    /// # use applab_ecs::system::{Scheduler, IntoSystem};
    /// # fn my_system(_: Query<&()>) {}
    /// # let mut scheduler = Scheduler::default();
    /// # scheduler.add_stage("target_stage");
    /// scheduler.add_system_to_stage("target_stage", my_system.system());
    /// ```
    pub fn add_system_to_stage(&mut self, target: impl StageLabel, system: impl SysRunner + 'static) {
        let stage = self
            .get_stage_mut(&target)
            .unwrap_or_else(|| panic!("Stage '{target:?}' does not exist"));

        stage.add_system(system);
    }

    /// Executes the startup stage.
    pub fn execute_startup(&mut self, world: &mut World) {
        let stage: BoxedStageLabel = Box::new(CoreStage::Startup);
        self.stages
            .get_mut(&*stage)
            .expect("Could not find stage CoreStage::Startup. This is a bug!")
            .run(world);
    }

    /// Executes stages and systems.
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_ecs::World;
    /// # use applab_ecs::system::Scheduler;
    /// # let mut scheduler = Scheduler::default();
    /// # let mut world = World::default();
    /// scheduler.execute(&mut world);
    /// ```
    pub fn execute(&mut self, world: &mut World) {
        for stage_label in self.stage_order.iter().skip(1) {
            self.stages
                .get_mut(stage_label)
                .expect("Could not find stage {stage_label:?}. This is a bug!")
                .run(world);
        }
    }

    /// Executes a specific stage.
    ///
    /// This can be useful for debugging and testing purposes.
    pub fn execute_stage<T: StageLabel>(&mut self, stage: T, world: &mut World) -> Result<(), ScheduleError> {
        let stage_label: BoxedStageLabel = Box::new(stage);
        if let Some(stage) = self.stages.get_mut(&stage_label) {
            stage.run(world);
            return Ok(());
        }

        let stage_name = type_name::<T>();
        Err(ScheduleError::StageNotFound { stage_name })
    }

    /// Returns the execution position of a stage with in the schedule.
    ///
    /// If the stage doesn't exist, `None` is returned instead.
    fn get_stage_position(&self, target: &dyn StageLabel) -> Option<usize> {
        self.stage_order
            .iter()
            .enumerate()
            .find(|(_, stage_label)| &***stage_label == target)
            .map(|(i, _)| i)
    }
}

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use applab_ecs_macros::Component;

    use crate as applab_ecs;
    use crate::{
        entity::Entity,
        query::Query,
        system::{
            schedule::{CoreStage, Scheduler},
            IntoSystem,
        },
        World,
    };

    #[derive(Component)]
    struct Number(i32);

    // Test system for adding 5 to a number
    fn add_five(mut query: Query<&mut Number>) {
        for number in query.iter_mut() {
            number.0 += 5;
        }
    }

    // Test system for multiplying a number by 3
    fn mul_three(mut query: Query<&mut Number>) {
        for number in query.iter_mut() {
            number.0 *= 3;
        }
    }

    // Test system for printing the numbers
    fn print_results(query: Query<&Number>) {
        let validate = |index: usize, val: i32| match index {
            0 => assert_eq!(val, 63),
            1 => assert_eq!(val, 24),
            2 => assert_eq!(val, 141),
            _ => panic!("Unexpected index"),
        };

        for (idx, number) in query.iter().enumerate() {
            println!("{}", number.0);
            validate(idx, number.0);
        }
    }

    // Test system what will panic if called
    fn panic(_query: Query<Entity>) {
        panic!("Oh no!");
    }

    #[test]
    fn add_stage() {
        let mut scheduler = Scheduler::default();
        scheduler.add_stage("MyStage1");
        scheduler.add_stage("MyStage2");

        assert!(scheduler.stage_order[6] == Box::new("MyStage1"));
        assert!(scheduler.stage_order[7] == Box::new("MyStage2"));
    }

    #[test]
    #[should_panic(expected = "Stage already exists: Update")]
    fn add_stage_duplicate_stage() {
        let mut scheduler = Scheduler::default();
        scheduler.add_stage(CoreStage::Update);
    }

    #[test]
    fn add_stage_before() {
        let mut scheduler = Scheduler::default();
        scheduler.add_stage_before("MyStage", CoreStage::Update);

        assert!(scheduler.stage_order[2] == Box::new(CoreStage::PreUpdate));
        assert!(scheduler.stage_order[3] == Box::new("MyStage"));
        assert!(scheduler.stage_order[4] == Box::new(CoreStage::Update));
    }

    #[test]
    #[should_panic = "Cannot add a stage before CoreStage::Startup"]
    fn add_stage_before_startup() {
        let mut scheduler = Scheduler::default();
        scheduler.add_stage_before("MyStage", CoreStage::Startup);
    }

    #[test]
    fn add_stage_after() {
        let mut scheduler = Scheduler::default();
        scheduler.add_stage_after("MyStage", CoreStage::Update);

        assert!(scheduler.stage_order[3] == Box::new(CoreStage::Update));
        assert!(scheduler.stage_order[4] == Box::new("MyStage"));
        assert!(scheduler.stage_order[5] == Box::new(CoreStage::PostUpdate));
    }

    #[test]
    fn add_startup_system() {
        let components = vec![Number(16), Number(3), Number(42)];

        let mut world = World::default();
        world.spawn_batched(components);

        let mut scheduler = Scheduler::default();

        scheduler.add_startup_system(add_five.system());
        scheduler.add_startup_system(mul_three.system());
        scheduler.add_startup_system(print_results.system());

        scheduler.execute_startup(&mut world);
    }

    #[test]
    fn add_system_to_stage() {
        let components = vec![Number(16), Number(3), Number(42)];

        let mut world = World::default();
        world.spawn_batched(components);

        let mut scheduler = Scheduler::default();

        scheduler.add_startup_system(panic.system());
        scheduler.add_system_to_stage(CoreStage::PreUpdate, add_five.system());
        scheduler.add_system_to_stage(CoreStage::PreUpdate, mul_three.system());
        scheduler.add_system_to_stage(CoreStage::Update, print_results.system());

        scheduler.execute(&mut world);
    }
}
