//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::{
    any::TypeId,
    fmt::Debug,
    ops::{Deref, DerefMut},
    sync::Arc,
};

use applab_util::{BinaryBlob, HashMap};
use parking_lot::Mutex;

use crate::query::Access;

//--------------------------------------------------------------------------------------------------
//-- TRAITS ----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub trait Resource: Debug + 'static {}

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[derive(Debug, Default)]
pub struct ResourceContainer {
    resources: HashMap<TypeId, BinaryBlob>,
    access:    Arc<Mutex<Access>>,
}

impl ResourceContainer {
    pub(crate) fn add<T: Resource>(&mut self, resource: T) -> bool {
        let type_id = TypeId::of::<T>();

        if !self.resources.contains_key(&type_id) {
            self.add_replace_internal(resource, type_id);
            return true;
        }

        false
    }

    pub(crate) fn replace<T: Resource>(&mut self, resource: T) {
        self.add_replace_internal(resource, TypeId::of::<T>());
    }

    pub(crate) fn remove<T: Resource>(&mut self) {
        let type_id = TypeId::of::<T>();
        self.resources.remove(&type_id);
    }

    #[must_use]
    pub(crate) fn get<T: Resource>(&self) -> Option<Res<T>> {
        let type_id = TypeId::of::<T>();
        let res = unsafe { &*self.resources.get(&type_id)?.ptr() };

        Some(Res::new(res, self.access.clone()))
    }

    #[must_use]
    pub(crate) fn get_mut<T: Resource>(&self) -> Option<ResMut<T>> {
        let type_id = TypeId::of::<T>();
        let res = unsafe { &mut *self.resources.get(&type_id)?.ptr_mut() };

        Some(ResMut::new(res, self.access.clone()))
    }

    #[must_use]
    pub(crate) fn contains<T: Resource>(&self) -> bool {
        let type_id = TypeId::of::<T>();
        self.resources.contains_key(&type_id)
    }

    #[inline]
    fn add_replace_internal<T: Resource>(&mut self, resource: T, type_id: TypeId) {
        let res_cell = BinaryBlob::new(resource);
        self.resources.insert(type_id, res_cell);
    }
}

//--------------------------------------------------------------------------------------------------

#[derive(Debug)]
pub struct Res<'world, T: Resource> {
    pub inner: &'world T,
    access:    Arc<Mutex<Access>>,
}

impl<'world, T: Resource> Res<'world, T> {
    pub fn new(resource: &'world T, access: Arc<Mutex<Access>>) -> Self {
        access.lock().add_read::<T>();

        Self {
            inner: resource,
            access,
        }
    }
}

impl<'world, T: Resource> Deref for Res<'world, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        self.inner
    }
}

impl<'world, T: Resource> Drop for Res<'world, T> {
    fn drop(&mut self) {
        self.access.lock().remove_read::<T>();
    }
}

//--------------------------------------------------------------------------------------------------

#[derive(Debug)]
pub struct ResMut<'world, T: Resource> {
    pub inner: &'world mut T,
    access:    Arc<Mutex<Access>>,
}

impl<'world, T: Resource> ResMut<'world, T> {
    pub fn new(resource: &'world mut T, access: Arc<Mutex<Access>>) -> Self {
        access.lock().add_write::<T>();

        Self {
            inner: resource,
            access,
        }
    }
}

impl<'world, T: Resource> Deref for ResMut<'world, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        self.inner
    }
}

impl<'world, T: Resource> DerefMut for ResMut<'world, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.inner
    }
}

impl<'world, T: Resource> Drop for ResMut<'world, T> {
    fn drop(&mut self) {
        self.access.lock().remove_write::<T>();
    }
}

//--------------------------------------------------------------------------------------------------

#[derive(Debug)]
pub struct LocalRes<'world, T>
where
    T: Resource + Default,
{
    res: Res<'world, T>,
}

impl<'world, T> From<Res<'world, T>> for LocalRes<'world, T>
where
    T: Resource + Default,
{
    fn from(res: Res<'world, T>) -> Self {
        Self { res }
    }
}

impl<'world, T> Deref for LocalRes<'world, T>
where
    T: Resource + Default,
{
    type Target = Res<'world, T>;

    fn deref(&self) -> &Self::Target {
        &self.res
    }
}

//--------------------------------------------------------------------------------------------------

#[derive(Debug)]
pub struct LocalResMut<'world, T>
where
    T: Resource + Default,
{
    pub res: ResMut<'world, T>,
}

impl<'world, T> LocalResMut<'world, T>
where
    T: Resource + Default,
{
    pub fn new(res: ResMut<'world, T>) -> Self {
        Self { res }
    }
}

impl<'world, T> Deref for LocalResMut<'world, T>
where
    T: Resource + Default,
{
    type Target = ResMut<'world, T>;

    fn deref(&self) -> &Self::Target {
        &self.res
    }
}

impl<'world, T> DerefMut for LocalResMut<'world, T>
where
    T: Resource + Default,
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.res
    }
}

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[cfg(test)]
mod tests {

    use crate as applab_ecs;
    use crate::{macros::Resource, resource::ResourceContainer};

    #[derive(Debug, Resource)]
    struct Counter(u32);

    #[test]
    fn add_remove() {
        let mut container = ResourceContainer::default();

        assert!(container.add(Counter(0)));
        assert!(container.get::<Counter>().is_some());

        container.remove::<Counter>();
        assert!(container.get::<Counter>().is_none());
    }

    #[test]
    fn add_with_existing() {
        let mut container = ResourceContainer::default();

        assert!(container.add(Counter(0)));
        assert!(!container.add(Counter(6)));
        assert_eq!(container.get::<Counter>().unwrap().inner.0, 0);
    }

    #[test]
    fn replace() {
        let mut container = ResourceContainer::default();

        assert!(container.add(Counter(0)));
        container.replace(Counter(6));
        assert_eq!(container.get::<Counter>().unwrap().inner.0, 6);
    }

    #[test]
    fn get() {
        let mut container = ResourceContainer::default();

        assert!(container.get::<Counter>().is_none());
        container.add(Counter(0));
        assert!(container.get::<Counter>().is_some());
    }

    #[test]
    fn get_mut() {
        let mut container = ResourceContainer::default();

        container.add(Counter(0));
        container.get_mut::<Counter>().unwrap().inner.0 = 6;
        assert_eq!(container.get::<Counter>().unwrap().inner.0, 6);
    }
}
