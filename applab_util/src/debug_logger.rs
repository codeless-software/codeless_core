//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::{
    io,
    sync::mpsc::{channel, Receiver, Sender},
};

use env_logger::{Builder, Env, Target};

//--------------------------------------------------------------------------------------------------
//-- PUBLIC STRUCTS --------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// A basic logger that captures stdout.
///
/// It is designed for use during tests in order to test outputs.
pub struct DebugLogger {
    tx: Receiver<u8>,
}

impl DebugLogger {
    /// Create a new logger.
    pub fn new() -> Self {
        let env = Env::default().filter_or("MY_LOG_LEVEL", "trace");
        let (rx, tx) = channel();
        Builder::from_env(env)
            .format_timestamp(None)
            .target(Target::Pipe(Box::new(WriteAdapter { sender: rx })))
            .init();

        Self { tx }
    }

    /// Collect and return outputs.
    ///
    /// # Arguments
    ///
    /// * `module_filter`: Only returns lines matching the given module.
    pub fn collect(&self, module_filter: &str) -> Vec<String> {
        String::from_utf8(self.tx.try_iter().collect::<Vec<u8>>())
            .unwrap_or(String::new())
            .split('\n')
            .filter(|s| s.contains(module_filter))
            .map(std::borrow::ToOwned::to_owned)
            .collect()
    }
}

impl Default for DebugLogger {
    fn default() -> Self {
        Self::new()
    }
}

//--------------------------------------------------------------------------------------------------
//-- PRIVATE STRUCTS -------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

struct WriteAdapter {
    sender: Sender<u8>,
}

impl io::Write for WriteAdapter {
    // On write we forward each u8 of the buffer to the sender and return the length of the buffer
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        for chr in buf {
            let _ = self.sender.send(*chr);
        }
        Ok(buf.len())
    }

    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }
}
