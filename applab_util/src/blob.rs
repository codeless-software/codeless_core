//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::{
    alloc::{alloc, dealloc, Layout},
    ptr,
    ptr::NonNull,
};

use crate::{drop_generic, CastU8};

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// A cell-like structure for storing generic binary data.
///
/// Great care must be used when dealing with this struct, as it will **NOT** prevent mutable
/// aliasing.
#[derive(Debug)]
pub struct BinaryBlob {
    ptr:     NonNull<u8>,
    layout:  Layout,
    drop_fn: unsafe fn(ptr: *mut u8),
}

impl BinaryBlob {
    /// Creates a new `BinaryBlob`.
    ///
    /// # Arguments
    ///
    /// * `value`: Data to be stored within the blob.
    pub fn new<T>(value: T) -> Self {
        let layout = Layout::new::<T>();
        let blob = unsafe { Self::new_from_layout(value.cast_u8(), layout, drop_generic::<T>) };

        std::mem::forget(value);
        blob
    }

    /// Create a new `BinaryBlob` from the given layout
    ///
    /// # Arguments
    ///
    /// * `value`: Item to be copied into the blob
    /// * `layout`: Memory layout of the item
    /// * `drop_fn`: Used to drop the value
    ///
    /// # Safety
    ///
    /// There are several things to keep in mind when using this function.
    ///
    /// 1. It **MUST** be ensured that `value` is no longer used once it has been passed into \
    ///    this function. \
    ///    This is especially true for items that contain heap allocated data (e.g. `String`).
    ///
    /// 2. `std::mem::forget` should be called for `value` directly after this function is called. \
    ///    Not doing so can cause a double free for all non copy types.  \
    ///    Using `std::mem::forget` also ensures that the value cannot be used afterwards (as per point 1).
    ///
    /// 3. `layout` **MUST** match the memory layout of `value`.
    ///
    /// 4. Don't use this function unless you absolutely must. \
    ///    The `new` function is easier to use and takes care of the above points for you.
    pub unsafe fn new_from_layout(value: *const u8, layout: Layout, drop_fn: unsafe fn(ptr: *mut u8)) -> Self {
        let ptr = if layout.size() == 0 {
            NonNull::dangling()
        } else {
            let data = alloc(layout);
            ptr::copy_nonoverlapping(value, data, layout.size());
            NonNull::new(data).expect("Failed to create NonNull")
        };

        Self { ptr, layout, drop_fn }
    }

    /// Return a const pointer to `T`.
    ///
    /// # Panics
    /// Panics if `self.layout` does not match `Layout<T>`.
    #[inline]
    pub fn ptr<T>(&self) -> *const T {
        assert_eq!(self.layout, Layout::new::<T>());
        self.ptr.as_ptr().cast()
    }

    /// Return a mut pointer to `T`.
    ///
    /// # Panics
    /// Panics if `self.layout` does not match `Layout<T>`.
    #[inline]
    pub fn ptr_mut<T>(&self) -> *mut T {
        assert_eq!(self.layout, Layout::new::<T>());
        self.ptr.as_ptr().cast()
    }
}

impl Drop for BinaryBlob {
    fn drop(&mut self) {
        let ptr = self.ptr.as_ptr();

        unsafe {
            (self.drop_fn)(ptr);

            // Make sure we don't deallocate on zero sized types.
            if self.layout.size() != 0 {
                dealloc(ptr, self.layout);
            }
        };
    }
}

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use crate::BinaryBlob;

    struct Unsized;

    #[test]
    fn get() {
        let item = "Foo".to_owned();

        unsafe {
            let cell = BinaryBlob::new(item);
            assert_eq!(*cell.ptr::<String>(), "Foo".to_owned());

            *cell.ptr_mut::<String>() = "Bar".to_owned();
            assert_eq!(*cell.ptr::<String>(), "Bar".to_owned());
        }
    }

    // NOTE: This test is designed for use with Miri.
    //       Miri will fail if it performs an invalid operation.
    #[test]
    #[allow(unused)]
    fn get_unsized() {
        let item = Unsized;

        unsafe {
            let cell = BinaryBlob::new(item);

            let item = cell.ptr::<Unsized>();
            let item = cell.ptr_mut::<Unsized>();
            *item = Unsized;
        }
    }
}
