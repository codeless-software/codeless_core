//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

mod blob;
mod blob_vec;
mod cast_u8;
mod debug_logger;
mod hash;
pub mod macros;

use std::{any::type_name, ptr};

pub use blob::BinaryBlob;
pub use blob_vec::BlobVec;
pub use cast_u8::CastU8;
pub use debug_logger::DebugLogger;
pub use hash::{BiHashMap, HashMap, HashSet};

//--------------------------------------------------------------------------------------------------
//-- FUNCTIONS -------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// Calls the `Drop` implementation for the type `T` on the value at the given memory location.
///
/// # Safety
/// This function is unsafe because it can be used to cause undefined behavior by calling the `Drop` implementation for a type with invalid or uninitialized data.
///
/// # Examples
/// ```
/// # use std::alloc::{dealloc, Layout};
/// # use applab_util::drop_generic;
/// let x = Box::new(42);
/// let ptr = Box::into_raw(x) as *mut u8;
///
/// unsafe {
///     drop_generic::<i32>(ptr);
///
///     // `drop_generic` will not deallocate memory.
///     // This must be handled by the user to prevent memory leaks.
///     dealloc(ptr, Layout::new::<i32>())
/// };
/// ```
pub unsafe fn drop_generic<T>(ptr: *mut u8) {
    ptr::drop_in_place(ptr.cast::<T>());
}

//--------------------------------------------------------------------------------------------------

/// Returns the short type name of `T`, as a string.
///
/// The short type name is the last component of the full type name, after the last `"::"` substring.
///
/// # Examples
/// ```
/// # use applab_util::short_type_name;
/// assert_eq!(short_type_name::<i32>(), "i32");
/// assert_eq!(short_type_name::<Vec<i32>>(), "Vec<i32>");
/// ```
///
/// # Panics
/// This function panics if it fails to get the short type name, which can occur if the `type_name::<T>()` function returns `None`.
///
/// # BUGS
/// This function may produce incorrect results if the full type name does not have the expected format.
pub fn short_type_name<T>() -> &'static str {
    type_name::<T>()
        .split("::")
        .last()
        .expect("(BUG) Failed to get short type name")
}

//--------------------------------------------------------------------------------------------------

/// Get the type name of a value.
///
/// This utility function is in place until [`std::any::type_name_of_val`] is stabilised, at which
/// point it will be deprecated.
pub fn type_name_of_val<T: ?Sized>(_val: &T) -> &'static str {
    type_name::<T>()
}
