//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

/// Utility trait for casting objects to u8 pointers.
///
/// It is mainly useful for working with binary data.
pub trait CastU8 {
    /// Cast a reference to self as a `*const u8`.
    fn cast_u8(&self) -> *const u8;

    /// Cast a reference to self as a `*mut u8`.
    fn cast_u8_mut(&self) -> *mut u8;
}

impl<T> CastU8 for T {
    fn cast_u8(&self) -> *const u8 {
        (self as *const T).cast::<u8>()
    }

    fn cast_u8_mut(&self) -> *mut u8 {
        self.cast_u8() as *mut _
    }
}
