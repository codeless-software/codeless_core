//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::{
    alloc::{self, alloc, dealloc, realloc, Layout},
    cmp,
    ptr::{self, NonNull},
};

use thiserror::Error;

use crate::blob_vec::MemReserveError::CapacityOverflow;

//--------------------------------------------------------------------------------------------------
//-- ENUMS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[derive(Error, Debug)]
pub enum MemReserveError {
    #[error("capacity overflow")]
    CapacityOverflow,
}

//--------------------------------------------------------------------------------------------------
//-- PUBLIC STRUCTS --------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// [`Vec`]-like structure for storing binary data.
#[derive(Debug)]
pub struct BlobVec {
    buf:     RawBlobVec,
    len:     usize,
    drop_fn: unsafe fn(ptr: *mut u8),
}

impl BlobVec {
    /// Create a new [`BlobVec`].
    #[inline]
    pub fn new(layout: Layout, drop_fn: unsafe fn(ptr: *mut u8)) -> Self {
        Self {
            buf: RawBlobVec::new(layout),
            len: 0,
            drop_fn,
        }
    }

    /// Reserves capacity for at least `additional` more elements.
    ///
    /// More space may be reserved to avoid frequent allocations.
    #[inline]
    pub fn reserve(&mut self, additional: usize) {
        // No need to reserve any space for zero sized types
        if self.is_zero() {
            return;
        }

        if self.needs_to_grow(additional) {
            self.buf
                .grow_amortized(self.len, additional)
                .expect("Failed to grow BlobVec buffer");
        }
    }

    /// Adds an additional element to the [`BlobVec`].
    ///
    /// Internally, it does this by copying the provided `elem` pointer to a new
    /// memory location.
    ///
    /// # Safety
    /// `elem` must be forgotten after calling this function to prevent issues such as double free.\
    /// This can be done via `std::mem::forget(elem)`.
    ///
    /// `elem` must match the memory layout of the [`BlobVec`] instance.
    #[inline]
    pub unsafe fn push(&mut self, elem: *const u8) {
        if !self.is_zero() {
            if self.len == self.capacity() {
                self.buf.grow();
            }

            self.buf.set_unchecked(self.len, elem);
        }

        self.len += 1;
    }

    /// Returns a pointer to the data at the given `index`.
    pub const fn get(&self, index: usize) -> Option<*const u8> {
        if index >= self.len {
            return None;
        }

        // SAFETY: The index is guaranteed to be within the array bounds.
        unsafe { Some(self.get_unchecked(index)) }
    }

    /// Return a pointer to the data at the given `index` without bounds checking.
    ///
    /// # Safety
    /// The index must be less than [`BlobVec::size`] to avoid accessing uninitialised data
    /// or reaching outside the array bounds.
    pub const unsafe fn get_unchecked(&self, index: usize) -> *const u8 {
        self.as_ptr().add(index * self.buf.layout.size())
    }

    /// Returns a mutable pointer to the data at the given `index`.
    pub fn get_mut(&mut self, index: usize) -> Option<*mut u8> {
        if index >= self.len {
            return None;
        }

        // SAFETY: The index is guaranteed to be within the array bounds.
        unsafe { Some(self.get_unchecked_mut(index)) }
    }

    /// Return a mutable pointer to the data at the given `index` without bounds checking.
    ///
    /// # Safety
    /// The index must be less than [`BlobVec::size`] to avoid accessing uninitialised data
    /// or reaching outside the array bounds.
    pub unsafe fn get_unchecked_mut(&mut self, index: usize) -> *mut u8 {
        self.as_ptr_mut().add(index * self.buf.layout.size())
    }

    /// Removes an element from the [`BlobVec`] and returns it.
    ///
    /// The removed element is replaced by the last element of the [`BlobVec`].
    /// Returns [`None`] if the layout is zero sized.
    #[inline]
    pub fn swap_remove(&mut self, index: usize) -> Option<*mut u8> {
        let len = self.len();
        debug_assert!(index < len);

        self.len -= 1;

        if self.is_zero() {
            return None;
        }

        // SAFETY: The swap allocation has the same layout as the vec elements, and all indices
        //         are guaranteed to be within the array bounds.
        unsafe {
            let swap = alloc(self.buf.layout);

            ptr::copy_nonoverlapping(self.buf.get_unchecked(index), swap, self.buf.layout.size());
            ptr::copy(
                self.buf.get_unchecked(len - 1),
                self.buf.get_unchecked(index),
                self.buf.layout.size(),
            );
            Some(swap)
        }
    }

    /// Removes an element from the [`BlobVec`] and drops it.
    ///
    /// The removed element is replaced by the last element of the [`BlobVec`].
    #[inline]
    pub fn swap_drop(&mut self, index: usize) {
        if let Some(ptr) = self.swap_remove(index) {
            // SAFETY: The memory being dropped and deallocated is within bounds and allocated.
            //         The bounds guarantee is made by the preceding swap_remove function.
            unsafe {
                (self.drop_fn)(ptr);
                dealloc(ptr, self.layout());
            }
        }
    }

    /// Replace an existing element with a new one.
    ///
    /// The old value is dropped.
    ///
    /// # Arguments
    ///
    /// * `index`: The index of the element to replace.
    /// * `elem`: New data to add.
    ///
    /// # Safety
    ///
    /// `index` must be less than [`BlobVec::size`].  
    ///
    /// `elem` must match the memory layout of the [`BlobVec`] instance.
    pub unsafe fn replace(&mut self, index: usize, elem: *const u8) {
        let len = self.len();
        debug_assert!(index < len);

        if self.is_zero() {
            return;
        }

        let ptr = self.get_unchecked_mut(index);
        (self.drop_fn)(ptr);

        ptr::copy_nonoverlapping(elem, ptr, self.layout().size());
    }

    /// Clear all elements.
    #[inline]
    pub fn clear(&mut self) {
        let len = self.len;

        self.len = 0;
        for i in 0..len {
            // SAFETY: The ptr is guaranteed to be within the array bounds and pointing to
            //         allocated memory, and the layout is correct for deallocating the memory.
            unsafe {
                let ptr = self.get_unchecked_mut(i);
                (self.drop_fn)(ptr);
            }
        }
    }

    /// Returns true if the [`BlobVec`] has no elements.
    #[inline]
    pub const fn is_empty(&self) -> bool {
        self.len == 0
    }

    /// Returns whether the stored element is zero sized.
    #[inline]
    pub const fn is_zero(&self) -> bool {
        self.capacity() == !0
    }

    /// Returns the length of the [`BlobVec`].
    #[inline]
    pub const fn len(&self) -> usize {
        self.len
    }

    /// Returns the number of elements the [`BlobVec`] can hold without reallocating.
    #[inline]
    pub const fn capacity(&self) -> usize {
        self.buf.cap
    }

    /// Returns the layout of the binary data stored in the [`BlobVec`].
    #[inline]
    pub const fn layout(&self) -> Layout {
        self.buf.layout
    }

    /// Returns a pointer to the underlying array.
    #[inline]
    pub const fn as_ptr(&self) -> *const u8 {
        self.buf.ptr.as_ptr()
    }

    /// Returns a mutable pointer to the underlying array.
    #[inline]
    pub fn as_ptr_mut(&mut self) -> *mut u8 {
        self.buf.ptr.as_ptr()
    }

    /// Returns whether the [`BlobVec`] needs to grow to store `additional` elements.
    #[inline]
    const fn needs_to_grow(&self, additional: usize) -> bool {
        additional > self.capacity() - self.len
    }
}

impl Drop for BlobVec {
    fn drop(&mut self) {
        // There's nothing to free if we have an unsized type.
        if self.is_zero() {
            return;
        }

        let allocation = self.as_ptr_mut();
        let size = self.buf.layout.size();

        for index in 0..self.len {
            // SAFETY: Each element being dropped is allocated and of the correct layout.
            unsafe {
                let ptr = allocation.add(size * index);
                (self.drop_fn)(ptr);
            }
        }
    }
}

//--------------------------------------------------------------------------------------------------
//-- PRIVATE STRUCTS -------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[derive(Debug)]
struct RawBlobVec {
    ptr:    NonNull<u8>,
    cap:    usize,
    layout: Layout,
}

impl RawBlobVec {
    #[inline]
    const fn new(layout: Layout) -> Self {
        // !0 is usize::MAX. The branch should be stripped at compile time.
        let cap = if layout.size() == 0 { !0 } else { 0 };
        Self {
            ptr: NonNull::dangling(),
            cap,
            layout,
        }
    }

    // NOTE: Adapted from RawVec::MIN_NON_ZERO_CAP in the standard library.
    //       See there for an explanation of what's going on.
    #[inline]
    const fn min_non_zero_cap(&self) -> usize {
        if self.layout.size() == 1 {
            8
        } else if self.layout.size() <= 1024 {
            4
        } else {
            1
        }
    }

    fn grow(&mut self) {
        debug_assert_ne!(self.layout.size(), 0, "Capacity overflow");

        let (new_cap, new_layout) = if self.cap == 0 {
            (1, self.layout)
        } else {
            let new_cap = 2 * self.cap;
            let new_layout = repeat(&self.layout, new_cap).unwrap().0;
            (new_cap, new_layout)
        };

        // Ensure that the new allocation doesn't exceed 'isize::MAX'.
        debug_assert!(new_layout.size() <= isize::MAX as usize, "Allocation too large");
        self.finish_grow(new_cap, new_layout);
    }

    fn grow_amortized(&mut self, len: usize, additional: usize) -> Result<(), MemReserveError> {
        debug_assert!(additional > 0);

        // Since we return a capacity of `usize::MAX` when the layout size is 0,
        // getting to here means the `RawBlobVec` overfull.
        if self.layout.size() == 0 {
            return Err(CapacityOverflow);
        }

        let required_capacity = len.checked_add(additional).ok_or(CapacityOverflow)?;

        let new_cap = cmp::max(self.cap * 2, required_capacity);
        let new_cap = cmp::max(self.min_non_zero_cap(), new_cap);
        let new_layout = repeat(&self.layout, new_cap).unwrap().0;

        self.finish_grow(new_cap, new_layout);
        Ok(())
    }

    fn finish_grow(&mut self, new_cap: usize, new_layout: Layout) {
        let new_ptr = if self.cap == 0 {
            unsafe { alloc(new_layout) }
        } else {
            let old_layout = repeat(&self.layout, self.cap).unwrap().0;
            let old_ptr = self.ptr.as_ptr();
            unsafe { realloc(old_ptr, old_layout, new_layout.size()) }
        };

        // If the allocation fails, 'new_ptr' will be null, in which case we abort
        self.ptr = NonNull::new(new_ptr).map_or_else(|| alloc::handle_alloc_error(new_layout), |p| p);

        self.cap = new_cap;
    }

    /// # Safety
    ///
    /// The caller must ensure that `index` is within bounds.
    ///
    /// Must not be used with unsized types.
    #[inline]
    unsafe fn set_unchecked(&mut self, index: usize, value: *const u8) {
        let ptr = self.get_unchecked(index);
        ptr::copy_nonoverlapping(value, ptr, self.layout.size());
    }

    /// # Safety
    ///
    /// The caller must ensure that `index` is within bounds.
    ///
    /// Must not be used with unsized types.
    #[inline]
    unsafe fn get_unchecked(&mut self, index: usize) -> *mut u8 {
        self.ptr.as_ptr().add(index * self.layout.size())
    }
}

impl Drop for RawBlobVec {
    fn drop(&mut self) {
        let elem_size = self.layout.size();

        if self.cap != 0 && elem_size != 0 {
            unsafe { dealloc(self.ptr.as_ptr(), repeat(&self.layout, self.cap).unwrap().0) }
        }
    }
}

//--------------------------------------------------------------------------------------------------
//-- FUNCTIONS -------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

// TODO: Replace with Layout::repeat if/when it becomes stable
/// From <https://doc.rust-lang.org/beta/src/core/alloc/layout.rs.html/>
#[inline]
fn repeat(layout: &Layout, n: usize) -> Option<(Layout, usize)> {
    // This cannot overflow. Quoting from the invariant of Layout:
    // > `size`, when rounded up to the nearest multiple of `align`,
    // > must not overflow (i.e., the rounded value must be less than
    // > `usize::MAX`)
    let padded_size = layout.size() + padding_needed_for(layout, layout.align());
    let alloc_size = padded_size.checked_mul(n)?;

    // SAFETY: self.align is already known to be valid and alloc_size has been
    // padded already.
    unsafe {
        Some((
            Layout::from_size_align_unchecked(alloc_size, layout.align()),
            padded_size,
        ))
    }
}

/// From <https://doc.rust-lang.org/beta/src/core/alloc/layout.rs.html/>
const fn padding_needed_for(layout: &Layout, align: usize) -> usize {
    let len = layout.size();

    // Rounded up value is:
    //   len_rounded_up = (len + align - 1) & !(align - 1);
    // and then we return the padding difference: `len_rounded_up - len`.
    //
    // We use modular arithmetic throughout:
    //
    // 1. align is guaranteed to be > 0, so align - 1 is always
    //    valid.
    //
    // 2. `len + align - 1` can overflow by at most `align - 1`,
    //    so the &-mask with `!(align - 1)` will ensure that in the
    //    case of overflow, `len_rounded_up` will itself be 0.
    //    Thus the returned padding, when added to `len`, yields 0,
    //    which trivially satisfies the alignment `align`.
    //
    // (Of course, attempts to allocate blocks of memory whose
    // size and padding overflow in the above manner should cause
    // the allocator to yield an error anyway.)

    let len_rounded_up = len.wrapping_add(align).wrapping_sub(1) & !align.wrapping_sub(1);
    len_rounded_up.wrapping_sub(len)
}

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[allow(clippy::missing_safety_doc, clippy::undocumented_unsafe_blocks, clippy::unwrap_used)]
#[cfg(test)]
mod tests {
    use std::{
        alloc::{dealloc, Layout},
        mem,
    };

    use crate::{drop_generic, BlobVec, CastU8};

    struct Unsized;

    fn get_layout<T>() -> (Layout, unsafe fn(ptr: *mut u8)) {
        (Layout::new::<T>(), drop_generic::<T>)
    }

    #[test]
    fn new() {
        let (layout, drop_fn) = get_layout::<String>();
        let vec = BlobVec::new(layout, drop_fn);

        assert_eq!(vec.len(), 0);
        assert_eq!(vec.capacity(), 0);
    }

    #[test]
    fn new_unsized() {
        let (layout, drop_fn) = get_layout::<Unsized>();
        let vec = BlobVec::new(layout, drop_fn);

        assert_eq!(vec.len(), 0);
        assert_eq!(vec.capacity(), !0);
    }

    #[test]
    fn reserve() {
        let (layout, drop_fn) = get_layout::<String>();
        let mut vec = BlobVec::new(layout, drop_fn);

        vec.reserve(10);
        assert_eq!(vec.capacity(), 10);

        vec.len = 5;

        vec.reserve(10);
        assert_eq!(vec.capacity(), 20);

        vec.len = 0;
    }

    #[test]
    fn push_and_get() {
        let (layout, drop_fn) = get_layout::<String>();
        let mut vec = BlobVec::new(layout, drop_fn);

        let hello = "Hello".to_owned();
        let world = "World!".to_owned();

        unsafe {
            vec.push(hello.cast_u8());
            vec.push(world.cast_u8());
        }

        // Ensure that we forget the string values.
        // Not doing this will cause undefined behaviour in the form of a double free.
        mem::forget(hello);
        mem::forget(world);

        assert_eq!(vec.len(), 2);
        assert_eq!(vec.capacity(), 2);

        let (hello, world) = unsafe {
            (
                &*vec.get(0).unwrap().cast::<String>(),
                &*vec.get(1).unwrap().cast::<String>(),
            )
        };

        assert_eq!(*hello, "Hello");
        assert_eq!(*world, "World!");
        assert_eq!(vec.get(2), None);
    }

    #[test]
    fn push_and_get_unsized() {
        let (layout, drop_fn) = get_layout::<Unsized>();
        let mut vec = BlobVec::new(layout, drop_fn);

        unsafe {
            vec.push(Unsized.cast_u8());
            vec.push(Unsized.cast_u8());
        }

        assert_eq!(vec.len(), 2);
        assert_eq!(vec.capacity(), !0);

        let (_e1, _e2) = unsafe {
            (
                &*vec.get(0).unwrap().cast::<Unsized>(),
                &*vec.get(1).unwrap().cast::<Unsized>(),
            )
        };
    }

    #[test]
    fn get_mut() {
        let (layout, drop_fn) = get_layout::<String>();
        let mut vec = BlobVec::new(layout, drop_fn);

        let hello = "Hello".to_owned();
        let world = "World!".to_owned();

        unsafe {
            vec.push(hello.cast_u8());
            vec.push(world.cast_u8());
        }

        // Ensure that we forget the string values.
        // Not doing this will cause undefined behaviour in the form of a double free.
        mem::forget(hello);
        mem::forget(world);

        unsafe {
            *vec.get_mut(0).unwrap().cast::<String>() = "Greetings".to_owned();
            *vec.get_mut(1).unwrap().cast::<String>() = "citizen!".to_owned();
        }

        let (citizen, greetings) = unsafe {
            (
                &*vec.get(1).unwrap().cast::<String>(),
                &*vec.get(0).unwrap().cast::<String>(),
            )
        };

        assert_eq!(*greetings, "Greetings");
        assert_eq!(*citizen, "citizen!");
        assert_eq!(vec.get_mut(2), None);
    }

    #[test]
    fn swap_remove() {
        let (layout, drop_fn) = get_layout::<String>();
        let mut vec = BlobVec::new(layout, drop_fn);

        let hello = "Hello".to_owned();
        let world = "World".to_owned();
        let ex = "!".to_owned();

        unsafe {
            vec.push(hello.cast_u8());
            vec.push(world.cast_u8());
            vec.push(ex.cast_u8());
        }

        // Ensure that we forget the string values.
        // Not doing this will cause undefined behaviour in the form of a double free.
        mem::forget(hello);
        mem::forget(world);
        mem::forget(ex);

        let (world_ptr, hello, ex) = unsafe {
            (
                vec.swap_remove(1).unwrap().cast::<String>(),
                &*vec.get(0).unwrap().cast::<String>(),
                &*vec.get(1).unwrap().cast::<String>(),
            )
        };

        let world = unsafe { &*world_ptr };

        assert_eq!(*hello, "Hello");
        assert_eq!(*world, "World");
        assert_eq!(*ex, "!");

        assert_eq!(vec.len(), 2);

        // The user is responsible for freeing the pointer returned by swap_remove.
        unsafe {
            let world_ptr = world_ptr.cast();

            (vec.drop_fn)(world_ptr);
            dealloc(world_ptr, vec.layout());
        }
    }

    #[test]
    fn replace() {
        let (layout, drop_fn) = get_layout::<String>();
        let mut vec = BlobVec::new(layout, drop_fn);

        let hello = "Hello".to_owned();
        let world = "World".to_owned();
        let ex = "!".to_owned();

        unsafe {
            vec.push(hello.cast_u8());
            vec.push(world.cast_u8());
            vec.push(ex.cast_u8());
        }

        // Ensure that we forget the string values.
        // Not doing this will cause undefined behaviour in the form of a double free.
        mem::forget(hello);
        mem::forget(world);
        mem::forget(ex);

        let (hello, new, ex) = unsafe {
            let new = "you".to_owned();
            vec.replace(1, new.cast_u8());
            mem::forget(new);
            (
                &*vec.get(0).unwrap().cast::<String>(),
                &*vec.get(1).unwrap().cast::<String>(),
                &*vec.get(2).unwrap().cast::<String>(),
            )
        };

        assert_eq!(*hello, "Hello");
        assert_eq!(*new, "you");
        assert_eq!(*ex, "!");

        assert_eq!(vec.len(), 3);
    }

    // This test is for detecting memory issues with MIRI.
    #[test]
    fn replace_unsized() {
        let (layout, drop_fn) = get_layout::<Unsized>();
        let mut vec = BlobVec::new(layout, drop_fn);

        unsafe {
            vec.push(Unsized.cast_u8());
            vec.push(Unsized.cast_u8());
            vec.push(Unsized.cast_u8());

            vec.replace(1, Unsized.cast_u8());
        }

        assert_eq!(vec.len(), 3);
    }

    #[test]
    fn clear() {
        let (layout, drop_fn) = get_layout::<String>();
        let mut vec = BlobVec::new(layout, drop_fn);

        for i in 0..5 {
            let entry = format!("Entry0{i}");
            unsafe {
                vec.push(entry.cast_u8());
            }

            mem::forget(entry);
        }

        assert_eq!(vec.len, 5);
        assert_eq!(vec.capacity(), 8);

        vec.clear();

        assert_eq!(vec.len, 0);
        assert_eq!(vec.capacity(), 8);
    }
}
