//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- ENUMS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// An enum representing a fill for a shape, which can be either no fill (`None`) or a solid color (`Solid`).
pub enum Fill {
    /// No fill for the shape.
    None,

    /// A solid color fill for the shape.
    Solid(Colour),
}

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// A struct representing a color with red, green, blue, and alpha channels.
///
/// Each channel is an f32 between 0.0 and 1.0.
pub struct Colour {
    /// The red channel.
    pub r: f32,

    /// The green channel.
    pub g: f32,

    /// The blue channel.
    pub b: f32,

    /// The alpha channel.
    pub a: f32,
}

impl Colour {
    /// Create a new `Colour` instance from RGB values.
    ///
    /// The alpha channel will be set to 1.0.
    ///
    /// # Arguments
    ///
    /// * `r` - A float value between 0.0 and 1.0 representing the red channel.
    /// * `g` - A float value between 0.0 and 1.0 representing the green channel.
    /// * `b` - A float value between 0.0 and 1.0 representing the blue channel.
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_draw::colour::Colour;
    /// let c = Colour::from_rgb(1.0, 0.0, 0.0);
    /// ```
    pub fn from_rgb(r: f32, g: f32, b: f32) -> Self {
        Self { r, g, b, a: 1.0 }
    }

    /// Create a new `Colour` instance from RGBA values.
    ///
    /// # Arguments
    ///
    /// * `r` - A float value between 0.0 and 1.0 representing the red channel.
    /// * `g` - A float value between 0.0 and 1.0 representing the green channel.
    /// * `b` - A float value between 0.0 and 1.0 representing the blue channel.
    /// * `a` - A float value between 0.0 and 1.0 representing the alpha channel.
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_draw::colour::Colour;
    /// let c = Colour::from_rgba(1.0, 0.0, 0.0, 1.0);
    /// ```
    pub fn from_rgba(r: f32, g: f32, b: f32, a: f32) -> Self {
        Self { r, g, b, a }
    }

    /// Create a new `Colour` instance from a hexadecimal string.
    ///
    /// Returns `None` if the hex string is invalid.
    ///
    /// # Arguments
    ///
    /// * `hex` - A hexadecimal string in the format "#RRGGBBAA".
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_draw::colour::Colour;
    /// let c = Colour::from_hex("#ff0000ff").unwrap();
    /// ```
    // TODO: Add support for hex values without alpha.
    pub fn from_hex(hex: &str) -> Option<Self> {
        if hex.len() != 9 || !hex.starts_with('#') {
            return None;
        }

        let hex_value = u32::from_str_radix(&hex[1..], 16).ok()?;
        let r = ((hex_value >> 24) & 0xff) as f32 / 255.0;
        let g = ((hex_value >> 16) & 0xff) as f32 / 255.0;
        let b = ((hex_value >> 8) & 0xff) as f32 / 255.0;
        let a = (hex_value & 0xff) as f32 / 255.0;

        Some(Self { r, g, b, a })
    }

    /// Create a new `Colour` instance from HSV (hue, saturation, value) values.
    ///
    /// # Arguments
    /// * `h` - A float representing the hue value. The value must be between 0.0 and 360.0.
    /// * `s` - A float representing the saturation value. The value must be between 0.0 and 1.0.
    /// * `v` - A float representing the value/brightness value. The value must be between 0.0 and 1.0.
    ///
    /// # Example
    /// ```
    /// # use applab_draw::colour::Colour;
    /// let colour = Colour::from_hsv(60.0, 0.5, 1.0);
    /// ```
    pub fn from_hsv(h: f32, s: f32, v: f32) -> Self {
        let c = v * s;
        let x = c * (1.0 - ((h / 60.0) % 2.0 - 1.0).abs());
        let m = v - c;

        let (r1, g1, b1) = match h as i32 / 60 {
            0 => (c, x, 0.0),
            1 => (x, c, 0.0),
            2 => (0.0, c, x),
            3 => (0.0, x, c),
            4 => (x, 0.0, c),
            _ => (c, 0.0, x),
        };

        Self {
            r: r1 + m,
            g: g1 + m,
            b: b1 + m,
            a: 1.0,
        }
    }

    /// Create a new `Colour` instance from HSL (hue, saturation, lightness) values.
    ///
    /// # Arguments
    /// * `h` - A float representing the hue value. The value must be between 0.0 and 360.0.
    /// * `s` - A float representing the saturation value. The value must be between 0.0 and 1.0.
    /// * `l` - A float representing the lightness value. The value must be between 0.0 and 1.0.
    ///
    /// # Example
    /// ```
    /// # use applab_draw::colour::Colour;
    /// let colour = Colour::from_hsl(60.0, 0.5, 0.5);
    /// ```
    pub fn from_hsl(h: f32, s: f32, l: f32) -> Self {
        let c = (1.0 - (2.0 * l - 1.0).abs()) * s;
        let x = c * (1.0 - ((h / 60.0) % 2.0 - 1.0).abs());
        let m = l - c / 2.0;

        let (r1, g1, b1) = match h as i32 / 60 {
            0 => (c, x, 0.0),
            1 => (x, c, 0.0),
            2 => (0.0, c, x),
            3 => (0.0, x, c),
            4 => (x, 0.0, c),
            _ => (c, 0.0, x),
        };

        Self {
            r: r1 + m,
            g: g1 + m,
            b: b1 + m,
            a: 1.0,
        }
    }
}

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use crate::colour::Colour;

    #[test]
    fn test_from_hsv() {
        // Test red
        let colour = Colour::from_hsv(0.0, 1.0, 1.0);
        assert_eq!(colour.r, 1.0);
        assert_eq!(colour.g, 0.0);
        assert_eq!(colour.b, 0.0);

        // Test green
        let colour = Colour::from_hsv(120.0, 1.0, 1.0);
        assert_eq!(colour.r, 0.0);
        assert_eq!(colour.g, 1.0);
        assert_eq!(colour.b, 0.0);

        // Test blue
        let colour = Colour::from_hsv(240.0, 1.0, 1.0);
        assert_eq!(colour.r, 0.0);
        assert_eq!(colour.g, 0.0);
        assert_eq!(colour.b, 1.0);

        // Test yellow
        let colour = Colour::from_hsv(60.0, 1.0, 1.0);
        assert_eq!(colour.r, 1.0);
        assert_eq!(colour.g, 1.0);
        assert_eq!(colour.b, 0.0);

        // Test black
        let colour = Colour::from_hsv(0.0, 0.0, 0.0);
        assert_eq!(colour.r, 0.0);
        assert_eq!(colour.g, 0.0);
        assert_eq!(colour.b, 0.0);

        // Test white
        let colour = Colour::from_hsv(0.0, 0.0, 1.0);
        assert_eq!(colour.r, 1.0);
        assert_eq!(colour.g, 1.0);
        assert_eq!(colour.b, 1.0);
    }

    #[test]
    fn test_from_hsl() {
        // Test red
        let colour = Colour::from_hsl(0.0, 1.0, 0.5);
        assert_eq!(colour.r, 1.0);
        assert_eq!(colour.g, 0.0);
        assert_eq!(colour.b, 0.0);
        assert_eq!(colour.a, 1.0);

        // Test green
        let colour = Colour::from_hsl(120.0, 1.0, 0.5);
        assert_eq!(colour.r, 0.0);
        assert_eq!(colour.g, 1.0);
        assert_eq!(colour.b, 0.0);
        assert_eq!(colour.a, 1.0);

        // Test blue
        let colour = Colour::from_hsl(240.0, 1.0, 0.5);
        assert_eq!(colour.r, 0.0);
        assert_eq!(colour.g, 0.0);
        assert_eq!(colour.b, 1.0);
        assert_eq!(colour.a, 1.0);

        // Test yellow
        let colour = Colour::from_hsl(60.0, 1.0, 0.5);
        assert_eq!(colour.r, 1.0);
        assert_eq!(colour.g, 1.0);
        assert_eq!(colour.b, 0.0);
        assert_eq!(colour.a, 1.0);

        // Test black
        let colour = Colour::from_hsl(0.0, 0.0, 0.0);
        assert_eq!(colour.r, 0.0);
        assert_eq!(colour.g, 0.0);
        assert_eq!(colour.b, 0.0);
        assert_eq!(colour.a, 1.0);

        // Test white
        let colour = Colour::from_hsl(0.0, 0.0, 1.0);
        assert_eq!(colour.r, 1.0);
        assert_eq!(colour.g, 1.0);
        assert_eq!(colour.b, 1.0);
        assert_eq!(colour.a, 1.0);
    }
}
