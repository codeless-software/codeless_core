//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub mod colour;
pub mod primitives;

//--------------------------------------------------------------------------------------------------
//-- ENUMS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[derive(thiserror::Error, Debug)]
pub enum PresentError {
    #[error("There is no memory left to allocate a new frame")]
    OutOfMemory,
    #[error("{0}")]
    Other(String),
    #[error("An unknown error has occurred")]
    Unknown,
}

//--------------------------------------------------------------------------------------------------
//-- TRAITS ----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// Provides drawing functions.
///
/// It is expected for there to be a different implementation for each available backend.
/// For example, a WgpuCanvas for a hardware accelerated Wgpu backend, or a
/// HtmlCanvas for a web based backend.
pub trait Canvas {
    /// Resize the canvas.
    ///
    /// This is normally called when the underlying render surface has been resized.
    /// For example, when the window has been resized.
    fn resize(&mut self, width: u32, height: u32);

    /// Processes all queued draw commands and presents the rendered buffer.
    fn present(&mut self) -> Result<(), PresentError>;
}
