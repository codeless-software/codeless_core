//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[derive(Debug)]
pub struct State {
    pub surface: wgpu::Surface,
    pub device:  wgpu::Device,
    pub queue:   wgpu::Queue,
    pub config:  wgpu::SurfaceConfiguration,
    pub width:   u32,
    pub height:  u32,
}

impl State {
    pub(crate) fn new<W>(window_handle: &W, width: u32, height: u32) -> Self
    where
        W: raw_window_handle::HasRawWindowHandle + raw_window_handle::HasRawDisplayHandle,
    {
        pollster::block_on(init_state(window_handle, width, height))
    }
}

//--------------------------------------------------------------------------------------------------
//-- PRIVATE FUNCTIONS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

async fn init_state<W>(window_handle: &W, width: u32, height: u32) -> State
where
    W: raw_window_handle::HasRawWindowHandle + raw_window_handle::HasRawDisplayHandle,
{
    let instance = wgpu::Instance::new(wgpu::InstanceDescriptor::default());
    let surface = unsafe { instance.create_surface(window_handle) }.unwrap();
    let adapter = instance
        .request_adapter(&wgpu::RequestAdapterOptions {
            power_preference:       wgpu::PowerPreference::default(),
            compatible_surface:     Some(&surface),
            force_fallback_adapter: false,
        })
        .await
        .unwrap();

    let (device, queue) = adapter
        .request_device(
            &wgpu::DeviceDescriptor {
                label:    None,
                features: wgpu::Features::empty(),
                limits:   if cfg!(target_arch = "wasm32") {
                    wgpu::Limits::downlevel_webgl2_defaults()
                } else {
                    wgpu::Limits::default()
                },
            },
            None,
        )
        .await
        .unwrap();

    let config = wgpu::SurfaceConfiguration {
        usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
        format: surface.get_capabilities(&adapter).formats[1],
        ..surface.get_default_config(&adapter, width, height).unwrap()
    };
    surface.configure(&device, &config);

    State {
        surface,
        device,
        queue,
        config,
        width,
        height,
    }
}
