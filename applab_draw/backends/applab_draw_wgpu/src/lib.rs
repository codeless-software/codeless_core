//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

extern crate core;

pub(crate) mod pipeline;
pub(crate) mod shader;
pub(crate) mod state;

use applab_draw::{Canvas, PresentError};

use crate::{
    pipeline::{example::ExamplePipeline, Pipeline},
    state::State,
};

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[derive(Debug)]
pub struct WgpuCanvas {
    state:            State,
    example_pipeline: ExamplePipeline,
}

impl WgpuCanvas {
    pub fn new<W>(window_handle: &W, width: u32, height: u32) -> Self
    where
        W: raw_window_handle::HasRawWindowHandle + raw_window_handle::HasRawDisplayHandle,
    {
        WgpuCanvas {
            state:            State::new(window_handle, width, height),
            example_pipeline: ExamplePipeline,
        }
    }

    #[inline]
    pub(crate) const fn state_ref(&self) -> &State {
        &self.state
    }

    #[inline]
    pub(crate) fn state_mut(&mut self) -> &mut State {
        &mut self.state
    }
}

impl Canvas for WgpuCanvas {
    fn resize(&mut self, width: u32, height: u32) {
        todo!()
    }

    fn present(&mut self) -> Result<(), PresentError> {
        let output = self.state.surface.get_current_texture().unwrap();
        let view = output.texture.create_view(&wgpu::TextureViewDescriptor::default());

        let mut encoder = self
            .state
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("Render Encoder"),
            });

        self.example_pipeline.render(&mut encoder, &view);

        self.state.queue.submit(std::iter::once(encoder.finish()));
        output.present();

        Ok(())
    }
}
