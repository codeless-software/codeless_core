//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

// TODO: Add support for de-duplicating entry points
// TODO: Add recursive import support

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::{
    fs, io,
    path::{Path, PathBuf},
};

use itertools::Itertools;
use log::warn;
use once_cell::sync::Lazy;
use regex::{Captures, Regex};

//--------------------------------------------------------------------------------------------------
//-- STATICS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// Regular expression for matching shader stages (vertex, fragment, or compute) and their entry points.
/// The entry point is optional.
static REGEX_SHADER_STAGE: Lazy<Regex> =
    Lazy::new(|| Regex::new(r#"@(?P<stage>vertex|fragment|compute).*(\nfn (?P<entry>\w+))?"#).unwrap());

/// Regular expression for matching shader include statements (e.g., `#import "some_file.wgsl"`).
static REGEX_SHADER_INCLUDE: Lazy<Regex> =
    Lazy::new(|| Regex::new(r#"#import ?"(?P<import_path>(\w|.|/)+)""#).unwrap());

//--------------------------------------------------------------------------------------------------
//-- ENUMS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// Possible errors when loading a shader.
#[derive(thiserror::Error, Debug)]
pub enum ShaderLoadError {
    #[error("Shader file '{0}' not found")]
    FileNotFound(String),

    #[error("Invalid UTF-8 in shader file '{0}'")]
    InvalidUtf8(String),

    #[error("Failed to process import: {0}")]
    InvalidImport(#[from] io::Error),

    #[error("Shader load error: {0}")]
    Other(io::Error),
}

//--------------------------------------------------------------------------------------------------

/// Possible errors when building a shader
#[derive(thiserror::Error, Debug)]
pub enum ShaderBuildError {
    #[error("Duplicate shader stages found")]
    DuplicateShaderStage,
}

//--------------------------------------------------------------------------------------------------

/// The three possible stages for a shader.
#[derive(Debug, Clone, Eq, PartialEq)]
pub(self) enum ShaderStage {
    Vertex(String),
    Fragment(String),
    Compute(String),
}

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// A compiled shader.
#[derive(Debug)]
pub struct Shader {
    vertex:   Option<String>,
    fragment: Option<String>,
    compute:  Option<String>,
    module:   wgpu::ShaderModule,
}

impl Shader {
    pub fn vertex_state(&self) -> Option<wgpu::VertexState> {
        let entry = self.vertex.as_ref()?.as_str();

        Some(wgpu::VertexState {
            module:      &self.module,
            entry_point: entry,
            buffers:     &[],
        })
    }

    pub fn fragment_state(&self) -> Option<wgpu::FragmentState> {
        let entry = self.fragment.as_ref()?.as_str();

        Some(wgpu::FragmentState {
            module:      &self.module,
            entry_point: entry,
            targets:     &[],
        })
    }
}

//--------------------------------------------------------------------------------------------------

/// A builder for a shader.
#[derive(Default, Debug)]
pub struct ShaderBuilder {
    sources: Vec<String>,
    imports: Vec<String>,
}

impl ShaderBuilder {
    /// Creates a new shader builder.
    pub fn new() -> Self {
        Self::default()
    }

    /// Adds a shader file to the builder.
    ///
    /// # Arguments
    ///
    /// * `path`: Path to the shader
    pub fn add_file(mut self, path: &Path) -> Result<Self, ShaderLoadError> {
        let shader_source = fs::read_to_string(path).map_err(|e| {
            let path_string = path.to_string_lossy().to_string();
            match e.kind() {
                io::ErrorKind::NotFound => ShaderLoadError::FileNotFound(path_string),
                io::ErrorKind::InvalidData => ShaderLoadError::InvalidUtf8(path_string),
                _ => ShaderLoadError::Other(e),
            }
        })?;

        let mut imports = load_imports(path.parent().unwrap(), &shader_source)?;

        self.sources.push(shader_source);
        self.imports.append(&mut imports);
        Ok(self)
    }

    /// Builds a WGSL shader from the sources and imports added to the builder.
    pub fn build(mut self, device: &wgpu::Device) -> Result<Shader, ShaderBuildError> {
        // Remove all duplicate imports.
        self.imports.dedup();

        // Combine all imports and sources into a single shader file, then strip out all remaining
        // #inport directives.
        let combined_shader = self.imports.iter().chain(self.sources.iter()).join("\n");
        let combined_shader = REGEX_SHADER_INCLUDE.replace_all(&combined_shader, "");

        let mut vertex = None;
        let mut fragment = None;
        let mut compute = None;

        // Find all shader stages and entry points
        for stage in find_shader_stage_and_entry_points(&combined_shader) {
            match stage {
                ShaderStage::Vertex(e) => {
                    validate_duplicate(&vertex)?;
                    vertex = Some(e);
                }
                ShaderStage::Fragment(e) => {
                    validate_duplicate(&fragment)?;
                    fragment = Some(e);
                }
                ShaderStage::Compute(e) => {
                    validate_duplicate(&compute)?;
                    compute = Some(e);
                }
            }
        }

        // Create the WGSL shader module
        let module = device.create_shader_module(wgpu::ShaderModuleDescriptor {
            label:  None,
            source: wgpu::ShaderSource::Wgsl(combined_shader),
        });

        Ok(Shader {
            vertex,
            fragment,
            compute,
            module,
        })
    }
}

//--------------------------------------------------------------------------------------------------
//-- PRIVATE FUNCTIONS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// Finds and returns all shader stages and entry points in the given WGSL shader.
///
/// # Arguments
///
/// * `shader_source`: The WGSL shader to process.
#[inline]
fn find_shader_stage_and_entry_points(shader_source: &str) -> Vec<ShaderStage> {
    let process_entry = |s: &str, c: &Captures| -> Option<String> {
        // Extract the entry point, returning it if it exists.
        if let Some(entry) = c.name("entry").map(|m| m.as_str()) {
            return Some(entry.to_owned());
        }

        // If no entry point is found, get the line number for the corresponding shader stage, then
        // then print a warning including the stage and line number.
        let match_start_index = c.get(0).unwrap().start();
        let line_number = shader_source
            .lines()
            .take_while(|line| match_start_index >= line.as_ptr() as usize - shader_source.as_ptr() as usize)
            .count();

        warn!("Shader stage '{}' is missing an entry point: Line {}", s, line_number);
        None
    };

    // Find all shader stages and entry points and return them.
    REGEX_SHADER_STAGE
        .captures_iter(shader_source)
        .filter_map(|c| {
            let stage = c.name("stage")?.as_str();

            match stage {
                "vertex" => Some(ShaderStage::Vertex(process_entry(stage, &c)?)),
                "fragment" => Some(ShaderStage::Fragment(process_entry(stage, &c)?)),
                "compute" => Some(ShaderStage::Compute(process_entry(stage, &c)?)),
                _ => {
                    warn!("Unknown shader stage {}", stage);
                    None
                }
            }
        })
        .collect()
}

//--------------------------------------------------------------------------------------------------

#[inline]
fn load_imports(base_path: &Path, source: &str) -> Result<Vec<String>, ShaderLoadError> {
    REGEX_SHADER_INCLUDE
        .captures_iter(source)
        .filter_map(|c| Some(base_path.join(PathBuf::from(c.name("import_path")?.as_str()))))
        .map(|p| match fs::read_to_string(p) {
            Ok(s) => Ok(s),
            Err(e) => Err(ShaderLoadError::InvalidImport(e)),
        })
        .collect()
}

//--------------------------------------------------------------------------------------------------

#[inline]
const fn validate_duplicate(stage: &Option<String>) -> Result<(), ShaderBuildError> {
    if stage.is_none() {
        return Ok(());
    }

    Err(ShaderBuildError::DuplicateShaderStage)
}

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[cfg(test)]
#[allow(clippy::unwrap_used)]
mod tests {
    use std::{error::Error, path::PathBuf};

    use applab_util::DebugLogger;
    use wgpu::DeviceDescriptor;

    use crate::shader::ShaderBuilder;

    //----------------------------------------------------------------------------------------------

    async fn init_wgpu() -> (wgpu::Instance, wgpu::Adapter, wgpu::Device, wgpu::Queue) {
        let instance = wgpu::Instance::new(wgpu::InstanceDescriptor::default());

        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference:       wgpu::PowerPreference::default(),
                force_fallback_adapter: false,
                compatible_surface:     None,
            })
            .await
            .unwrap();

        let (device, queue) = adapter
            .request_device(&DeviceDescriptor::default(), None)
            .await
            .unwrap();

        (instance, adapter, device, queue)
    }

    //----------------------------------------------------------------------------------------------

    #[test]
    #[cfg(not(miri))]
    fn add_file() -> Result<(), Box<dyn Error>> {
        let (_, _, device, _) = pollster::block_on(init_wgpu());
        let shader = ShaderBuilder::new()
            .add_file(&PathBuf::from("../../../resources/test/shader.wgsl"))?
            .build(&device)?;

        assert_eq!(shader.vertex.unwrap(), "vs_main");
        assert_eq!(shader.fragment.unwrap(), "fs_main");
        assert!(shader.compute.is_none());

        Ok(())
    }

    //----------------------------------------------------------------------------------------------

    // NOTE: Disabled until include handling is added
    #[test]
    #[cfg(not(miri))]
    fn add_file_multiple() -> Result<(), Box<dyn Error>> {
        let (_, _, device, _) = pollster::block_on(init_wgpu());
        let shader = ShaderBuilder::new()
            .add_file(&PathBuf::from("../../../resources/test/shader_split.vert.wgsl"))?
            .add_file(&PathBuf::from("../../../resources/test/shader_split.frag.wgsl"))?
            .add_file(&PathBuf::from("../../../resources/test/shader_split.comp.wgsl"))?
            .build(&device)?;

        assert_eq!(shader.vertex.unwrap(), "vs_main");
        assert_eq!(shader.fragment.unwrap(), "fs_main");
        assert_eq!(shader.compute.unwrap(), "cs_main");

        Ok(())
    }

    //----------------------------------------------------------------------------------------------

    #[test]
    #[cfg(not(miri))]
    fn add_file_missing_entry() -> Result<(), Box<dyn Error>> {
        let logger = DebugLogger::new();

        let (_, _, device, _) = pollster::block_on(init_wgpu());
        ShaderBuilder::new()
            .add_file(&PathBuf::from("../../../resources/test/missing_entry.wgsl"))?
            .build(&device)?;

        let log_string = logger.collect("applab_draw_wgpu");
        assert!(log_string[0].contains("Shader stage 'vertex' is missing an entry point: Line 1"));

        Ok(())
    }

    //----------------------------------------------------------------------------------------------

    #[test]
    #[should_panic(expected = "Shader file 'notashader.wgsl' not found")]
    fn add_file_not_found() {
        if let Err(e) = ShaderBuilder::new().add_file(&PathBuf::from("notashader.wgsl")) {
            panic!("{}", e);
        }
    }

    //----------------------------------------------------------------------------------------------

    #[test]
    #[should_panic(expected = "Invalid UTF-8 in shader file '../../../resources/test/invalid.wgsl'")]
    fn add_file_invalid_utf8() {
        if let Err(e) = ShaderBuilder::new().add_file(&PathBuf::from("../../../resources/test/invalid.wgsl")) {
            panic!("{}", e);
        }
    }

    //----------------------------------------------------------------------------------------------

    #[test]
    #[cfg_attr(
        not(miri),
        should_panic(expected = "Failed to process import: No such file or directory (os error 2)")
    )]
    #[cfg_attr(
        miri,
        should_panic(expected = "Failed to process import: entity not found (os error 2)")
    )] // MIRI throws a different error for whatever reason...
    fn add_file_invalid_import() {
        if let Err(e) = ShaderBuilder::new().add_file(&PathBuf::from("../../../resources/test/invalid_import.wgsl")) {
            panic!("{}", e);
        }
    }
}
