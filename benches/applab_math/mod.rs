//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

mod matrix_multiply;

use std::time;

use criterion::*;

fn bench_matrix_multiply(c: &mut Criterion) {
    let mut group = c.benchmark_group("matrix_multiply");
    group.warm_up_time(time::Duration::from_millis(500));
    group.measurement_time(time::Duration::from_secs(4));

    group.bench_function("matrix_multiply", |b| {
        let mut bench = matrix_multiply::Benchmark::new();
        b.iter(move || bench.run());
    });

    group.finish();
}

criterion_group!(benches, bench_matrix_multiply);
criterion_main!(benches);
