//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

use applab_math::matrix::{Mat, Mat4, Mat4f};

pub struct Benchmark {
    m: Vec<Mat<f32, 6, 6>>,
}

impl Benchmark {
    pub fn new() -> Self {
        let mut m = Vec::with_capacity(400);

        for _ in 0..400 {
            m.push(Mat::random());
        }
        Self { m }
    }

    pub fn run(&mut self) {
        for m in self.m.chunks(2) {
            let m1 = &m[0];
            let m2 = &m[1];
            let res = m1 * m2;
        }
    }
}
