//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

mod simple_insert;
mod simple_iter;

use std::time;

use criterion::*;

fn bench_simple_insert(c: &mut Criterion) {
    let mut group = c.benchmark_group("simple_insert");
    group.warm_up_time(time::Duration::from_millis(500));
    group.measurement_time(time::Duration::from_secs(4));

    group.bench_function("simple_insert", |b| {
        let mut bench = simple_insert::Benchmark::new();
        b.iter(move || bench.run());
    });

    group.finish();
}

fn bench_simple_iter(c: &mut Criterion) {
    let mut group = c.benchmark_group("simple_iter");
    group.warm_up_time(time::Duration::from_millis(500));
    group.measurement_time(time::Duration::from_secs(4));

    group.bench_function("simple_iter", |b| {
        let mut bench = simple_iter::Benchmark::new();
        b.iter(move || bench.run());
    });

    group.finish();
}

criterion_group!(benches, bench_simple_insert, bench_simple_iter);
criterion_main!(benches);
