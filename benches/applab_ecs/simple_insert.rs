//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

use applab_ecs::{macros::Component, World};
use glam::{Mat4, Vec3};

#[derive(Component)]
struct Transform(Mat4);

#[derive(Component)]
struct Position(Vec3);

#[derive(Component)]
struct Rotation(Vec3);

#[derive(Component)]
struct Velocity(Vec3);

pub struct Benchmark;

impl Benchmark {
    pub const fn new() -> Self {
        Self
    }

    pub fn run(&mut self) {
        let mut world = World::default();

        world.spawn_batched((0..10_000).map(|_| {
            (
                Transform(Mat4::from_scale(Vec3::ONE)),
                Position(Vec3::X),
                Rotation(Vec3::X),
                Velocity(Vec3::X),
            )
        }));
    }
}
