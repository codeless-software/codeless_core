//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::any::type_name;

use crate::App;

//--------------------------------------------------------------------------------------------------
//-- TRAITS ----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub trait Plugin {
    fn register(&self, app: &mut App);
    fn name(&self) -> &str {
        type_name::<Self>()
    }
}

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use applab_ecs::{
        event::EventWriter,
        macros::Resource,
        resource::ResMut,
        system::{CoreStage, IntoSystem},
    };

    use crate::{plugin::Plugin, App, AppExit};

    #[derive(Debug, Resource)]
    struct TestResource(bool);

    struct TestPlugin;

    impl Plugin for TestPlugin {
        fn register(&self, app: &mut App) {
            app.add_resource(TestResource(false))
                .add_system_to_stage(CoreStage::Update, test_system.system());
        }
    }

    fn test_system(mut resource: ResMut<TestResource>, mut exit: EventWriter<AppExit>) {
        resource.0 = true;
        exit.send(AppExit);
    }

    #[test]
    fn add_plugin() {
        let mut app = App::default();
        app.add_plugin(TestPlugin);

        assert!(app.world.get_resource::<TestResource>().is_some());
    }
}
