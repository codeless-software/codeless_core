//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::{
    any::type_name,
    error::Error,
    fmt::Debug,
    mem,
    sync::mpsc::{channel, Receiver, Sender},
};

use applab_ecs::{
    event::{Event, EventReader, Events},
    macros::{Event, Resource},
    resource::{LocalRes, Resource},
    system::{label::StageLabel, CoreStage, IntoSystem, Scheduler, SysRunner},
    World,
};
use log::{debug, error, info};

use crate::plugin::Plugin;

//--------------------------------------------------------------------------------------------------
//-- TYPES -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub type RunnerResult = Result<(), Box<dyn Error>>;

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[derive(Debug, Event)]
pub struct AppExit;

//--------------------------------------------------------------------------------------------------

#[derive(Default, Debug, Resource)]
struct AppExitSignal {
    sender: Option<Sender<()>>,
}

impl AppExitSignal {
    fn new(sender: Sender<()>) -> Self {
        Self { sender: Some(sender) }
    }
}

//--------------------------------------------------------------------------------------------------

pub struct Runner {
    pub name:   &'static str,
    pub run_fn: Box<dyn Fn(App) -> RunnerResult>,
}

impl Runner {
    pub fn new<T>(run_fn: T) -> Self
    where
        T: Fn(App) -> RunnerResult + 'static,
    {
        Self {
            name:   type_name::<T>(),
            run_fn: Box::new(run_fn),
        }
    }
}

//--------------------------------------------------------------------------------------------------

pub struct App {
    pub world:     World,
    pub scheduler: Scheduler,
    pub runner:    Runner,

    app_exit_sender:   Sender<()>,
    app_exit_receiver: Receiver<()>,
}

impl Default for App {
    fn default() -> Self {
        let mut app = Self::empty();
        let exit_sender = app.app_exit_sender.clone();

        app.add_event::<AppExit>().add_system_to_stage(
            CoreStage::Last,
            Self::app_exit_system
                .builder()
                .with_resource(AppExitSignal::new(exit_sender))
                .build(&Self::app_exit_system),
        );

        app
    }
}

impl App {
    pub fn empty() -> Self {
        let (sender, receiver) = channel();

        Self {
            world:     World::default(),
            scheduler: Scheduler::default(),
            runner:    Runner::new(default_run_fn),

            app_exit_receiver: receiver,
            app_exit_sender:   sender,
        }
    }

    /// Initialise a resource using the default values.
    ///
    /// # Examples
    /// ```
    /// # use applab_core::App;
    /// # use applab_ecs::macros::Resource;
    /// #[derive(Default, Debug, Resource)]
    /// struct MyResource {
    ///     image_path: String,
    /// };
    ///
    /// # impl MyResource {
    /// #     pub fn new(path: &str) -> Self {
    /// #         MyResource { image_path: path.to_owned() }
    /// #     }
    /// # }
    /// App::default().init_resource::<MyResource>();
    /// ```
    pub fn init_resource<T>(&mut self) -> &mut Self
    where
        T: Resource + Default,
    {
        self.world.init_resource::<T>();
        self
    }

    /// Adds a new resource.
    ///
    /// # Arguments
    ///
    /// * `resource`: Resource to add to the `World`.
    ///
    /// # Panics
    /// Panics if the resource already exists.
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_core::App;
    /// # use applab_ecs::macros::Resource;
    /// #[derive(Debug, Resource)]
    /// struct MyResource {
    ///     image_path: String,
    /// };
    ///
    /// # impl MyResource {
    /// #     pub fn new(path: &str) -> Self {
    /// #         MyResource { image_path: path.to_owned() }
    /// #     }
    /// # }
    /// App::default().add_resource(MyResource::new("path/to/image.png"));
    /// ```
    pub fn add_resource(&mut self, resource: impl Resource) -> &mut Self {
        self.world.add_resource(resource);
        self
    }

    /// Register an event with the App.
    ///
    /// This is done by adding a [`Resource`] of the type [`Events::<T>`],
    /// and inserting an [`update_system`](Events::update_system) into [`CoreStage::First`].
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_core::App;
    /// # use applab_ecs::macros::Event;
    ///
    /// #[derive(Debug, Event)]
    /// enum MyEvent {
    ///     Moved,
    ///     Resized,
    ///     Closed,
    /// }
    ///
    /// # let mut app = App::default();
    /// app.add_event::<MyEvent>();
    /// ```
    pub fn add_event<T: Event>(&mut self) -> &mut Self {
        if !self.world.has_resource::<Events<T>>() {
            self.init_resource::<Events<T>>()
                .add_system_to_stage(CoreStage::First, Events::<T>::update_system.system());
        }

        self
    }

    /// Adds a new stage to the last position in the schedule.
    ///
    /// # Panics
    /// Panics if the stage already exits.
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_core::App;
    /// App::default().add_stage("my_stage");
    /// ```
    pub fn add_stage(&mut self, label: impl StageLabel) -> &mut Self {
        self.scheduler.add_stage(label);
        self
    }

    /// Adds a new stage immediately before the `target` stage.
    ///
    /// # Panics
    /// Panics if the `target` stage doesn't exist, or the stage defined by `label` already exists.
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_core::App;
    /// App::default()
    ///     .add_stage("target_stage")
    ///     .add_stage_before("my_stage", "target_stage");
    /// ```
    pub fn add_stage_before(&mut self, label: impl StageLabel, target: impl StageLabel) -> &mut Self {
        self.scheduler.add_stage_before(label, target);
        self
    }

    /// Adds a new stage immediately after the `target` stage.
    ///
    /// # Panics
    /// Panics if the `target` stage doesn't exist, or the stage defined by `label` already exists.
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_core::App;
    /// App::default()
    ///     .add_stage("target_stage")
    ///     .add_stage_after("my_stage", "target_stage");
    /// ```
    pub fn add_stage_after(&mut self, label: impl StageLabel, target: impl StageLabel) -> &mut Self {
        self.scheduler.add_stage_after(label, target);
        self
    }

    /// Adds a new system to [`CoreStage::Startup`].
    pub fn add_startup_system(&mut self, system: impl SysRunner + 'static) -> &mut Self {
        self.scheduler.add_startup_system(system);
        self
    }

    /// Adds a system to the `target` stage.
    ///
    /// # Panics
    /// Panics if the `target` stage doesn't exist.
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_core::App;
    /// # use applab_ecs::macros::Component;
    /// # use applab_ecs::query::Query;
    /// # use applab_ecs::system::IntoSystem;
    /// #[derive(Component)]
    /// # struct MyComponent;
    /// # fn my_system(_: Query<&MyComponent>) {}
    /// App::default()
    ///     .add_stage("target_stage")
    ///     .add_system_to_stage("target_stage", my_system.system());
    pub fn add_system_to_stage(&mut self, target: impl StageLabel, system: impl SysRunner + 'static) -> &mut Self {
        self.scheduler.add_system_to_stage(target, system);
        self
    }

    /// Adds a new plugin, registering its stages, systems, and resources.
    pub fn add_plugin(&mut self, plugin: impl Plugin) -> &mut Self {
        debug!("Added plugin: {}", plugin.name());
        plugin.register(self);
        self
    }

    pub fn set_runner(&mut self, runner: impl Fn(Self) -> RunnerResult + 'static) -> &mut Self {
        self.runner = Runner::new(runner);
        debug!("Setting {} as runner", self.runner.name);
        self
    }

    /// Run the app, looping until execution is finished.
    pub fn run(&mut self) -> RunnerResult {
        let mut app = mem::take(self);
        let runner = mem::replace(&mut app.runner, Runner::new(default_run_fn));

        (runner.run_fn)(app)
    }

    pub fn exit_requested(&self) -> bool {
        self.app_exit_receiver.try_recv().is_ok()
    }

    fn app_exit_system(app_exit: EventReader<AppExit>, exit_signal: LocalRes<AppExitSignal>) {
        if !app_exit.is_empty() {
            info!("Received app exit event");

            if let Err(e) = exit_signal.sender.as_ref().map_or_else(
                || Err("Sender is None".to_owned()),
                |sender| match sender.send(()) {
                    Ok(_) => Ok(()),
                    Err(e) => Err(format!("{e}")),
                },
            ) {
                error!("Failed to send app exit signal: {e}");
            }
        }
    }
}

//--------------------------------------------------------------------------------------------------
//-- PRIVATE FUNCTIONS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

fn default_run_fn(mut app: App) -> RunnerResult {
    app.scheduler.execute_startup(&mut app.world);

    while !app.exit_requested() {
        app.scheduler.execute(&mut app.world);
    }

    Ok(())
}

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[cfg(test)]
#[allow(clippy::unwrap_used)]
mod tests {
    use applab_ecs::macros::Resource;

    use crate::App;

    #[derive(Debug, Resource)]
    struct TestResource(u8);

    impl Default for TestResource {
        fn default() -> Self {
            Self(42)
        }
    }

    #[test]
    fn init_resource() {
        let mut app = App::default();
        app.init_resource::<TestResource>();

        let res = app.world.get_resource::<TestResource>();
        assert!(res.is_some());
        assert!(res.unwrap().0 == 42);
    }

    #[test]
    fn add_resource() {
        let mut app = App::default();
        app.add_resource(TestResource(0));
        assert!(app.world.has_resource::<TestResource>());
    }

    #[test]
    fn add_stage() {
        let mut app = App::default();
        app.add_stage("MyStage");
        assert!(app.scheduler.has_stage(&"MyStage"));
    }
}
