//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//! This is a basic example on how to use `applab_window` and `applab_winit`.
//!
//! It creates a single window which can moved, resized, and closed.

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::{error::Error, fmt::Debug};

use applab_core::App;
use applab_draw::Canvas;
use applab_draw_wgpu::WgpuCanvas;
use applab_ecs::{
    event::EventReader,
    macros::Resource,
    resource::{LocalResMut, Res, ResMut},
    system::{
        command::{CommandBuffer, Commands},
        CoreStage, IntoSystem,
    },
};
use applab_window::{
    event::{WindowCloseRequested, WindowCreated, WindowRedraw},
    Window, WindowContainer,
};
use applab_window_winit::plugin::WinitWindowPlugin;

#[derive(Debug, Resource)]
struct DrawWrapper {
    draw: WgpuCanvas,
}

//--------------------------------------------------------------------------------------------------
//-- FUNCTIONS -------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[derive(Debug, Resource, Default)]
struct Done(bool);

fn init_draw(
    mut commands: Commands,
    mut windows: ResMut<WindowContainer>,
    mut done: LocalResMut<Done>,
    mut event: EventReader<WindowCreated>,
) {
    if done.0 {
        return;
    }

    for id in event.iter() {
        let window = windows.get_mut(&id.id).unwrap();
        let draw = applab_draw_wgpu::WgpuCanvas::new(window, window.framebuffer_width(), window.framebuffer_height());

        commands.add_resource(DrawWrapper { draw });
        window.request_redraw();
        done.0 = true;
    }
}

fn render(mut events: EventReader<WindowRedraw>, mut draw: ResMut<DrawWrapper>, mut windows: ResMut<WindowContainer>) {
    for event in events.iter() {
        draw.draw.present();
    }
}

fn close_requested(mut events: EventReader<WindowCloseRequested>, mut windows: ResMut<WindowContainer>) {
    for event in events.iter() {
        println!("Close requested: {}", event.id);
        windows.get_mut(&event.id).map(Window::close);
    }
}

//--------------------------------------------------------------------------------------------------

fn main() -> Result<(), Box<dyn Error>> {
    env_logger::init();

    App::default()
        .add_startup_system(init_draw.system())
        .add_plugin(
            WinitWindowPlugin::default()
                .with_primary_window()
                .exit_on_primary_window_closed(),
        )
        .add_system_to_stage(CoreStage::First, init_draw.system())
        .add_system_to_stage(CoreStage::Update, render.system())
        .add_system_to_stage(CoreStage::Last, close_requested.system())
        .run()
}
