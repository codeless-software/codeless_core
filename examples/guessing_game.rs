use std::{
    cmp::Ordering::{Equal, Greater, Less},
    error::Error,
    io,
    io::Write,
    str::FromStr,
};

use applab_core::{App, AppExit};
use applab_ecs::{
    event::EventWriter,
    macros::Resource,
    resource::{Res, ResMut},
    system::{CoreStage, IntoSystem},
};

#[derive(Debug, Resource)]
struct Answer(u8);

#[derive(Debug, Resource)]
struct Guess(Option<u8>, u8);

fn startup(mut answer: ResMut<Answer>) {
    *answer = Answer(rand::random());

    println!("Welcome to the guessing game!");
    println!("You must guess a number between 0 and 255.");
}

fn get_input(mut guess: ResMut<Guess>) {
    let mut string = String::new();

    print!("\nEnter a guess:\t");
    io::stdout().flush().expect("Failed to flush stdout");
    io::stdin().read_line(&mut string).expect("Failed to get input");

    let guess_val = u8::from_str(string.trim()).ok();

    guess.0 = guess_val;
}

fn check_answer(mut guess: ResMut<Guess>, answer: Res<Answer>, mut exit: EventWriter<AppExit>) {
    let answer = answer.0;

    guess.0.map_or_else(
        || println!("Invalid input!"),
        |val| {
            guess.1 += 1;

            match val.cmp(&answer) {
                Greater => println!("Lower"),
                Less => println!("Higher"),
                Equal => {
                    println!("You guessed it in only {} tries!", guess.1);
                    exit.send(AppExit);
                }
            };
        },
    );
}

fn main() -> Result<(), Box<dyn Error>> {
    App::default()
        .add_resource(Answer(0))
        .add_resource(Guess(None, 0))
        .add_startup_system(startup.system())
        .add_system_to_stage(CoreStage::PreUpdate, get_input.system())
        .add_system_to_stage(CoreStage::Update, check_answer.system())
        .run()?;

    println!("\nThanks for playing!");
    Ok(())
}
