//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//! This is a basic example on how to use `applab_window` and `applab_winit`.
//!
//! It creates a single window which can moved, resized, and closed.

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::error::Error;

use applab_core::App;
use applab_ecs::{
    event::EventReader,
    resource::ResMut,
    system::{CoreStage, IntoSystem},
};
use applab_window::{event::WindowCloseRequested, Window, WindowContainer};
use applab_window_winit::plugin::WinitWindowPlugin;

//--------------------------------------------------------------------------------------------------
//-- FUNCTIONS -------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

fn close_requested(mut events: EventReader<WindowCloseRequested>, mut windows: ResMut<WindowContainer>) {
    for event in events.iter() {
        println!("Close requested: {}", event.id);
        windows.get_mut(&event.id).map(Window::close);
    }
}

//--------------------------------------------------------------------------------------------------

fn main() -> Result<(), Box<dyn Error>> {
    env_logger::init();

    App::default()
        .add_plugin(
            WinitWindowPlugin::default()
                .with_primary_window()
                .exit_on_primary_window_closed(),
        )
        .add_system_to_stage(CoreStage::Last, close_requested.system())
        .run()
}
