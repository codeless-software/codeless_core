//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use num_traits::Float;

use crate::{
    bezier::{Bezier2, QuadBezier2},
    vector::Vec2,
};

//--------------------------------------------------------------------------------------------------
//-- TYPES -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub type CubicBezier2f = CubicBezier2<f32>;
pub type CubicBezier2d = CubicBezier2<f64>;

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub struct CubicBezier2<T: Float> {
    pub point_a: Vec2<T>,
    pub point_b: Vec2<T>,
    pub point_c: Vec2<T>,
    pub point_d: Vec2<T>,
}

impl<T: Float> CubicBezier2<T> {
    pub const fn new(point_a: Vec2<T>, point_b: Vec2<T>, point_c: Vec2<T>, point_d: Vec2<T>) -> Self {
        Self {
            point_a,
            point_b,
            point_c,
            point_d,
        }
    }
}

//--------------------------------------------------------------------------------------------------
//-- MACROS ----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

macro_rules! impl_bezier {
    ($i: ident) => {
        #[allow(clippy::default_numeric_fallback)]
        impl Bezier2<$i> for CubicBezier2<$i> {
            type Output = Self;
            type Derivative = QuadBezier2<$i>;

            fn length(&self) -> $i {
                let a = self.point_a;
                let b = self.point_b;
                let c = self.point_c;
                let d = self.point_d;

                let chord = (d - a).magnitude();
                let cont_a = (a - b).magnitude();
                let cont_b = (b - c).magnitude();
                let cont_c = (c - d).magnitude();

                let cont_sum = cont_a + cont_b + cont_c;
                let aprox_len = (cont_sum + chord) / 2.0;

                // Use cubic Bezier curve length formula to get more accurate result
                let mut len = 0.0;
                let mut t = 0.0;
                let steps = 1000;
                for _ in 0..steps {
                    let t_next = (t + 1.0) / steps as $i;
                    len += (self.point(t) - self.point(t_next)).magnitude();
                    t = t_next;
                }

                len
            }

            #[inline]
            fn point(&self, t: $i) -> Vec2<$i> {
                let t2 = t * t;
                let one_minus_t = 1.0 - t;
                let one_minus_t2 = one_minus_t * one_minus_t;

                one_minus_t2 * one_minus_t * self.point_a
                + 3.0 * t * one_minus_t2 * self.point_b
                + 3.0 * t2 * one_minus_t * self.point_c
                + t2 * t * self.point_d
            }

            fn split(&self, t: $i) -> [Self::Output; 2] {
                let p0 = self.point_a;
                let p1 = self.point_b;
                let p2 = self.point_c;
                let p3 = self.point_d;

                let t2 = t * t;
                let t3 = t2 * t;
                let one_minus_t = 1.0 - t;
                let one_minus_t2 = one_minus_t * one_minus_t;
                let one_minus_t3 = one_minus_t2 * one_minus_t;

                let left = {
                    let f0 = p0;
                    let f1 = (p0 + p1) * t;
                    let f2 = (p0 + (2.0 * p1) + p2) * t2;
                    let f3 = (p0 + (3.0 * p1) + (3.0 * p2) + p3) * t3;

                    Self::new(f0, f1, f2, f3)
                };

                let right = {
                    let f0 = p3;
                    let f1 = (p3 + p2) * one_minus_t;
                    let f2 = (p3 + (2.0 * p2) + p1) * one_minus_t2;
                    let f3 = (p3 + (3.0 * p2) + (3.0 * p1) + p0) * one_minus_t3;

                    Self::new(f0, f1, f2, f3)
                };

                [left, right]
            }

            fn derivative(&self) -> Self::Derivative {
                todo!()
            }
        }
    }
}

impl_bezier!(f32);
impl_bezier!(f64);

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use crate::{
        bezier::{Bezier2, CubicBezier2f},
        vector::Vec2f,
    };

    #[test]
    fn point() {
        let cube = CubicBezier2f::new(
            Vec2f::new(-2.0, 0.0),
            Vec2f::new(-1.0, 1.0),
            Vec2f::new(1.0, -1.0),
            Vec2f::new(2.0, 0.0),
        );

        assert_eq!(cube.point(0.0), Vec2f::new(-2.0, 0.0));
        assert_eq!(cube.point(0.25), Vec2f::new(-1.09375, 0.28125));
        assert_eq!(cube.point(0.5), Vec2f::new(0.0, 0.0));
        assert_eq!(cube.point(0.75), Vec2f::new(1.09375, -0.28125));
        assert_eq!(cube.point(1.0), Vec2f::new(2.0, 0.0));
    }
}
