//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use num_traits::Float;

use crate::{
    bezier::{Bezier2, CubicBezier2, LinearBezier2},
    vector::Vec2,
};

//--------------------------------------------------------------------------------------------------
//-- TYPES -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub type QuadBezier2f = QuadBezier2<f32>;
pub type QuadBezier2d = QuadBezier2<f64>;

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[derive(Debug)]
pub struct QuadBezier2<T: Float> {
    pub point_a: Vec2<T>,
    pub point_b: Vec2<T>,
    pub point_c: Vec2<T>,
}

impl<T: Float> QuadBezier2<T> {
    pub const fn new(point_a: Vec2<T>, point_b: Vec2<T>, point_c: Vec2<T>) -> Self {
        Self {
            point_a,
            point_b,
            point_c,
        }
    }
}

//--------------------------------------------------------------------------------------------------
//-- MACROS ----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

macro_rules! impl_bezier {
    ($i: ident) => {
        #[allow(clippy::default_numeric_fallback)]
        impl QuadBezier2<$i> {
            /// Returns a cubic representation of this curve.
            pub fn cubic(&self) -> CubicBezier2<$i> {
                CubicBezier2::new(
                    self.point_a,
                    self.point_a + 2.0 / 3.0 * (self.point_b - self.point_a),
                    self.point_c + 2.0 / 3.0 * (self.point_b - self.point_c),
                    self.point_c
                )
            }
        }

        #[allow(clippy::default_numeric_fallback)]
        impl Bezier2<$i> for QuadBezier2<$i> {
            type Output = Self;
            type Derivative = LinearBezier2<$i>;

            fn length(&self) -> $i {
                todo!()
            }

            #[inline]
            fn point(&self, t: $i) -> Vec2<$i> {
                let one_minus_t = 1.0 - t;
                let one_minus_t2 = one_minus_t * one_minus_t;

                one_minus_t2 * self.point_a
                + 2.0 * one_minus_t * t * self.point_b
                + t * t * self.point_c
            }

            fn split(&self, t: $i) -> [Self::Output; 2] {
                todo!()
            }

            fn derivative(&self) -> Self::Derivative {
                let p1 = self.point_b - self.point_a;
                let p2 = self.point_c - self.point_b;

                Self::Derivative::new(p1 * 2.0, p2 * 2.0)
            }
        }
    }
}

impl_bezier!(f32);
impl_bezier!(f64);

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use crate::{
        bezier::{Bezier2, QuadBezier2f},
        vector::Vec2f,
    };

    #[test]
    fn point() {
        let quad = QuadBezier2f::new(Vec2f::new(-1.0, 0.0), Vec2f::new(0.0, 1.0), Vec2f::new(1.0, 0.0));

        assert_eq!(quad.point(0.0), Vec2f::new(-1.0, 0.0));
        assert_eq!(quad.point(0.25), Vec2f::new(-0.5, 0.375));
        assert_eq!(quad.point(0.5), Vec2f::new(0.0, 0.5));
        assert_eq!(quad.point(0.75), Vec2f::new(0.5, 0.375));
        assert_eq!(quad.point(1.0), Vec2f::new(1.0, 0.0));
    }

    #[test]
    // TODO: Update this test to actually test the output.
    fn derivative() {
        let quad = QuadBezier2f::new(Vec2f::new(-1.0, 0.0), Vec2f::new(0.0, 1.0), Vec2f::new(1.0, 0.0));
        dbg!(quad.derivative());
    }
}
