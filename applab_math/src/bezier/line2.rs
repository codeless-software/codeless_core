//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use num_traits::Float;

use crate::{bezier::Bezier2, vector::Vec2};

//--------------------------------------------------------------------------------------------------
//-- TYPES -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub type LinearBezier2f = LinearBezier2<f32>;
pub type LinearBezier2d = LinearBezier2<f64>;

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[derive(Debug)]
pub struct LinearBezier2<T: Float> {
    pub point_a: Vec2<T>,
    pub point_b: Vec2<T>,
}

impl<T: Float> LinearBezier2<T> {
    pub const fn new(point_a: Vec2<T>, point_b: Vec2<T>) -> Self {
        Self { point_a, point_b }
    }
}

//--------------------------------------------------------------------------------------------------
//-- MACROS ----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

macro_rules! impl_bezier {
    ($i: ident) => {
        #[allow(clippy::default_numeric_fallback)]
        impl Bezier2<$i> for LinearBezier2<$i> {
            type Output = Self;
            type Derivative = Vec2<$i>;

            fn length(&self) -> $i {
                todo!()
            }

            fn point(&self, t: $i) -> Vec2<$i> {
                (1.0 - t) * self.point_a + t * self.point_b
            }

            fn split(&self, t: $i) -> [Self::Output; 2] {
                unimplemented!("Split is not implemented for LinearBezier2");
            }

            fn derivative(&self) -> Self::Derivative {
                todo!()
            }
        }
    }
}

impl_bezier!(f32);
impl_bezier!(f64);

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use crate::{
        bezier::{Bezier2, LinearBezier2f},
        vector::Vec2f,
    };

    #[test]
    fn point() {
        let line = LinearBezier2f::new(Vec2f::new(0.0, 0.0), Vec2f::new(1.0, 1.0));

        assert_eq!(line.point(0.0), Vec2f::new(0.0, 0.0));
        assert_eq!(line.point(0.25), Vec2f::new(0.25, 0.25));
        assert_eq!(line.point(0.5), Vec2f::new(0.5, 0.5));
        assert_eq!(line.point(0.75), Vec2f::new(0.75, 0.75));
        assert_eq!(line.point(1.0), Vec2f::new(1.0, 1.0));
    }
}
