//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::ops::{Add, AddAssign, Div, DivAssign, Index, IndexMut, Mul, MulAssign, Rem, RemAssign, Sub, SubAssign};

use num_traits::{Float, Num, NumAssign};

use crate::{lerp, lerp_clamped, matrix::Mat, Rotation};

//--------------------------------------------------------------------------------------------------
//-- TYPES -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub type Vec1i = Vec1<i32>;
pub type Vec1u = Vec1<u32>;
pub type Vec1f = Vec1<f32>;
pub type Vec1d = Vec1<f64>;

pub type Vec2i = Vec2<i32>;
pub type Vec2u = Vec2<u32>;
pub type Vec2f = Vec2<f32>;
pub type Vec2d = Vec2<f64>;

pub type Vec3i = Vec3<i32>;
pub type Vec3u = Vec3<u32>;
pub type Vec3f = Vec3<f32>;
pub type Vec3d = Vec3<f64>;

pub type Vec4i = Vec4<i32>;
pub type Vec4u = Vec4<u32>;
pub type Vec4f = Vec4<f32>;
pub type Vec4d = Vec4<f64>;

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// A 1-dimensional vector.
#[derive(Debug, Default, Copy, Clone, Hash, PartialOrd, PartialEq)]
pub struct Vec1<S> {
    pub x: S,
}

//--------------------------------------------------------------------------------------------------

/// A 2-dimensional vector.
#[derive(Debug, Default, Copy, Clone, Hash, PartialOrd, PartialEq)]
pub struct Vec2<S> {
    pub x: S,
    pub y: S,
}

impl<S> Vec2<S>
where
    S: Float,
{
    pub fn from_rotation(rotation: Rotation) -> Self {
        let radians = rotation.to_radians().value();

        Self {
            x: S::from(radians.cos()).unwrap(),
            y: S::from(radians.sin()).unwrap(),
        }
    }
}

//--------------------------------------------------------------------------------------------------

/// A 3-dimensional vector.
#[derive(Debug, Default, Copy, Clone, Hash, PartialOrd, PartialEq)]
pub struct Vec3<S> {
    pub x: S,
    pub y: S,
    pub z: S,
}

impl<S> Vec3<S>
where
    S: Float,
{
    pub fn from_rotation(yaw: Rotation, pitch: Rotation) -> Self {
        let yaw_rad = yaw.to_radians().value();
        let pitch_rad = pitch.to_radians().value();

        let x = yaw_rad.cos() * pitch_rad.cos();
        let y = pitch_rad.sin();
        let z = yaw_rad.sin() * pitch_rad.cos();

        Self {
            x: S::from(x).unwrap(),
            y: S::from(y).unwrap(),
            z: S::from(z).unwrap(),
        }
    }
}

//--------------------------------------------------------------------------------------------------

/// A 4-dimensional vector.
#[derive(Debug, Default, Copy, Clone, Hash, PartialOrd, PartialEq)]
pub struct Vec4<S> {
    pub x: S,
    pub y: S,
    pub z: S,
    pub w: S,
}

//--------------------------------------------------------------------------------------------------
//-- MACROS ----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

macro_rules! impl_vector {
    ($VecN:ident { $($field:ident),+}) => {
        impl<S> $VecN<S> {
            pub const fn new($($field: S),+) -> Self {
                Self { $($field),+ }
            }
        }

        impl<S: Copy + Num> $VecN<S> {
            pub fn magnitude(&self) -> S {
                $(self.$field * self.$field +)+ S::zero()
            }

            pub fn dot(&self, other: &Self) -> S {
                $(self.$field * other.$field +)+ S::zero()
            }
        }

        impl<S: Float> $VecN<S> {
            pub fn normalise(&self) -> Self {
                self / self.magnitude()
            }

            pub fn lerp(a: &Self, b: &Self, t: S) -> Self {
                Self { $($field: lerp(a.$field, b.$field, t)),+ }
            }

            pub fn lerp_clamped(a: &Self, b: &Self, t: S) -> Self {
                Self { $($field: lerp_clamped(a.$field, b.$field, t)),+ }
            }
        }
    };
}

impl_vector!(Vec1 { x });
impl_vector!(Vec2 { x, y });
impl_vector!(Vec3 { x, y, z });
impl_vector!(Vec4 { x, y, z, w });

//--------------------------------------------------------------------------------------------------

macro_rules! impl_conversions {
    ($VecN:ident { $($field:ident),+ }, $n:expr) => {
        impl<T: Num> AsRef<[T; $n]> for $VecN<T> {
            #[allow(clippy::transmute_ptr_to_ptr)]
            #[inline]
            fn as_ref(&self) -> &[T; $n] {
                // SAFETY: The vector and array have the same memory layout.
                unsafe { std::mem::transmute(self) }
            }
        }

        impl<T: Num> AsMut<[T; $n]> for $VecN<T> {
            #[allow(clippy::transmute_ptr_to_ptr)]
            #[inline]
            fn as_mut(&mut self) -> &mut [T; $n] {
                // SAFETY: The vector and array have the same memory layout.
                unsafe { std::mem::transmute(self) }
            }
        }

        impl<T: Copy> From<[T; $n]> for $VecN<T> {
            fn from(value: [T; $n]) -> Self {
                // SAFETY: The vector and array have the same memory layout.
                unsafe { std::mem::transmute_copy(&value) }
            }
        }

        impl<T: Copy> From<&[T; $n]> for $VecN<T> {
            fn from(value: &[T; $n]) -> Self {
                // SAFETY: The vector and array have the same memory layout.
                unsafe { std::mem::transmute_copy(value) }
            }
        }
    };
}

impl_conversions!(Vec1 { x }, 1);
impl_conversions!(Vec2 { x, y }, 2);
impl_conversions!(Vec3 { x, y, z }, 3);
impl_conversions!(Vec4 { x, y, z, w }, 4);

//--------------------------------------------------------------------------------------------------

macro_rules! impl_vector_ops {
    ($VecN:ident { $($field:ident),+ }) => {

        //--- ADD ----------------------------------------------------------------------------------
        impl<T: Num> Add<$VecN<T>> for $VecN<T> {
            type Output = Self;
            fn add(self, rhs: Self) -> Self::Output { Self::new($(self.$field + rhs.$field,)+) }
        }

        impl<T: Copy + Num> Add<$VecN<T>> for &$VecN<T> {
            type Output = $VecN<T>;
            fn add(self, rhs: $VecN<T>) -> Self::Output { Self::Output::new($(self.$field + rhs.$field,)+) }
        }

        impl<T: Copy + Num> Add<T> for $VecN<T> {
            type Output = Self;
            fn add(self, scalar: T) -> Self::Output { Self::new($(self.$field + scalar,)+) }
        }

        impl<T: Copy + Num> Add<T> for &$VecN<T> {
            type Output = $VecN<T>;
            fn add(self, scalar: T) -> Self::Output { Self::Output::new($(self.$field + scalar,)+) }
        }

        impl<T: NumAssign> AddAssign<$VecN<T>> for $VecN<T> {
            fn add_assign(&mut self, rhs: Self) { $(self.$field.add_assign(rhs.$field);)+ }
        }

        impl<T: Copy + NumAssign> AddAssign<T> for $VecN<T> {
            fn add_assign(&mut self, scalar: T) { $(self.$field.add_assign(scalar);)+ }
        }

        //--- SUB ----------------------------------------------------------------------------------
        impl<T: Num> Sub<$VecN<T>> for $VecN<T> {
            type Output = Self;
            fn sub(self, rhs: Self) -> Self::Output { Self::new($(self.$field - rhs.$field,)+) }
        }

        impl<T: Copy + Num> Sub<$VecN<T>> for &$VecN<T> {
            type Output = $VecN<T>;
            fn sub(self, rhs: $VecN<T>) -> Self::Output { Self::Output::new($(self.$field - rhs.$field,)+) }
        }

        impl<T: Copy + Num> Sub<T> for $VecN<T> {
            type Output = Self;
            fn sub(self, scalar: T) -> Self::Output { Self::new($(self.$field - scalar,)+) }
        }

        impl<T: Copy + Num> Sub<T> for &$VecN<T> {
            type Output = $VecN<T>;
            fn sub(self, scalar: T) -> Self::Output { Self::Output::new($(self.$field - scalar,)+) }
        }

        impl<T: NumAssign> SubAssign<$VecN<T>> for $VecN<T> {
            fn sub_assign(&mut self, rhs: Self) { $(self.$field.sub_assign(rhs.$field);)+ }
        }

        impl<T: Copy + NumAssign> SubAssign<T> for $VecN<T> {
            fn sub_assign(&mut self, scalar: T) { $(self.$field.sub_assign(scalar);)+ }
        }

        //--- MUL ----------------------------------------------------------------------------------
        impl<T: Num> Mul<$VecN<T>> for $VecN<T> {
            type Output = Self;
            fn mul(self, rhs: Self) -> Self::Output { Self::new($(self.$field * rhs.$field,)+) }
        }

        impl<T: Copy + Num> Mul<$VecN<T>> for &$VecN<T> {
            type Output = $VecN<T>;
            fn mul(self, rhs: $VecN<T>) -> Self::Output { Self::Output::new($(self.$field * rhs.$field,)+) }
        }

        impl<T: Copy + Num> Mul<T> for $VecN<T> {
            type Output = Self;
            fn mul(self, scalar: T) -> Self::Output { Self::new($(self.$field * scalar,)+) }
        }

        impl<T: Copy + Num> Mul<T> for &$VecN<T> {
            type Output = $VecN<T>;
            fn mul(self, scalar: T) -> Self::Output { Self::Output::new($(self.$field * scalar,)+) }
        }

        impl<T: NumAssign> MulAssign<$VecN<T>> for $VecN<T> {
            fn mul_assign(&mut self, rhs: Self) { $(self.$field.mul_assign(rhs.$field);)+ }
        }

        impl<T: Copy + NumAssign> MulAssign<T> for $VecN<T> {
            fn mul_assign(&mut self, scalar: T) { $(self.$field.mul_assign(scalar);)+ }
        }

        //--- DIV ----------------------------------------------------------------------------------
        impl<T: Num> Div<$VecN<T>> for $VecN<T> {
            type Output = Self;
            fn div(self, rhs: Self) -> Self::Output { Self::new($(self.$field / rhs.$field,)+) }
        }

        impl<T: Copy + Num> Div<$VecN<T>> for &$VecN<T> {
            type Output = $VecN<T>;
            fn div(self, rhs: Self::Output) -> Self::Output { Self::Output::new($(self.$field / rhs.$field,)+) }
        }

        impl<T: Copy + Num> Div<T> for $VecN<T> {
            type Output = Self;
            fn div(self, scalar: T) -> Self::Output { Self::new($(self.$field / scalar,)+) }
        }

        impl<T: Copy + Num> Div<T> for &$VecN<T> {
            type Output = $VecN<T>;
            fn div(self, scalar: T) -> Self::Output { Self::Output::new($(self.$field / scalar,)+) }
        }

        impl<T: NumAssign> DivAssign<$VecN<T>> for $VecN<T> {
            fn div_assign(&mut self, rhs: Self) { $(self.$field.div_assign(rhs.$field);)+ }
        }

        impl<T: Copy + NumAssign> DivAssign<T> for $VecN<T> {
            fn div_assign(&mut self, scalar: T) { $(self.$field.div_assign(scalar);)+ }
        }

        //--- REM ----------------------------------------------------------------------------------
        impl<T: Num> Rem<$VecN<T>> for $VecN<T> {
            type Output = Self;
            fn rem(self, rhs: Self) -> Self::Output { Self::new($(self.$field % rhs.$field,)+) }
        }

        impl<T: Copy + Num> Rem<$VecN<T>> for &$VecN<T> {
            type Output = $VecN<T>;
            fn rem(self, rhs: $VecN<T>) -> Self::Output { Self::Output::new($(self.$field % rhs.$field,)+) }
        }

        impl<T: Copy + Num> Rem<T> for $VecN<T> {
            type Output = Self;
            fn rem(self, scalar: T) -> Self::Output { Self::new($(self.$field % scalar,)+) }
        }

        impl<T: Copy + Num> Rem<T> for &$VecN<T> {
            type Output = $VecN<T>;
            fn rem(self, scalar: T) -> Self::Output { Self::Output::new($(self.$field % scalar,)+) }
        }

        impl<T: NumAssign> RemAssign<$VecN<T>> for $VecN<T> {
            fn rem_assign(&mut self, rhs: Self) { $(self.$field.rem_assign(rhs.$field);)+ }
        }

        impl<T: Copy + NumAssign> RemAssign<T> for $VecN<T> {
            fn rem_assign(&mut self, scalar: T) { $(self.$field.rem_assign(scalar);)+ }
        }

        //--- INDEX --------------------------------------------------------------------------------
        impl<T: Num> Index<usize> for $VecN<T> {
            type Output = T;

            fn index(&self, index: usize) -> &Self::Output {
                &self.as_ref()[index]
            }
        }

        impl<T: Num> IndexMut<usize> for $VecN<T> {
            fn index_mut(&mut self, index: usize) -> &mut Self::Output {
                &mut self.as_mut()[index]
            }
        }

        //--- SCALAR OPS ---------------------------------------------------------------------------
        impl_scalar_ops!($VecN<u8>    { $($field),+ });
        impl_scalar_ops!($VecN<u16>   { $($field),+ });
        impl_scalar_ops!($VecN<u32>   { $($field),+ });
        impl_scalar_ops!($VecN<u64>   { $($field),+ });
        impl_scalar_ops!($VecN<usize> { $($field),+ });
        impl_scalar_ops!($VecN<i8>    { $($field),+ });
        impl_scalar_ops!($VecN<i16>   { $($field),+ });
        impl_scalar_ops!($VecN<i32>   { $($field),+ });
        impl_scalar_ops!($VecN<i64>   { $($field),+ });
        impl_scalar_ops!($VecN<isize> { $($field),+ });
        impl_scalar_ops!($VecN<f32>   { $($field),+ });
        impl_scalar_ops!($VecN<f64>   { $($field),+ });
    };
}

//--------------------------------------------------------------------------------------------------

macro_rules! impl_scalar_ops {
    ($VecN:ident < $S:ident >  { $($field:ident),+ }) => {
        //--- ADD ----------------------------------------------------------------------------------
        impl Add<$VecN<$S>> for $S {
            type Output = $VecN<$S>;
            fn add(self, vector: Self::Output) -> Self::Output {
                vector + self
            }
        }

        impl Add<&$VecN<$S>> for $S {
            type Output = $VecN<$S>;
            fn add(self, vector: &$VecN<$S>) -> Self::Output {
                vector + self
            }
        }

        //--- SUB ----------------------------------------------------------------------------------
        impl Sub<$VecN<$S>> for $S {
            type Output = $VecN<$S>;
            fn sub(self, vector: Self::Output) -> Self::Output {
                vector - self
            }
        }

        impl Sub<&$VecN<$S>> for $S {
            type Output = $VecN<$S>;
            fn sub(self, vector: &$VecN<$S>) -> Self::Output {
                vector - self
            }
        }

        //--- MUL ----------------------------------------------------------------------------------
        impl Mul<$VecN<$S>> for $S {
            type Output = $VecN<$S>;
            fn mul(self, vector: Self::Output) -> Self::Output {
                vector * self
            }
        }

        impl Mul<&$VecN<$S>> for $S {
            type Output = $VecN<$S>;
            fn mul(self, vector: &$VecN<$S>) -> Self::Output {
                vector * self
            }
        }

        //--- DIV ----------------------------------------------------------------------------------
        impl Div<$VecN<$S>> for $S {
            type Output = $VecN<$S>;
            fn div(self, vector: Self::Output) -> Self::Output {
                vector / self
            }
        }

        impl Div<&$VecN<$S>> for $S {
            type Output = $VecN<$S>;
            fn div(self, vector: &$VecN<$S>) -> Self::Output {
                vector / self
            }
        }

        //--- REM ----------------------------------------------------------------------------------
        impl Rem<$VecN<$S>> for $S {
            type Output = $VecN<$S>;
            fn rem(self, vector: Self::Output) -> Self::Output {
                vector % self
            }
        }

        impl Rem<&$VecN<$S>> for $S {
            type Output = $VecN<$S>;
            fn rem(self, vector: &$VecN<$S>) -> Self::Output {
                vector % self
            }
        }
    };
}

//--------------------------------------------------------------------------------------------------

impl_vector_ops!(Vec1 { x });
impl_vector_ops!(Vec2 { x, y });
impl_vector_ops!(Vec3 { x, y, z });
impl_vector_ops!(Vec4 { x, y, z, w });

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use crate::vector::{Vec2f, Vec3f};

    #[test]
    fn as_ref() {
        // Get an array ref from a Vec2f, and ensure it contains the same values as the original Vec.
        let vec2 = Vec2f::new(5.0, 4.0);
        let vec2_ref = vec2.as_ref();
        assert_eq!(vec2_ref, &[5.0, 4.0]);

        // Same as above, but with a Vec3f.
        let vec3 = Vec3f::new(1.0, 2.0, 3.0);
        let vec3_ref = vec3.as_ref();
        assert_eq!(vec3_ref, &[1.0, 2.0, 3.0]);
    }

    //----------------------------------------------------------------------------------------------

    #[test]
    fn as_mut() {
        // Get a mutable array reference to a Vec3f;
        let mut vec3 = Vec3f::new(1.0, 2.0, 3.0);
        let vec3_mut = vec3.as_mut();

        assert_eq!(vec3_mut, &[1.0, 2.0, 3.0]);

        // Mutate the values in the array, then assert that the Vec3f has also been updated.
        vec3_mut[0] = 7.5;
        vec3_mut[2] = 10.3;

        assert_eq!(vec3_mut, &[7.5, 2.0, 10.3]);
        assert_eq!(vec3, Vec3f::new(7.5, 2.0, 10.3));
    }

    //----------------------------------------------------------------------------------------------

    #[test]
    fn index() {
        let vec3 = Vec3f::new(1.0, 2.0, 3.0);
        assert_eq!(vec3[0], 1.0);
        assert_eq!(vec3[1], 2.0);
        assert_eq!(vec3[2], 3.0);
    }

    //----------------------------------------------------------------------------------------------

    #[test]
    fn index_mut() {
        let mut vec3 = Vec3f::new(1.0, 2.0, 3.0);

        vec3[0] = 9.0;
        vec3[1] = 8.0;
        vec3[2] = 7.0;

        assert_eq!(vec3, Vec3f::new(9.0, 8.0, 7.0));
    }
}
