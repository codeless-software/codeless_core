//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

#![macro_use]

#[rustfmt::skip]
macro_rules! impl_operator {
    (<$($([$c:tt])? $s:ident: $bound:ident $(+ $other_bounds:ident)*),+> $op:ident<$rhs:ty> for $lhs:ty {
        fn $method:ident($lhs_var:ident, $rhs_var:ident) -> $output:ty { $body:expr }
    }) => {
        impl<$($($c)? $s: $bound $(+ $other_bounds)*),+> $op<$rhs> for $lhs {
            type Output = $output;
            fn $method(self, other: $rhs) -> Self::Output {
                let ($lhs_var, $rhs_var) = (self, other); $body
            }
        }
        
        impl<$($($c)? $s: $bound $(+ $other_bounds)*),+> $op<&$rhs> for $lhs {
            type Output = $output;
            fn $method(self, other: &$rhs) -> Self::Output {
                let ($lhs_var, $rhs_var) = (self, other); $body
            }
        }
        
        impl<$($($c)? $s: $bound $(+ $other_bounds)*),+> $op<$rhs> for &$lhs {
            type Output = $output;
            fn $method(self, other: $rhs) -> Self::Output {
                let ($lhs_var, $rhs_var) = (self, other); $body
            }
        }
        
        impl<$($($c)? $s: $bound $(+ $other_bounds)*),+> $op<&$rhs> for &$lhs {
            type Output = $output;
            fn $method(self, other: &$rhs) -> Self::Output {
                let ($lhs_var, $rhs_var) = (self, other); $body
            }
        }
    }
}
