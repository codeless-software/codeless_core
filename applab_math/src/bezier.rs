//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

mod cube2;
mod line2;
mod quad2;

use std::{fmt::Display, fs, io};

use num_traits::Float;

pub use self::{
    cube2::{CubicBezier2, CubicBezier2d, CubicBezier2f},
    line2::{LinearBezier2, LinearBezier2d, LinearBezier2f},
    quad2::{QuadBezier2, QuadBezier2d, QuadBezier2f},
};
use crate::vector::Vec2;

//--------------------------------------------------------------------------------------------------
//-- TRAITS ----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub trait Bezier2<I> {
    type Output;
    type Derivative;

    fn length(&self) -> I;
    fn point(&self, t: I) -> Vec2<I>;
    fn split(&self, t: I) -> [Self::Output; 2];
    fn derivative(&self) -> Self::Derivative;
}

//--------------------------------------------------------------------------------------------------
//-- FUNCTIONS -------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// Exports points on a bezier curve as a csv file.
///
/// This can be useful for debugging and visualising the curves in spreadsheet applications.
pub fn export_csv<I, T>(bezier: &T, steps: u32, name: &str) -> io::Result<()>
where
    I: Display + Float,
    T: Bezier2<I>,
{
    let step_len = 1.0 / steps as f32;

    let output = (0..=steps)
        .map(|i| {
            let point = bezier.point(I::from(step_len * i as f32).expect("Failed to create `T` from provided values"));
            format!("{}\t{}\n", point.x, point.y)
        })
        .collect::<String>();

    fs::write(name, output)
}
