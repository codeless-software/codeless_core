//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

mod determinant;

use std::{
    fmt::{Display, Formatter},
    ops::{Add, AddAssign, Index, IndexMut, Mul, Neg, Sub},
};

pub use determinant::*;
use num_traits::{Num, NumCast};
use rand::Rng;

use crate::vector::{Vec2, Vec3, Vec4};

//--------------------------------------------------------------------------------------------------
//-- TYPES -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub type SquareMat<S, const D: usize> = Mat<S, D, D>;

pub type Mat1<S> = SquareMat<S, 1>;
pub type Mat1f = Mat1<f32>;
pub type Mat1d = Mat1<f64>;

pub type Mat2<S> = SquareMat<S, 2>;
pub type Mat2f = Mat2<f32>;
pub type Mat2d = Mat2<f64>;

pub type Mat3<S> = SquareMat<S, 3>;
pub type Mat3f = Mat3<f32>;
pub type Mat3d = Mat3<f64>;

pub type Mat4<S> = SquareMat<S, 4>;
pub type Mat4f = Mat4<f32>;
pub type Mat4d = Mat4<f64>;

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// A generic matrix with scalar type S, ROWS rows and COLS columns.
#[derive(Debug)]
pub struct Mat<S, const ROWS: usize, const COLS: usize> {
    array: [[S; COLS]; ROWS],
}

//--------------------------------------------------------------------------------------------------
//-- MATRIX IMPLEMENTATIONS ------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// Nicely format the matrix
impl<S: Display, const ROWS: usize, const COLS: usize> Display for Mat<S, ROWS, COLS> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for row in 0..ROWS {
            for col in 0..COLS {
                write!(f, "{:>4}", self.array[row][col])?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

//--------------------------------------------------------------------------------------------------

/// Implement default
impl<S, const ROWS: usize, const COLS: usize> Default for Mat<S, ROWS, COLS>
where
    S: Num + NumCast + Copy,
{
    fn default() -> Self {
        Self::zero()
    }
}

//--------------------------------------------------------------------------------------------------

impl<S, const ROWS: usize, const COLS: usize> AsRef<[[S; COLS]; ROWS]> for Mat<S, ROWS, COLS> {
    fn as_ref(&self) -> &[[S; COLS]; ROWS] {
        &self.array
    }
}

impl<S, const ROWS: usize, const COLS: usize> AsMut<[[S; COLS]; ROWS]> for Mat<S, ROWS, COLS> {
    fn as_mut(&mut self) -> &mut [[S; COLS]; ROWS] {
        &mut self.array
    }
}

//--------------------------------------------------------------------------------------------------

impl<S, const ROWS: usize, const COLS: usize> Index<usize> for Mat<S, ROWS, COLS> {
    type Output = [S; COLS];

    fn index(&self, index: usize) -> &Self::Output {
        &self.array[index]
    }
}

impl<S, const ROWS: usize, const COLS: usize> IndexMut<usize> for Mat<S, ROWS, COLS> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.array[index]
    }
}

//--------------------------------------------------------------------------------------------------

/// Functions for matrices of any size.
impl<S, const ROWS: usize, const COLS: usize> Mat<S, ROWS, COLS>
where
    S: Num + NumCast + Copy,
{
    pub fn zero() -> Self {
        Self {
            array: [[S::zero(); COLS]; ROWS],
        }
    }

    /// Returns a new random matrix.
    ///
    /// The value range is from -256 to 256.
    pub fn random() -> Self {
        let mut rng = rand::thread_rng();
        let mut array = [[S::zero(); COLS]; ROWS];

        for row in 0..ROWS {
            for col in 0..COLS {
                array[row][col] = S::from(rng.gen_range(u8::MIN..u8::MAX))
                    .expect("Failed to create 'S' from random value. This should never happen.");
            }
        }

        Self { array }
    }

    #[inline]
    pub const fn is_square(&self) -> bool {
        ROWS == COLS
    }
}

//--------------------------------------------------------------------------------------------------

/// Functions for matrices where the rows and columns are the same.
impl<S, const SIZE: usize> Mat<S, SIZE, SIZE>
where
    S: Num + NumCast + Default + Copy + AddAssign + Neg,
{
    /// Returns a new identity matrix.
    pub fn identity() -> Self {
        let array = std::array::from_fn(|i| std::array::from_fn(|j| if i == j { S::one() } else { S::zero() }));
        Self { array }
    }
}

//--------------------------------------------------------------------------------------------------
//-- MACROS ----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

// TODO: Add a debug assert check to ensure the correct layout is used.
macro_rules! impl_matrix_index_ops {
    ($MatN:ident, $VecN:ident, $n:expr) => {
        impl_operator!(<S: Copy + Num + Default + AddAssign> Mul<$VecN<S>> for $MatN<S> {
            fn mul(lhs, rhs) -> $VecN<S> {
                {
                    let mut res = $VecN::<S>::default();
                    for row in 0..$n {
                        for col in 0..$n {
                            res[row] += lhs[row][col] * rhs[col];
                        }
                    }

                    res
                }
            }
        });
    };
}

impl_matrix_index_ops!(Mat2, Vec2, 2);
impl_matrix_index_ops!(Mat3, Vec3, 3);
impl_matrix_index_ops!(Mat4, Vec4, 4);

//--------------------------------------------------------------------------------------------------

// Matrix addition
impl_operator!(<S: Copy + Num + NumCast, [const] ROWS: usize, [const] COLS: usize> Add<Mat<S, ROWS, COLS>> for Mat<S, ROWS, COLS> {
    fn add(lhs, rhs) -> Mat<S, ROWS, COLS> {
        {
            let mut res = Mat::zero();
            for row in 0..ROWS {
                for col in 0..COLS {
                    res[row][col] = lhs[row][col] + rhs[row][col];
                }
            }

            res
        }
    }
});

// Matrix subtraction
impl_operator!(<S: Copy + Num + NumCast, [const] ROWS: usize, [const] COLS: usize> Sub<Mat<S, ROWS, COLS>> for Mat<S, ROWS, COLS> {
    fn sub(lhs, rhs) -> Mat<S, ROWS, COLS> {
        {
            let mut res = Mat::zero();
            for row in 0..ROWS {
                for col in 0..COLS {
                    res[row][col] = lhs[row][col] - rhs[row][col];
                }
            }

            res
        }
    }
});

// Matrix multiplication
impl_operator!(<S: Num + NumCast + Copy + AddAssign, [const] L_ROWS: usize, [const] R_COLS: usize, [const] SHARED: usize> Mul<Mat<S, SHARED, R_COLS>> for Mat<S, L_ROWS, SHARED> {
    fn mul(lhs, rhs) -> Mat<S, SHARED, R_COLS> {
        {
            let mut res = Mat::zero();

            for row in 0..L_ROWS {
                for col in 0..R_COLS {
                    for shared in 0..SHARED {
                        res[row][col] += lhs[row][shared] * rhs[shared][col];
                    }
                }
            }

            res
        }
    }
});

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use crate::matrix::{Mat, Mat3f};

    #[test]
    fn index() {
        let mat = Mat3f {
            array: [[1.0, 2.0, 3.0], [4.0, 5.0, 6.0], [7.0, 8.0, 9.0]],
        };

        assert_eq!(mat[0], [1.0, 2.0, 3.0]);
        assert_eq!(mat[1], [4.0, 5.0, 6.0]);
        assert_eq!(mat[2], [7.0, 8.0, 9.0]);
    }

    //----------------------------------------------------------------------------------------------

    #[test]
    fn index_mut() {
        let mut mat = Mat3f {
            array: [[1.0, 2.0, 3.0], [4.0, 5.0, 6.0], [7.0, 8.0, 9.0]],
        };

        assert_ne!(mat[0], [1.0, 0.0, 0.0]);
        assert_ne!(mat[1], [0.0, 1.0, 0.0]);
        assert_ne!(mat[2], [0.0, 0.0, 1.0]);

        mat[0] = [1.0, 0.0, 0.0];
        mat[1] = [0.0, 1.0, 0.0];
        mat[2] = [0.0, 0.0, 1.0];

        assert_eq!(mat[0], [1.0, 0.0, 0.0]);
        assert_eq!(mat[1], [0.0, 1.0, 0.0]);
        assert_eq!(mat[2], [0.0, 0.0, 1.0]);
    }

    //----------------------------------------------------------------------------------------------

    #[test]
    fn identity() {
        let mat = Mat::<f32, 10, 10>::identity() * Mat::<f32, 10, 5>::zero();
        println!("{}", mat);
    }
}
