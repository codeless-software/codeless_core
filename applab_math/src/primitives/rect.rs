//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use num_traits::{Float, Num, NumAssignOps, NumOps};

use crate::{primitives::Line, vector::Vec2};

pub type Rectf = Rect<f32>;
pub type Rectd = Rect<f64>;

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub enum RectOrigin {
    Center,
    TopLeft,
    TopRight,
    BottomLeft,
    BottomRight,
}

#[derive(Debug, Copy, Clone)]
pub struct Rect<T>
where
    T: num_traits::Float,
{
    /// First point in the rect.
    pub a: Vec2<T>,

    /// Second point in the rect.
    pub b: Vec2<T>,
}

impl<T> Rect<T>
where
    T: Float + NumOps,
{
    #[inline]
    pub const fn new(a: Vec2<T>, b: Vec2<T>) -> Self {
        Self { a, b }
    }

    pub fn from_dimensions(width: T, height: T, position: Vec2<T>, origin: RectOrigin) -> Self {
        let (min, max) = match origin {
            RectOrigin::Center => {
                let half_width = width / T::from(2).unwrap();
                let half_height = height / T::from(2).unwrap();
                (
                    Vec2::new(position.x - half_width, position.y - half_height),
                    Vec2::new(position.x + half_width, position.y + half_height),
                )
            }
            RectOrigin::TopLeft => (position, Vec2::new(position.x + width, position.y + height)),
            RectOrigin::TopRight => (
                Vec2::new(position.x - width, position.y),
                Vec2::new(position.x, position.y + height),
            ),
            RectOrigin::BottomLeft => (
                Vec2::new(position.x, position.y - height),
                Vec2::new(position.x + width, position.y),
            ),
            RectOrigin::BottomRight => (Vec2::new(position.x - width, position.y - height), position),
        };

        Self::new(min, max)
    }

    /// Returns the top-left corner of the rectangle.
    #[inline]
    pub fn top_left(&self) -> Vec2<T> {
        Vec2::new(T::min(self.a.x, self.b.x), T::min(self.a.y, self.b.y))
    }

    /// Returns the top-right corner of the rectangle.
    #[inline]
    pub fn top_right(&self) -> Vec2<T> {
        Vec2::new(T::max(self.a.x, self.b.x), T::min(self.a.y, self.b.y))
    }

    /// Returns the bottom-left corner of the rectangle.
    #[inline]
    pub fn bottom_left(&self) -> Vec2<T> {
        Vec2::new(T::min(self.a.x, self.b.x), T::max(self.a.y, self.b.y))
    }

    /// Returns the bottom-right corner of the rectangle.
    #[inline]
    pub fn bottom_right(&self) -> Vec2<T> {
        Vec2::new(T::max(self.a.x, self.b.x), T::max(self.a.y, self.b.y))
    }

    #[inline]
    pub fn min(&self) -> Vec2<T> {
        Vec2::new(T::min(self.a.x, self.b.x), T::min(self.a.y, self.b.y))
    }

    #[inline]
    pub fn max(&self) -> Vec2<T> {
        Vec2::new(T::max(self.a.x, self.b.x), T::max(self.a.y, self.b.y))
    }

    /// Returns all four corners of the rectangle in the following order:
    /// * Top Left
    /// * Top Right
    /// * Bottom Right
    /// * Bottom Left
    #[inline]
    pub fn corners(&self) -> (Vec2<T>, Vec2<T>, Vec2<T>, Vec2<T>) {
        (
            self.top_left(),
            self.top_right(),
            self.bottom_right(),
            self.bottom_left(),
        )
    }

    /// Returns all four edges of the rectangle in the following order:
    /// * Top
    /// * Right
    /// * Bottom
    /// * Left
    pub fn edges(&self) -> (Line<T>, Line<T>, Line<T>, Line<T>) {
        (
            Line::new(self.top_left(), self.top_right()),
            Line::new(self.top_right(), self.bottom_right()),
            Line::new(self.bottom_right(), self.bottom_left()),
            Line::new(self.bottom_left(), self.top_left()),
        )
    }

    /// Returns the center of the rectangle.
    #[inline]
    pub fn center(&self) -> Vec2<T> {
        self.a + (self.b - self.a) / T::from(2).expect("The number 2 couldn't be converted to a float... What?")
    }

    /// Returns the width of the rectangle.
    #[inline]
    pub fn width(&self) -> T {
        (self.a.x - self.b.x).abs()
    }

    /// Returns the height of the rectangle.
    #[inline]
    pub fn height(&self) -> T {
        (self.a.y - self.b.y).abs()
    }

    /// Returns whether the rectangle contains a given point.
    #[inline]
    pub fn contains_point(&self, point: Vec2<T>) -> bool {
        point >= self.top_left() && point <= self.bottom_right()
    }
}

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use crate::{primitives::Rectf, vector::Vec2};

    fn rect_variations() -> (Rectf, Rectf, Rectf, Rectf) {
        (
            Rectf::new(Vec2::default(), Vec2::new(2.0, 1.0)),
            Rectf::new(Vec2::default(), Vec2::new(-2.0, 1.0)),
            Rectf::new(Vec2::default(), Vec2::new(-2.0, -1.0)),
            Rectf::new(Vec2::default(), Vec2::new(2.0, -1.0)),
        )
    }

    #[test]
    fn top_left() {
        let (br, bl, tr, tl) = rect_variations();

        assert_eq!(br.top_left(), Vec2::new(0.0, 0.0));
        assert_eq!(bl.top_left(), Vec2::new(-2.0, 0.0));
        assert_eq!(tr.top_left(), Vec2::new(-2.0, -1.0));
        assert_eq!(tl.top_left(), Vec2::new(0.0, -1.0));
    }

    #[test]
    fn top_right() {
        let (br, bl, tr, tl) = rect_variations();

        assert_eq!(br.top_right(), Vec2::new(2.0, 0.0));
        assert_eq!(bl.top_right(), Vec2::new(0.0, 0.0));
        assert_eq!(tr.top_right(), Vec2::new(0.0, -1.0));
        assert_eq!(tl.top_right(), Vec2::new(2.0, -1.0));
    }

    #[test]
    fn bottom_left() {
        let (br, bl, tr, tl) = rect_variations();

        assert_eq!(br.bottom_left(), Vec2::new(0.0, 1.0));
        assert_eq!(bl.bottom_left(), Vec2::new(-2.0, 1.0));
        assert_eq!(tr.bottom_left(), Vec2::new(-2.0, 0.0));
        assert_eq!(tl.bottom_left(), Vec2::new(0.0, 0.0));
    }

    #[test]
    fn bottom_right() {
        let (br, bl, tr, tl) = rect_variations();

        assert_eq!(br.bottom_right(), Vec2::new(2.0, 1.0));
        assert_eq!(bl.bottom_right(), Vec2::new(0.0, 1.0));
        assert_eq!(tr.bottom_right(), Vec2::new(0.0, 0.0));
        assert_eq!(tl.bottom_right(), Vec2::new(2.0, 0.0));
    }
}
