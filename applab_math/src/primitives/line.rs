//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use num_traits::{Float, Num};

use crate::vector::Vec2;

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub struct Line<T>
where
    T: Copy + Num,
{
    pub start: Vec2<T>,
    pub end:   Vec2<T>,
}

impl<T> Line<T>
where
    T: Copy + Num,
{
    pub const fn new(start: Vec2<T>, end: Vec2<T>) -> Self {
        Self { start, end }
    }

    pub fn length(&self) -> T {
        (self.end - self.start).magnitude()
    }
}

impl<T> Line<T>
where
    T: Float,
{
    pub fn lerp(&self, t: T) -> Vec2<T> {
        Vec2::lerp(&self.start, &self.end, t)
    }

    pub fn lerp_unclamped(&self, t: T) -> Vec2<T> {
        Vec2::lerp_clamped(&self.start, &self.end, t)
    }
}
