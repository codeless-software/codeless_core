//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use num_traits::Float;

use crate::{primitives::Rect, vector::Vec2};

//--------------------------------------------------------------------------------------------------
//-- TYPES -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub type Ray2f = Ray2<f32>;
pub type Ray2d = Ray2<f64>;

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[derive(Debug)]
pub struct Ray2<T>
where
    T: Float,
{
    pub origin:    Vec2<T>,
    pub direction: Vec2<T>,
    pub distance:  T,

    inverse: Vec2<T>,
}

impl<T> Ray2<T>
where
    T: Float,
{
    pub fn new(origin: Vec2<T>, direction: Vec2<T>, distance: T) -> Self {
        let inverse = Vec2::new(T::one() / direction.x, T::one() / direction.y);

        Self {
            origin,
            direction,
            distance,
            inverse,
        }
    }

    /// Tests for an intersection between a `Ray2` and a `Rect`.
    ///
    /// # Arguments
    ///
    /// * `rect`: The `Rect` to test against.
    ///
    /// # Examples
    ///
    /// ```
    /// # use applab_math::{
    /// #    primitives::{Ray2, Rect, RectOrigin},
    /// #    vector::Vec2,
    /// #    Rotation,
    /// # };
    /// let rect = Rect::from_dimensions(4.0, 2.0, Vec2::default(), RectOrigin::Center);
    /// let ray = Ray2::new(
    ///     Vec2::new(4.0, -8.0),
    ///     Vec2::from_rotation(Rotation::Degrees(120.0)),
    ///     100.0,
    /// );
    ///
    /// assert!(ray.hits_rect(&rect));
    /// ```
    pub fn hits_rect(&self, rect: &Rect<T>) -> bool {
        let mut tmin = T::zero();
        let mut tmax = T::infinity();

        for i in 0..2 {
            let t1 = (rect.a[i] - self.origin[i]) * self.inverse[i];
            let t2 = (rect.b[i] - self.origin[i]) * self.inverse[i];

            tmin = T::max(T::min(t1, t2), tmin);
            tmax = T::min(T::max(t1, t2), tmax);
        }

        tmax > tmin
    }
}

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use super::Ray2;
    use crate::{
        primitives::Rect,
        vector::{Vec2, Vec2f},
        Rotation,
    };

    #[test]
    fn intersects() {
        let rect = Rect::new(Vec2f::new(0.0, 0.0), Vec2f::new(4.0, 2.0));

        //--- RAY HIT ------------------------------------------------------------------------------

        // Cast from left of rect to the right
        let ray = Ray2::new(Vec2::new(-2.0, 1.0), Vec2::from_rotation(Rotation::Degrees(0.0)), 100.0);
        assert!(ray.hits_rect(&rect));

        // Cast from bottom of rect upwards
        let ray = Ray2::new(
            Vec2::new(1.0, -4.0),
            Vec2::from_rotation(Rotation::Degrees(90.0)),
            100.0,
        );
        assert!(ray.hits_rect(&rect));

        // Cast diagonally from top right to bottom left
        let ray = Ray2::new(
            Vec2::new(8.0, 5.0),
            Vec2::from_rotation(Rotation::Degrees(220.0)),
            100.0,
        );
        assert!(ray.hits_rect(&rect));

        //--- RAY EDGE -----------------------------------------------------------------------------

        // NOTE: Ray edges currently fail, so the test has been disabled.
        //       They will be re-enabled once this is sorted.

        // Cast up along left edge of Rect
        let ray = Ray2::new(
            Vec2::new(0.0, -4.0),
            Vec2::from_rotation(Rotation::Degrees(90.0)),
            100.0,
        );
        // assert!(ray.hits_rect(&rect));

        let ray = Ray2::new(
            Vec2::new(10.0, 0.0),
            Vec2::from_rotation(Rotation::Degrees(180.0)),
            100.0,
        );
        // assert!(ray.hits_rect(&rect));

        //--- RAY MISS -----------------------------------------------------------------------------

        // Cast from left of rect to the right
        let ray = Ray2::new(Vec2::new(60.0, 1.0), Vec2::from_rotation(Rotation::Degrees(0.0)), 100.0);
        assert!(!ray.hits_rect(&rect));
    }
}
