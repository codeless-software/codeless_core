//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub mod bezier;
pub mod macros;
pub mod matrix;
pub mod primitives;
pub mod vector;

use num_traits::{clamp, Float};

//--------------------------------------------------------------------------------------------------
//-- ENUMS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// Represents a rotation in either degrees or radians.
#[derive(Debug, Copy, Clone)]
pub enum Rotation {
    Degrees(f32),
    Radians(f32),
}

impl Rotation {
    /// Converts a rotation in radians to degrees.
    ///
    /// If the original rotation is already in degrees, returns a copy of itself.
    pub fn to_degrees(&self) -> Self {
        match self {
            Self::Degrees(_) => *self,
            Self::Radians(v) => Self::Radians(v.to_degrees()),
        }
    }

    /// Converts a rotation in degrees to radians.
    ///
    /// If the original rotation is already in radians, returns a copy of itself.
    pub fn to_radians(&self) -> Self {
        match self {
            Self::Degrees(v) => Self::Degrees(v.to_radians()),
            Self::Radians(_) => *self,
        }
    }

    /// Returns the angle value as a `f32`, regardless of the original unit.
    ///
    /// If the original rotation is in degrees, returns the angle value in degrees.
    /// If the original rotation is in radians, returns the angle value in radians.
    pub const fn value(&self) -> f32 {
        match self {
            Self::Degrees(v) => *v,
            Self::Radians(v) => *v,
        }
    }
}

//--------------------------------------------------------------------------------------------------
//-- FUNCTIONS -------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[inline]
pub fn lerp<T: Float>(a: T, b: T, t: T) -> T {
    a + (b - a) * t
}

//--------------------------------------------------------------------------------------------------

#[inline]
pub fn lerp_clamped<T: Float>(a: T, b: T, t: T) -> T {
    let t = clamp(t, T::zero(), T::one());
    lerp(a, b, t)
}

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use std::f32::consts::PI;

    use crate::Rotation;

    #[test]
    fn rotation() {
        let deg_rotation = Rotation::Degrees(90.0);
        let rad_rotation = Rotation::Radians(PI);

        // Convert degrees to radians
        let rad = deg_rotation.to_radians();
        assert_eq!(rad.value(), PI / 2.0);

        // Convert radians to degrees
        let deg = rad_rotation.to_degrees();
        assert_eq!(deg.value(), 180.0);

        // Get angle value regardless of the original unit
        assert_eq!(deg_rotation.value(), 90.0);
        assert_eq!(rad_rotation.value(), PI);
    }
}
