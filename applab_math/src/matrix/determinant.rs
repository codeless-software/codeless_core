//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use num_traits::{Num, NumCast};

use crate::matrix::{Mat1, Mat2, Mat3, Mat4};

//--------------------------------------------------------------------------------------------------
//-- IMPLEMENTATIONS -------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

impl<S: Copy + Num + NumCast + Default> Mat1<S> {
    #[inline]
    pub fn determinant(&self) -> S {
        self[0][0]
    }
}

//--------------------------------------------------------------------------------------------------

impl<S: Copy + Num + NumCast + Default> Mat2<S> {
    #[inline]
    pub fn determinant(&self) -> S {
        self[0][0] * self[1][1] - self[0][1] * self[1][0]
    }
}

//--------------------------------------------------------------------------------------------------

impl<S: Copy + Num + NumCast + Default> Mat3<S> {
    #[inline]
    pub fn determinant(&self) -> S {
        self[0][0] * (self[1][1] * self[2][2] - self[1][2] * self[2][1])
            - self[0][1] * (self[1][0] * self[2][2] - self[1][2] * self[2][0])
            + self[0][2] * (self[1][0] * self[2][1] - self[1][1] * self[2][0])
    }
}

//--------------------------------------------------------------------------------------------------

impl<S: Copy + Num + NumCast + Default> Mat4<S> {
    #[inline]
    pub fn determinant(&self) -> S {
        let a = self[0][0];
        let b = self[0][1];
        let c = self[0][2];
        let d = self[0][3];
        let e = self[1][0];
        let f = self[1][1];
        let g = self[1][2];
        let h = self[1][3];
        let i = self[2][0];
        let j = self[2][1];
        let k = self[2][2];
        let l = self[2][3];
        let m = self[3][0];
        let n = self[3][1];
        let o = self[3][2];
        let p = self[3][3];

        a * (f * (k * p - l * o) - g * (j * p - l * n) + h * (j * o - k * n))
            - b * (e * (k * p - l * o) - g * (i * p - l * m) + h * (i * o - k * m))
            + c * (e * (j * p - l * n) - f * (i * p - l * m) + h * (i * n - j * m))
            - d * (e * (j * o - k * n) - f * (i * o - k * m) + g * (i * n - j * m))
    }
}

//--------------------------------------------------------------------------------------------------
//-- TESTS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use crate::matrix::{Mat1f, Mat2f, Mat3f, Mat4f};

    #[test]
    fn determinant() {
        assert!(Mat1f::identity().determinant() == 1.0);
        assert!(Mat2f::identity().determinant() == 1.0);
        assert!(Mat3f::identity().determinant() == 1.0);
        assert!(Mat4f::identity().determinant() == 1.0);
    }
}
