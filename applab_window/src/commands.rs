//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- ENUMS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// Enum of possible window commands.
///
/// It is intended to be consumed by a window backend (e.g. `applab_winit`), and not by the end user.
#[derive(Debug)]
pub enum WindowCommand {
    /// Sets the title.
    /// See [Window::set_title](crate::Window::set_title)
    Title(&'static str),

    /// Set the window size.
    /// See [Window::Set_size](crate::Window::set_size)
    Resize(u32, u32),

    /// Move the window to a new position.
    /// See [Window::set_position](crate::Window::set_position)
    Move(i32, i32),

    /// Request a redraw from the window.
    RequestRedraw,

    /// Close the window.
    /// See [Window::close](crate::Window::close)
    Close,
}
