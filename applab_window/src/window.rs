//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::fmt::{Display, Formatter};

use applab_ecs::macros::Resource;
use raw_window_handle::{HasRawDisplayHandle, HasRawWindowHandle, RawDisplayHandle, RawWindowHandle};

use crate::commands::WindowCommand;

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// Basic wrapper for a UUID which represents a window id.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct WindowId(uuid::Uuid);

impl Default for WindowId {
    fn default() -> Self {
        Self(uuid::Uuid::new_v4())
    }
}

impl Display for WindowId {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl WindowId {
    /// Returns a new, random window ID.
    pub fn new() -> Self {
        Self::default()
    }

    /// Return the primary window ID.
    ///
    /// This ID is equal to `00000000-0000-0000-0000-000000000000`.
    pub const fn primary() -> Self {
        Self(uuid::Uuid::nil())
    }
}

//--------------------------------------------------------------------------------------------------

/// Describes a window.
#[derive(Debug, Clone, Resource)]
pub struct WindowDescriptor {
    pub title: &'static str,

    pub width:  u32,
    pub height: u32,

    // TODO: Replace this with a Vec2 once the math crate exists.
    pub position_x: i32,
    pub position_y: i32,
}

impl Default for WindowDescriptor {
    fn default() -> Self {
        Self {
            title:      "AppLab Window",
            width:      1280,
            height:     720,
            position_x: 0,
            position_y: 0,
        }
    }
}

//--------------------------------------------------------------------------------------------------

/// Represents a window.
///
/// It stores information on the current window state, such as the size, position, and ID,
/// and also provides access to various commands to modify the window.
#[derive(Debug)]
pub struct Window {
    id: WindowId,

    title: &'static str,

    width:              u32,
    height:             u32,
    framebuffer_width:  u32,
    framebuffer_height: u32,

    // TODO: Replace this with a Vec2 once the math crate has been added.
    position: Option<(i32, i32)>,

    scale_factor: f32,

    commands: Vec<WindowCommand>,

    raw_window_handle:  RawWindowHandle,
    raw_display_handle: RawDisplayHandle,
}

unsafe impl HasRawWindowHandle for Window {
    fn raw_window_handle(&self) -> RawWindowHandle {
        self.raw_window_handle
    }
}

unsafe impl HasRawDisplayHandle for Window {
    fn raw_display_handle(&self) -> RawDisplayHandle {
        self.raw_display_handle
    }
}

impl Window {
    pub const fn new(
        id: WindowId,
        descriptor: WindowDescriptor,
        framebuffer_width: u32,
        framebuffer_height: u32,
        position: Option<(i32, i32)>,
        scale_factor: f32,
        raw_window_handle: RawWindowHandle,
        raw_display_handle: RawDisplayHandle,
    ) -> Self {
        Self {
            id,
            title: descriptor.title,
            width: descriptor.width,
            height: descriptor.height,
            framebuffer_width,
            framebuffer_height,
            position,
            scale_factor,

            commands: vec![],
            raw_window_handle,
            raw_display_handle,
        }
    }

    pub fn drain_commands(&mut self) -> impl Iterator<Item = WindowCommand> + '_ {
        self.commands.drain(..)
    }

    //-- GETTERS -----------------------------------------------------------------------------------

    /// Returns the window ID.
    pub const fn id(&self) -> WindowId {
        self.id
    }

    /// Returns the window title.
    pub const fn title(&self) -> &str {
        self.title
    }

    /// Returns the window width.
    ///
    /// This is the width requested upon window creation.
    pub const fn width(&self) -> u32 {
        self.width
    }

    /// Returns the window height.
    ///
    /// This is the height requested upon window creation.
    pub const fn height(&self) -> u32 {
        self.height
    }

    /// Returns the framebuffer width.
    ///
    /// This value is reported in pixels.
    pub const fn framebuffer_width(&self) -> u32 {
        self.framebuffer_width
    }

    /// Returns the framebuffer height.
    ///
    /// This value is reported in pixels.
    pub const fn framebuffer_height(&self) -> u32 {
        self.framebuffer_height
    }

    /// Returns the window position.
    pub const fn position(&self) -> Option<(i32, i32)> {
        self.position
    }

    /// Returns the window scale factor.
    pub const fn scale_factor(&self) -> f32 {
        self.scale_factor
    }

    //-- SETTERS -----------------------------------------------------------------------------------

    /// Sets the actual window size, based on what is reported from the backend.
    ///
    /// This function is intended to be called by the window backend implementations, and not by
    /// the end user.
    pub fn set_actual_window_size_from_backed(&mut self, width: u32, height: u32) {
        self.width = width;
        self.height = height;
    }

    /// Sets the actual framebuffer size, based on what is reported from the backend.
    ///
    /// This function is intended to be called by the window backend implementations, and not by
    /// the end user.
    pub fn set_actual_framebuffer_size_from_backed(&mut self, width: u32, height: u32) {
        self.framebuffer_width = width;
        self.framebuffer_height = height;
    }

    /// Sets the actual window position, based on what is reported from the backend.
    ///
    /// This function is intended to be called by window backends, and not by the end user.
    pub fn set_actual_position_from_backend(&mut self, position_x: i32, position_y: i32) {
        self.position = Some((position_x, position_y));
    }

    pub fn set_actual_scale_factor_from_backend(&mut self, scale_factor: f32) {
        self.scale_factor = scale_factor;
    }

    //-- COMMANDS ----------------------------------------------------------------------------------

    /// Sets the window title.
    ///
    /// # Arguments
    ///
    /// * `title`: The new window title.
    pub fn set_title(&mut self, title: &'static str) {
        self.commands.push(WindowCommand::Title(title));
        self.title = title;
    }

    /// Sets the window size.
    ///
    /// # Arguments
    ///
    /// * `width`: New window width.
    /// * `height`: New window height
    pub fn set_size(&mut self, width: u32, height: u32) {
        self.commands.push(WindowCommand::Resize(width, height));
    }

    /// Sets the window position.
    ///
    /// # Arguments
    ///
    /// * `pos_x`: The new X position.
    /// * `pos_y`: The new Y position.
    pub fn set_position(&mut self, pos_x: i32, pos_y: i32) {
        self.commands.push(WindowCommand::Move(pos_x, pos_y));
    }

    /// Requests a redraw from the window.
    pub fn request_redraw(&mut self) {
        self.commands.push(WindowCommand::RequestRedraw);
    }

    /// Close the window.
    pub fn close(&mut self) {
        self.commands.push(WindowCommand::Close);
    }
}
