//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use applab_core::AppExit;
use applab_ecs::{resource::Res, system::command::Commands};

use crate::WindowContainer;

//--------------------------------------------------------------------------------------------------
//-- FUNCTIONS -------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub fn exit_when_all_windows_closed(mut commands: Commands, windows: Res<WindowContainer>) {
    if windows.is_empty() {
        commands.send_event(AppExit);
    }
}

//--------------------------------------------------------------------------------------------------

pub fn exit_when_primary_window_closed(mut commands: Commands, windows: Res<WindowContainer>) {
    if windows.get_primary().is_none() {
        commands.send_event(AppExit);
    }
}
