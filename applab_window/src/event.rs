//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//! Window events.
//!
//! Provides various window events, which are sent by the window backends (e.g. `applab_winit`).
//! These events are used like any other ecs event.
//! ```
//! # use applab_ecs::event::EventReader;
//! # use applab_window::event::WindowClosed;
//! fn my_system(mut closed_event: EventReader<WindowClosed>) {
//!     for event in closed_event.iter() {
//!         // ...
//!     }
//! }
//! ```

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use applab_ecs::macros::Event;

use crate::{WindowDescriptor, WindowId};

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// Request to create a new window.
#[derive(Debug, Event)]
pub struct CreateWindow {
    /// ID of the new window to be created.
    pub id: WindowId,

    /// Description of the window.
    pub descriptor: WindowDescriptor,
}

//--------------------------------------------------------------------------------------------------

/// Sent when a window has been created.
#[derive(Debug, Event)]
pub struct WindowCreated {
    /// ID of the newly created window.
    pub id: WindowId,
}

//--------------------------------------------------------------------------------------------------

/// Sent when a window has been resized, whether by the user or by [`Window::set_size`](crate::Window::set_size).
#[derive(Debug, Event)]
pub struct WindowResized {
    /// ID of the window that has been resized.
    pub id: WindowId,

    /// New window width.
    pub width: u32,

    /// New window height.
    pub height: u32,
}

//--------------------------------------------------------------------------------------------------

#[derive(Debug, Event)]
pub struct FrameBufferResized {
    /// ID of the window that has been resized.
    pub id: WindowId,

    /// New framebuffer width.
    pub width: u32,

    /// New framebuffer height.
    pub height: u32,
}

//--------------------------------------------------------------------------------------------------

/// Sent when a window has been moved, whether by the user or by [`Window::set_position`](crate::Window::set_position).
#[derive(Debug, Event)]
pub struct WindowMoved {
    /// ID of the window that has been moved.
    pub id: WindowId,

    /// New X position.
    pub x: i32,

    /// New Y position.
    pub y: i32,
}

//--------------------------------------------------------------------------------------------------

/// Sent when a window redraw has been requested.
#[derive(Debug, Event)]
pub struct WindowRedraw {
    /// ID of the window that has requested a redraw
    pub id: WindowId,
}

//--------------------------------------------------------------------------------------------------

#[derive(Debug, Event)]
pub struct WindowScaleFactorChanged {
    pub id:           WindowId,
    pub scale_factor: f32,
}

//--------------------------------------------------------------------------------------------------

/// Sent when closing a window has been requested.
#[derive(Debug, Event)]
pub struct WindowCloseRequested {
    /// ID of the window to be closed.
    pub id: WindowId,
}

//--------------------------------------------------------------------------------------------------

/// Sent when a window has been closed
#[derive(Debug, Event)]
pub struct WindowClosed {
    /// ID of the window that has been closed.
    pub id: WindowId,
}
