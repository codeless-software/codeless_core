//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use applab_core::{plugin::Plugin, App};
use applab_ecs::{
    event::Events,
    system::{CoreStage, IntoSystem},
};

use crate::{
    event::*,
    system::{exit_when_all_windows_closed, exit_when_primary_window_closed},
    WindowContainer, WindowDescriptor, WindowId,
};

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[derive(Default, Clone)]
pub struct WindowPlugin {
    with_primary_window:           bool,
    exit_on_all_windows_closed:    bool,
    exit_on_primary_window_closed: bool,
}

impl WindowPlugin {
    pub const fn with_primary_window(mut self) -> Self {
        self.with_primary_window = true;
        self
    }

    pub const fn exit_on_all_windows_closed(mut self) -> Self {
        self.exit_on_all_windows_closed = true;
        self
    }

    pub const fn exit_on_primary_window_closed(mut self) -> Self {
        self.exit_on_primary_window_closed = true;
        self
    }
}

impl Plugin for WindowPlugin {
    fn register(&self, app: &mut App) {
        app.add_event::<CreateWindow>()
            .add_event::<WindowCreated>()
            .add_event::<WindowResized>()
            .add_event::<WindowMoved>()
            .add_event::<WindowScaleFactorChanged>()
            .add_event::<WindowRedraw>()
            .add_event::<WindowCloseRequested>()
            .add_event::<WindowClosed>()
            .add_event::<FrameBufferResized>()
            .init_resource::<WindowContainer>();

        if self.with_primary_window {
            let descriptor = app
                .world
                .get_resource::<WindowDescriptor>()
                .map(|descriptor| (*descriptor).clone())
                .unwrap_or_default();

            app.world.resource_mut::<Events<CreateWindow>>().send(CreateWindow {
                id: WindowId::primary(),
                descriptor,
            });

            app.world.remove_resource::<WindowDescriptor>();
        }

        if self.exit_on_all_windows_closed {
            app.add_system_to_stage(CoreStage::PostUpdate, exit_when_all_windows_closed.system());
        }

        if self.exit_on_primary_window_closed {
            app.add_system_to_stage(CoreStage::PostUpdate, exit_when_primary_window_closed.system());
        }
    }
}
