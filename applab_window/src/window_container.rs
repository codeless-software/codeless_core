//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use applab_ecs::macros::Resource;
use applab_util::HashMap;

use crate::window::{Window, WindowId};

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// Stores and provides access to multiple windows.
#[derive(Debug, Default, Resource)]
pub struct WindowContainer {
    windows: HashMap<WindowId, Window>,
}

impl WindowContainer {
    #[inline]
    pub fn len(&self) -> usize {
        self.windows.len()
    }

    #[inline]
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// Add a window.
    pub fn add(&mut self, window: Window) {
        self.windows.insert(window.id(), window);
    }

    /// Get a reference to a window.
    ///
    /// # Arguments
    ///
    /// * `id`: ID of the window to fetch
    pub fn get(&self, id: &WindowId) -> Option<&Window> {
        self.windows.get(id)
    }

    /// Get a mutable reference to a window.
    ///
    /// # Arguments
    ///
    /// * `id`: ID of the window to fetch.
    pub fn get_mut(&mut self, id: &WindowId) -> Option<&mut Window> {
        self.windows.get_mut(id)
    }

    /// Get a reference to the primary window.
    pub fn get_primary(&self) -> Option<&Window> {
        self.get(&WindowId::primary())
    }

    /// Get a mutable reference to the primary window.
    pub fn get_primary_mut(&mut self) -> Option<&mut Window> {
        self.get_mut(&WindowId::primary())
    }

    /// Remove a window.
    ///
    /// # Arguments
    ///
    /// * `id`: ID of the window to remove.
    pub fn remove(&mut self, id: &WindowId) {
        self.windows.remove(id);
    }

    /// Returns an iterator over all windows.
    pub fn iter(&self) -> impl Iterator<Item = &Window> {
        self.windows.values()
    }

    /// Returns a mutable iterator over all windows.
    pub fn iter_mut(&mut self) -> impl Iterator<Item = &mut Window> {
        self.windows.values_mut()
    }
}
