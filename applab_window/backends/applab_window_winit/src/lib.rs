//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub mod plugin;
mod system;

use std::ops::{Deref, DerefMut};

use applab_core::{App, RunnerResult};
use applab_ecs::{
    event::{Events, ManualEventReader},
    macros::Resource,
    resource::ResMut,
    World,
};
use applab_util::{BiHashMap, HashMap};
use applab_window::{
    event::{
        CreateWindow, FrameBufferResized, WindowCloseRequested, WindowCreated, WindowMoved, WindowRedraw,
        WindowResized, WindowScaleFactorChanged,
    },
    Window, WindowContainer, WindowDescriptor, WindowId,
};
use log::error;
use raw_window_handle::{HasRawDisplayHandle, HasRawWindowHandle};
use thiserror::Error;
use winit::{
    dpi::LogicalSize,
    event::{Event, WindowEvent},
    event_loop::{ControlFlow, EventLoop, EventLoopWindowTarget},
    platform::run_return::EventLoopExtRunReturn,
};

//--------------------------------------------------------------------------------------------------
//-- ENUMS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[derive(Debug, Error)]
enum WinitPluginError {
    #[error("Window with ID `{0}` already exists")]
    WindowIdAlreadyExists(WindowId),

    #[error("Failed to create window")]
    WindowCreationFailed(#[from] winit::error::OsError),

    #[error("Required resource `{0}` is missing")]
    RequiredResourceMissing(&'static str),
}

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[derive(Debug, Resource)]
struct WinitEventLoopProxy(winit::event_loop::EventLoopProxy<()>);

impl Deref for WinitEventLoopProxy {
    type Target = winit::event_loop::EventLoopProxy<()>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for WinitEventLoopProxy {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

//--------------------------------------------------------------------------------------------------

#[derive(Debug, Default, Resource)]
struct WinitWindowContainer {
    windows: HashMap<winit::window::WindowId, winit::window::Window>,
    id_map:  BiHashMap<winit::window::WindowId, WindowId>,
}

impl WinitWindowContainer {
    fn create_window(
        &mut self,
        window_target: &winit::event_loop::EventLoopWindowTarget<()>,
        window_id: WindowId,
        window_descriptor: WindowDescriptor,
    ) -> Result<Window, WinitPluginError> {
        // Early return if a window with the given ID already exists.
        if self.id_map.contains_right(&window_id) {
            return Err(WinitPluginError::WindowIdAlreadyExists(window_id));
        }

        let builder = winit::window::WindowBuilder::new();

        let winit_window = builder
            .with_title(window_descriptor.title)
            .with_inner_size(winit::dpi::LogicalSize::new(
                window_descriptor.width,
                window_descriptor.height,
            ))
            .with_position(winit::dpi::LogicalPosition::new(
                window_descriptor.position_x,
                window_descriptor.position_y,
            ))
            .build(window_target)?;

        let physical_size = winit_window.inner_size();
        let position = winit_window.outer_position().ok().map(|pos| (pos.x, pos.y));

        let scale_factor = winit_window.scale_factor() as f32;
        let winit_id = winit_window.id();

        let raw_window_handle = winit_window.raw_window_handle();
        let raw_display_handle = winit_window.raw_display_handle();

        self.windows.insert(winit_id, winit_window);
        self.id_map.insert(winit_id, window_id);

        Ok(Window::new(
            window_id,
            window_descriptor,
            physical_size.width,
            physical_size.height,
            position,
            scale_factor,
            raw_window_handle,
            raw_display_handle,
        ))
    }

    fn get_window(&self, id: &WindowId) -> Option<&winit::window::Window> {
        let winit_id = self.id_map.get_by_right(id)?;
        self.windows.get(winit_id)
    }

    fn get_window_mut(&mut self, id: &WindowId) -> Option<&mut winit::window::Window> {
        let winit_id = self.id_map.get_by_right(id)?;
        self.windows.get_mut(winit_id)
    }

    fn remove_window(&mut self, window_id: WindowId) -> Option<winit::window::Window> {
        let (winit_id, _) = self.id_map.remove_by_right(&window_id)?;
        self.windows.remove(&winit_id)
    }

    fn window_id(&self, winit_id: &winit::window::WindowId) -> WindowId {
        self.get_window_id(winit_id)
            .map_or_else(|| panic!("Winit window with id `{:?}` not found", winit_id), |id| id)
    }

    fn get_window_id(&self, winit_id: &winit::window::WindowId) -> Option<WindowId> {
        self.id_map.get_by_left(winit_id).copied()
    }

    fn request_redraw(&self, id: &WindowId) {
        let window = self.get_window(id).unwrap();
        window.request_redraw();
    }
}

//--------------------------------------------------------------------------------------------------
//-- PRIVATE FUNCTIONS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

fn app_runner(mut app: App) -> RunnerResult {
    let mut event_loop = winit::event_loop::EventLoop::new();

    app.scheduler.execute_startup(&mut app.world);

    let event_handler = move |event: Event<()>,
                              event_loop: &EventLoopWindowTarget<()>,
                              control_flow: &mut ControlFlow| {
        control_flow.set_poll();

        if app.exit_requested() {
            control_flow.set_exit();
        }

        match event {
            Event::NewEvents(_) => {}
            Event::WindowEvent { event, window_id } => {
                let (mut windows, _, id) = if let Some(v) = get_container_and_id(window_id, &app.world) {
                    v
                } else {
                    return;
                };

                match event {
                    WindowEvent::Resized(framebuffer_size) => {
                        windows.get_mut(&id).map_or_else(
                            || error!("No window found with ID `{id}`"),
                            |window| {
                                let size = LogicalSize::from_physical(framebuffer_size, window.scale_factor() as f64);
                                window.set_actual_window_size_from_backed(size.width, size.height);
                                window.set_actual_framebuffer_size_from_backed(
                                    framebuffer_size.width,
                                    framebuffer_size.height,
                                );

                                app.world.resource_mut::<Events<WindowResized>>().send(WindowResized {
                                    id,
                                    width: size.width,
                                    height: size.height,
                                });

                                app.world
                                    .resource_mut::<Events<FrameBufferResized>>()
                                    .send(FrameBufferResized {
                                        id,
                                        width: framebuffer_size.width,
                                        height: framebuffer_size.height,
                                    });
                            },
                        );
                    }

                    WindowEvent::Moved(position) => {
                        windows.get_mut(&id).map_or_else(
                            || error!("No window found with ID `{id}`"),
                            |window| {
                                println!("Window moved: {}, {}", position.x, position.y);
                                window.set_actual_position_from_backend(position.x, position.y);

                                app.world.resource_mut::<Events<WindowMoved>>().send(WindowMoved {
                                    id,
                                    x: position.x,
                                    y: position.y,
                                });
                            },
                        );
                    }

                    WindowEvent::CloseRequested => {
                        app.world
                            .resource_mut::<Events<WindowCloseRequested>>()
                            .send(WindowCloseRequested { id });
                    }

                    WindowEvent::Destroyed => {
                        println!("Boom!");
                    }

                    WindowEvent::DroppedFile(_) => {}
                    WindowEvent::HoveredFile(_) => {}
                    WindowEvent::HoveredFileCancelled => {}
                    WindowEvent::ReceivedCharacter(_) => {}
                    WindowEvent::Focused(_) => {}
                    WindowEvent::KeyboardInput { .. } => {}
                    WindowEvent::ModifiersChanged(_) => {}
                    WindowEvent::Ime(_) => {}
                    WindowEvent::CursorMoved { .. } => {}
                    WindowEvent::CursorEntered { .. } => {}
                    WindowEvent::CursorLeft { .. } => {}
                    WindowEvent::MouseWheel { .. } => {}
                    WindowEvent::MouseInput { .. } => {}
                    WindowEvent::TouchpadPressure { .. } => {}
                    WindowEvent::AxisMotion { .. } => {}
                    WindowEvent::Touch(_) => {}
                    WindowEvent::ScaleFactorChanged { scale_factor, .. } => {
                        let scale_factor = scale_factor as f32;

                        windows.get_mut(&id).map_or_else(
                            || error!("No window found with ID `{id}`"),
                            |window| {
                                println!("Scale factor changed: {scale_factor}");
                                window.set_actual_scale_factor_from_backend(scale_factor);

                                app.world
                                    .resource_mut::<Events<WindowScaleFactorChanged>>()
                                    .send(WindowScaleFactorChanged { id, scale_factor });
                            },
                        );
                    }
                    WindowEvent::ThemeChanged(_) => {}
                    WindowEvent::Occluded(_) => {}
                    WindowEvent::TouchpadMagnify { .. } => {}
                    WindowEvent::SmartMagnify { .. } => {}
                    WindowEvent::TouchpadRotate { .. } => {}
                }
            }
            Event::DeviceEvent { .. } => {}
            Event::UserEvent(_) => {}
            Event::Suspended => {}
            Event::Resumed => {}
            Event::MainEventsCleared => {
                let events = app.world.resource::<Events<CreateWindow>>().get_reader();
                handle_create_window_events(&mut app, event_loop, events);

                app.scheduler.execute(&mut app.world);
            }
            Event::RedrawRequested(window_id) => {
                let (mut windows, _, id) = if let Some(v) = get_container_and_id(window_id, &app.world) {
                    v
                } else {
                    return;
                };

                windows.get_mut(&id).map_or_else(
                    || error!("No window found with ID `{id}`"),
                    |_| {
                        app.world
                            .resource_mut::<Events<WindowRedraw>>()
                            .send(WindowRedraw { id });
                    },
                );
            }
            Event::RedrawEventsCleared => {}
            Event::LoopDestroyed => {}
        }
    };

    run_return(&mut event_loop, event_handler);
    Ok(())
}

//--------------------------------------------------------------------------------------------------

fn handle_create_window_events(
    app: &mut App,
    event_loop: &EventLoopWindowTarget<()>,
    mut create_window_events: ManualEventReader<CreateWindow>,
) -> Result<(), WinitPluginError> {
    let events = app.world.resource::<Events<CreateWindow>>();
    let mut winit_container = app.world.resource_mut::<WinitWindowContainer>();
    let mut win_container = app.world.resource_mut::<WindowContainer>();

    for event in create_window_events.iter(&events) {
        let window = winit_container.create_window(event_loop, event.id, event.descriptor.clone())?;
        win_container.add(window);

        winit_container.request_redraw(&event.id);

        app.world
            .resource_mut::<Events<WindowCreated>>()
            .send(WindowCreated { id: event.id });
    }

    Ok(())
}

//--------------------------------------------------------------------------------------------------

fn run_return<F>(event_loop: &mut EventLoop<()>, event_handler: F)
where
    F: FnMut(Event<()>, &EventLoopWindowTarget<()>, &mut ControlFlow),
{
    event_loop.run_return(event_handler);
}

//--------------------------------------------------------------------------------------------------
//-- PRIVATE HELPER FUNCTIONS ----------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

fn get_container_and_id(
    winit_id: winit::window::WindowId,
    world: &World,
) -> Option<(ResMut<WindowContainer>, ResMut<WinitWindowContainer>, WindowId)> {
    let windows = world.resource_mut::<WindowContainer>();
    let winit_windows = world.resource_mut::<WinitWindowContainer>();

    let id = if let Some(id) = winit_windows.get_window_id(&winit_id) {
        id
    } else {
        println!("Skipped! {:?}", winit_id);
        return None;
    };

    Some((windows, winit_windows, id))
}
