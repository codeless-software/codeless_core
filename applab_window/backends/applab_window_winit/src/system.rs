//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use applab_ecs::{event::EventWriter, resource::ResMut};
use applab_window::{commands::WindowCommand, event::WindowClosed, WindowContainer};
use winit::dpi::{LogicalSize, PhysicalPosition};

use crate::WinitWindowContainer;

//--------------------------------------------------------------------------------------------------
//-- FUNCTIONS -------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub(super) fn process_window_commands(
    mut windows: ResMut<WindowContainer>,
    mut winit_windows: ResMut<WinitWindowContainer>,
    mut window_close_events: EventWriter<WindowClosed>,
) {
    let mut removed_windows = vec![];

    for cl_window in windows.iter_mut() {
        let id = cl_window.id();

        for event in cl_window.drain_commands() {
            let winit_window = winit_windows.get_window_mut(&id).unwrap();

            match event {
                WindowCommand::Title(title) => winit_window.set_title(title),

                WindowCommand::Resize(width, height) => winit_window.set_inner_size(LogicalSize::new(width, height)),

                WindowCommand::Move(x, y) => winit_window.set_outer_position(PhysicalPosition::new(x, y)),

                WindowCommand::RequestRedraw => winit_window.request_redraw(),

                WindowCommand::Close => {
                    removed_windows.push(id);
                    break;
                }
            };
        }
    }

    if !removed_windows.is_empty() {
        for id in removed_windows {
            winit_windows.remove_window(id);
            windows.remove(&id);
            window_close_events.send(WindowClosed { id });
        }
    }
}
