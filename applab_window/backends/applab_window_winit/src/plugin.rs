//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use applab_core::{plugin::Plugin, App};
use applab_ecs::system::{CoreStage, IntoSystem};
use applab_window::plugin::WindowPlugin;

use crate::{app_runner, system::process_window_commands, WinitWindowContainer};

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[derive(Default)]
pub struct WinitWindowPlugin {
    window_plugin: WindowPlugin,
}

impl WinitWindowPlugin {
    pub const fn with_primary_window(mut self) -> Self {
        self.window_plugin = self.window_plugin.with_primary_window();
        self
    }

    pub const fn exit_on_all_windows_closed(mut self) -> Self {
        self.window_plugin = self.window_plugin.exit_on_all_windows_closed();
        self
    }

    pub const fn exit_on_primary_window_closed(mut self) -> Self {
        self.window_plugin = self.window_plugin.exit_on_primary_window_closed();
        self
    }
}

impl Plugin for WinitWindowPlugin {
    fn register(&self, app: &mut App) {
        app.add_plugin(self.window_plugin.clone())
            .init_resource::<WinitWindowContainer>()
            .add_system_to_stage(CoreStage::Last, process_window_commands.system())
            .set_runner(app_runner);
    }
}
