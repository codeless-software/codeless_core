[![pipeline status](https://gitlab.com/applab-software/applab_core/badges/master/pipeline.svg)](https://gitlab.com/applab-software/applab_core/-/commits/master)
[![coverage report](https://gitlab.com/applab-software/applab_core/badges/master/coverage.svg)](https://gitlab.com/applab-software/applab_core/-/commits/master)
[![Latest Release](https://gitlab.com/applab-software/applab_core/-/badges/release.svg)](https://gitlab.com/applab-software/applab_core/-/releases)

---
